package com.fynxapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Category;
import com.fynxapp.utils.TypefaceSpan;

import java.util.ArrayList;
import java.util.List;


public class AddAccount extends ActionBarActivity {
    private Typeface mTypeface;
    private EditText mTitle;
    private DBHelper db;
    private List<Account> mAccountList;
    private List<String> mAccounts;
    private Bundle mBundle;
    private int id;
    private boolean mEdit;
    private Account mEditAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >  10) {
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
        setContentView(R.layout.add_account);

        db = new DBHelper(this);
        mAccountList = db.getAccountsList();

        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            id = mBundle.getInt("id");
        }

        mTitle = (EditText) this.findViewById(R.id.et_acc_title);
        mTitle.setTypeface(mTypeface);

        if (mEdit) {
            mEditAccount = db.getAccountById(id);
            getSupportActionBar().setTitle(getString(R.string.edit_account));
            if (mEditAccount != null) {
                mTitle.setText(mEditAccount.getName());
            }
        }

        mAccounts = new ArrayList<String>();

        if (mAccountList != null
                && mAccountList.size() > 0) {
            for (int i = 0; i < mAccountList.size(); i++) {
                Account account = mAccountList.get(i);
                if (mEdit
                        && !account.getName().equalsIgnoreCase(mEditAccount.getName())) {
                    mAccounts.add(account.getName().toUpperCase());
                } else if (!mEdit) {
                    mAccounts.add(account.getName().toUpperCase());
                }
            }
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_account, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            String name = mTitle.getText().toString();
            Account account = new Account();
            if (name == null
                    || name.equals("")) {
                Toast.makeText(this, getString(R.string.err_acc_name), Toast.LENGTH_SHORT).show();
            } else {
                if (mAccounts.contains(name.toUpperCase())) {
                    Toast.makeText(this, getString(R.string.err_acc_exists), Toast.LENGTH_SHORT).show();
                } else {
                    account.setName(name);
                    if (mEdit) {
                        account.setId(mEditAccount.getId());
                    }
                    db.saveAccount(account);
                    if (mEdit)
                        db.updateLedger("account", account.getName(), mEditAccount.getName(), null);
                    this.finish();
                }
            }
            return true;
        } else if (id == R.id.cancel) {
            this.finish();
        } else if (id == R.id.discard) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.info_delete))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            db.deleteAccountById(mEditAccount.getId());
                            AddAccount.this.finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
        if (Build.VERSION.SDK_INT > 10) {
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
}
