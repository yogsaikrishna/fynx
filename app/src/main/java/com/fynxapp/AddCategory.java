package com.fynxapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Category;
import com.fynxapp.utils.TypefaceSpan;

import java.util.ArrayList;
import java.util.List;


public class AddCategory extends ActionBarActivity {
    private Typeface mTypeface;
    private EditText mCategory;
    private RadioButton mIncome, mExpense;
    private RadioGroup mRadioGroup;
    private String mType;
    private DBHelper db;
    private List<Category> mCategoryList;
    private List<String> mIncomeCategories, mExpenseCategories;
    private Bundle mBundle;
    private int id;
    private boolean mEdit;
    private Category mEditCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > 10) {
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
        setContentView(R.layout.add_category);

        db = new DBHelper(this);
        mCategoryList = db.getCategoriesByType(null);

        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            id = mBundle.getInt("id");
        }

        mCategory = (EditText) this.findViewById(R.id.et_category);
        mIncome = (RadioButton) this.findViewById(R.id.rb_income);
        mExpense = (RadioButton) this.findViewById(R.id.rb_expense);
        mRadioGroup = (RadioGroup) this.findViewById(R.id.radiogroup);

        mCategory.setTypeface(mTypeface);
        mIncome.setTypeface(mTypeface);
        mExpense.setTypeface(mTypeface);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_income:
                        mType = "income";
                        break;
                    case R.id.rb_expense:
                        mType = "expense";
                        break;
                }
            }
        });

        if (mEdit) {
            mEditCategory = db.getCategoriesById(id);
            getSupportActionBar().setTitle(getString(R.string.edit_category));
            if (mEditCategory != null) {
                mCategory.setText(mEditCategory.getName());
                if (mEditCategory.getType().equals("income")) {
                    mRadioGroup.check(R.id.rb_income);
                } else {
                    mRadioGroup.check(R.id.rb_expense);
                }
            }
        }

        mIncomeCategories = new ArrayList<String>();
        mExpenseCategories = new ArrayList<String>();

        if (mCategoryList != null
                && mCategoryList.size() > 0) {
            for (int i = 0; i < mCategoryList.size(); i++) {
                Category category = mCategoryList.get(i);
                if (category.getType().equals("income")) {
                    if (mEdit
                            && !category.getName().equalsIgnoreCase(mEditCategory.getName())) {
                        mIncomeCategories.add(category.getName().toUpperCase());
                    } else if (!mEdit) {
                        mIncomeCategories.add(category.getName().toUpperCase());
                    }
                } else {
                    if (mEdit
                            && !category.getName().equalsIgnoreCase(mEditCategory.getName())) {
                        mExpenseCategories.add(category.getName().toUpperCase());
                    } else if (!mEdit) {
                        mExpenseCategories.add(category.getName().toUpperCase());
                    }
                }
            }
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_category, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            String name = mCategory.getText().toString();
            Category category = new Category();
            boolean flag = true;
            if (name == null
                    || name.equals("")) {
                Toast.makeText(this, getString(R.string.err_cat_name), Toast.LENGTH_SHORT).show();
            } else if (mType == null) {
                Toast.makeText(this, getString(R.string.err_cat_type), Toast.LENGTH_SHORT).show();
            } else {
                if (mType.equals("income")) {
                    if (mIncomeCategories.contains(name.toUpperCase())) {
                        Toast.makeText(this, getString(R.string.err_cat_income), Toast.LENGTH_SHORT).show();
                        flag = false;
                    }
                } else if (mType.equals("expense")) {
                    if (mExpenseCategories.contains(name.toUpperCase())) {
                        Toast.makeText(this, getString(R.string.err_cat_expense), Toast.LENGTH_SHORT).show();
                        flag = false;
                    }
                }
                if (flag) {
                    category.setName(name);
                    category.setType(mType);
                    if (mEdit) {
                        category.setId(mEditCategory.getId());
                    }
                    db.saveCategory(category);
                    if (mEdit) {
                        db.updateLedger("category", category.getName(), mEditCategory.getName(),
                                mEditCategory.getType());
                    }
                    this.finish();
                }
            }
            return true;
        } else if (id == R.id.cancel) {
            this.finish();
        } else if (id == R.id.discard) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.info_delete))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            db.deleteCategoryById(mEditCategory.getId());
                            AddCategory.this.finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
        if (Build.VERSION.SDK_INT > 10) {
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
}
