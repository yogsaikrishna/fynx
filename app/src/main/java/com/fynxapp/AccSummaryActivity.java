package com.fynxapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Category;
import com.fynxapp.db.objects.Summary;
import com.fynxapp.drawer.AccListAdapter;
import com.fynxapp.drawer.AccRowData;
import com.fynxapp.drawer.HeaderRow;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.ExpandableListAdapter;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.FileUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


public class AccSummaryActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "AccSummaryActivity";
    private static final int CATEGORY_MENU_ID = 128;
    private static final int ACCOUNT_MENU_ID = 256;

    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_accounts, R.drawable.ic_categories, R.drawable.ic_reports,
            R.drawable.ic_action_objective, R.drawable.ic_action_recurring,
            R.drawable.ic_action_settings};
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mNavAdapter;
    private Vector<RowData> mData;
    private Vector<AccRowData> mAccData;
    private RowData mRowData;
    private AccRowData mAccRowData;
    private ListAdapter mListAdapter;
    private AccListAdapter mAdapter;
    private ListView mList;
    private TextView mNoData;
    private DBHelper db;
    private List<Summary> mSummaryList;
    private List<Summary> headerData;
    private List<Summary> summaryData;
    private List<Account> mAccounts;
    private List<Category> mCategories;
    private HashMap<String, List<Summary>> mExpListData;
    private SharedPreferences mSharedPreferences;
    private String mGrouping;
    private Typeface mTypeface;
    private ExpandableListAdapter mAccListAdapter;
    private ExpandableListView mListView;
    private SpannableString mTitle;
    private SpannableString mSummary;
    private List<String> mCheckedAccounts, mCheckedCategories;
    private boolean purchased;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expandable_listview);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");
        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        db = new DBHelper(this);
        headerData = db.getDataHeader(mGrouping);
        summaryData = db.getDataSummary(mGrouping);
        mAccounts = db.getAccountsList();
        mCategories = db.getCategoriesByType(null);

        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mCheckedAccounts = new ArrayList<String>();
        mCheckedCategories = new ArrayList<String>();

        mNavAdapter = (ListView) findViewById(R.id.left_drawer);
        mList = (ListView) this.findViewById(R.id.listView);
        mListView = (ExpandableListView) this.findViewById(R.id.exp_list_view);
        mNoData = (TextView) this.findViewById(R.id.tv_no_data);

        initializeNavList();
        addNavigationDrawer();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mNavAdapter.setOnItemClickListener(this);

        mNoData.setTypeface(mTypeface);
        mNoData.setVisibility(View.GONE);

        updateData();

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;
                Summary summary = mSummaryList.get(i);
                if (summary.getType().equals("income")) {
                    intent = new Intent(AccSummaryActivity.this, AddIncomeActivity.class);
                } else {
                    intent = new Intent(AccSummaryActivity.this, AddExpenseActivity.class);
                }
                intent.putExtra("edit", true);
                intent.putExtra("id", summary.getId());
                startActivity(intent);
            }
        });

        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
                Intent intent;
                Summary summary;
                Summary header = headerData.get(i);
                if (mGrouping.equalsIgnoreCase("weekly")) {
                    summary = mExpListData.get(header.getStartDate()).get(i2);
                } else {
                    summary = mExpListData.get(header.getGroup()).get(i2);
                }
                if (summary.getType().equals("income")) {
                    intent = new Intent(AccSummaryActivity.this, AddIncomeActivity.class);
                } else {
                    intent = new Intent(AccSummaryActivity.this, AddExpenseActivity.class);
                }
                intent.putExtra("edit", true);
                intent.putExtra("id", summary.getId());
                startActivity(intent);
                return true;
            }
        });

        mTitle = new SpannableString(this.getTitle());
        mTitle.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mTitle.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(mTitle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.acc_summary, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Menu accSubMenu = menu.findItem(R.id.by_account).getSubMenu();
        Menu catSubMenu = menu.findItem(R.id.by_category).getSubMenu();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            MenuItem item = menu.findItem(R.id.all);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.week);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.month);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.year);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.export);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.csv);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.excel);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.by_account);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.by_category);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
            item = menu.findItem(R.id.by_date);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
        }
        if (mCheckedAccounts.size() == 0)
            accSubMenu.clear();
        if (mAccounts != null
                && mAccounts.size() > 0) {
            if (accSubMenu.findItem(ACCOUNT_MENU_ID) == null) {
                for (int i = 0; i < mAccounts.size(); i++) {
                    Account account = mAccounts.get(i);
                    SpannableString s = CommonsUtils.getSpannableString(this, account.getName());
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, s).setCheckable(true);
                    else
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, account.getName()).setCheckable(true);
                }
            }
        }
        if (mCheckedCategories.size() == 0)
            catSubMenu.clear();
        if (mCategories != null
                && mCategories.size() > 0) {
            if (catSubMenu.findItem(CATEGORY_MENU_ID) == null) {
                for (int i = 0; i < mCategories.size(); i++) {
                    Category category = mCategories.get(i);
                    SpannableString s = CommonsUtils.getSpannableString(this, category.getName());
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
                        catSubMenu.add(0, CATEGORY_MENU_ID, 0, s).setCheckable(true);
                    else
                        catSubMenu.add(0, CATEGORY_MENU_ID, 0, category.getName()).setCheckable(true);
                }
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        Uri uri;

        switch (item.getItemId()) {
            case R.id.all:
                mCheckedAccounts.clear();
                mCheckedCategories.clear();
                supportInvalidateOptionsMenu();
                updateData();
                getSupportActionBar().setSubtitle(null);
                break;
            case R.id.week:
                mSummaryList = db.getAccountSummary(7);
                updateList();

                mSummary = new SpannableString(getString(R.string.filter_week));
                mSummary.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.month:
                mSummaryList = db.getAccountSummary(30);
                updateList();

                mSummary = new SpannableString(getString(R.string.filter_month));
                mSummary.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.year:
                mSummaryList = db.getAccountSummary(90);
                updateList();

                mSummary = new SpannableString(getString(R.string.filter_year));
                mSummary.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.csv:
                mSummaryList = db.getAccountSummary();
                uri = FileUtils.exportToCSV(mSummaryList);
                if (uri != null) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("text/csv");
                    startActivity(Intent.createChooser(intent, getString(R.string.export)));
                }
                break;
            case R.id.excel:
                mSummaryList = db.getAccountSummary();
                uri = FileUtils.exportToExcel(mSummaryList);
                if (uri != null) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("application/vnd.ms-excel");
                    startActivity(Intent.createChooser(intent, getString(R.string.export)));
                }
                break;
            case ACCOUNT_MENU_ID:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mCheckedAccounts.remove(item.getTitle().toString());
                } else {
                    item.setChecked(true);
                    mCheckedAccounts.add(item.getTitle().toString());
                }

                if (mCheckedAccounts.size() > 0) {
                    mSummaryList = db.getAccountSummary("account", CommonsUtils.joinList(",", mCheckedAccounts));
                    updateList();
                    mSummary = new SpannableString(getString(R.string.filtered_account));
                    mSummary.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mSummary.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    getSupportActionBar().setSubtitle(mSummary);
                } else {
                    updateData();
                    getSupportActionBar().setSubtitle(null);
                }
                return true;
            case CATEGORY_MENU_ID:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mCheckedCategories.remove(item.getTitle().toString());
                } else {
                    item.setChecked(true);
                    mCheckedCategories.add(item.getTitle().toString());
                }

                if (mCheckedCategories.size() > 0) {
                    mSummaryList = db.getAccountSummary("category", CommonsUtils.joinList(",", mCheckedCategories));
                    updateList();
                    mSummary = new SpannableString(getString(R.string.filtered_category));
                    mSummary.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, mSummary.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    getSupportActionBar().setSubtitle(mSummary);
                } else {
                    updateData();
                    getSupportActionBar().setSubtitle(null);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(AccSummaryActivity.this, AddIncomeActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(AccSummaryActivity.this, AddExpenseActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(AccSummaryActivity.this, AccountsActivity.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                intent = new Intent(AccSummaryActivity.this, CategoriesActivity.class);
                startActivity(intent);
                finish();
                break;
            case 4:
                intent = new Intent(AccSummaryActivity.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 5:
                intent = new Intent(AccSummaryActivity.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 6:
                intent = new Intent(AccSummaryActivity.this, RecurringActivity.class);
                startActivity(intent);
                finish();
                break;
            case 7:
                intent = new Intent(AccSummaryActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCheckedCategories.clear();
        mCheckedAccounts.clear();
        supportInvalidateOptionsMenu();

        getSupportActionBar().setSubtitle(null);
        headerData = db.getDataHeader(mGrouping);
        summaryData = db.getDataSummary(mGrouping);
        updateData();
    }

    private void updateList() {
        mListView.setVisibility(View.GONE);
        mList.setVisibility(View.VISIBLE);

        mAccData = new Vector<AccRowData>();

        if (mSummaryList != null
                && mSummaryList.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < mSummaryList.size(); i++) {
                Summary summary = mSummaryList.get(i);
                String date[] = summary.getDate().split("-");
                String month = date[1].trim();
                String day = date[0].trim();
                day = day.length() > 1 ? day : "0" + day;
                String year = date[2].trim();
                double amount = summary.getAmount();
                mAccRowData = new AccRowData(month, day + " " + year, amount,
                        summary.getTitle(), summary.getCategory());
                if (summary.getType().equals("income")) {
                    mAccRowData.setType("income");
                } else {
                    mAccRowData.setType("expense");
                }
                mAccData.add(mAccRowData);
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        mAdapter = new AccListAdapter(this, R.layout.account_list_item, R.id.tv_amount, mAccData);
        mList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void updateData() {
        mList.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);

        List<HeaderRow> headerList = new ArrayList<HeaderRow>();
        HashMap<String, List<AccRowData>> childList = new HashMap<String, List<AccRowData>>();
        mExpListData = new HashMap<String, List<Summary>>();

        if (headerData != null
                && headerData.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < headerData.size(); i++) {
                Summary header = headerData.get(i);
                String group = header.getGroup();

                HeaderRow headerRow = new HeaderRow();

                if (mGrouping.equalsIgnoreCase("weekly")) {
                    headerRow.setDate(header.getStartDate());
                } else {
                    headerRow.setDate(group);
                }

                headerRow.setAmount((int) header.getAmount() + "");

                headerList.add(headerRow);
                List<AccRowData> mAccData = new ArrayList<AccRowData>();
                List<Summary> childData = new ArrayList<Summary>();

                for (int j = 0; j < summaryData.size(); j++) {
                    Summary child = summaryData.get(j);
                    String childGroup = child.getGroup();

                    if (childGroup.equals(group)) {
                        String date[] = child.getDate().split("-");
                        String month = date[1].trim();
                        String day = date[0].trim();
                        day = day.length() > 1 ? day : "0" + day;
                        String year = date[2].trim();
                        double amount = child.getAmount();
                        mAccRowData = new AccRowData(month, day + " " + year, amount,
                                child.getTitle(), child.getCategory());
                        if (child.getType().equals("income")) {
                            mAccRowData.setType("income");
                        } else {
                            mAccRowData.setType("expense");
                        }
                        mAccData.add(mAccRowData);
                        childData.add(child);
                    }
                }
                if (mGrouping.equalsIgnoreCase("weekly")) {
                    childList.put(header.getStartDate(), mAccData);
                    mExpListData.put(header.getStartDate(), childData);
                } else {
                    childList.put(group, mAccData);
                    mExpListData.put(group, childData);
                }
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        mAccListAdapter = new ExpandableListAdapter(this, headerList, childList);
        mListView.setAdapter(mAccListAdapter);
        mAccListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(AccSummaryActivity.this, tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                    startActivity(intent);
                    finish();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(AccSummaryActivity.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addNavigationDrawer() {
        mData = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            if (i == 4 || i == 5) {
                if (purchased) {
                    mRowData = new RowData(mListText[i], mImgResources[i], false);
                } else {
                    mRowData = new RowData(mListText[i], mImgResources[i], true);
                }
            } else {
                mRowData = new RowData(mListText[i], mImgResources[i], false);
            }
            mData.add(mRowData);
        }
        mListAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, mData);
        mNavAdapter.setAdapter(mListAdapter);
        mListAdapter.notifyDataSetChanged();
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.add_expense);
        mListText[2] = getString(R.string.accounts);
        mListText[3] = getString(R.string.categories);
        mListText[4] = getString(R.string.reports);
        mListText[5] = getString(R.string.goals);
        mListText[6] = getString(R.string.recurring);
        mListText[7] = getString(R.string.action_settings);
    }
}
