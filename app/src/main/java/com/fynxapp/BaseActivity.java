package com.fynxapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.haibison.android.lockpattern.LockPatternActivity;
import com.haibison.android.lockpattern.util.Settings;


public class BaseActivity extends ActionBarActivity {
    private static final int REQ_ENTER_PATTERN = 2;
    private char[] savedPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        savedPattern = Settings.Security.getPattern(this);
        if (savedPattern != null) {
            Intent intent = new Intent(LockPatternActivity.ACTION_COMPARE_PATTERN, null,
                    this, LockPatternActivity.class);
            intent.putExtra(LockPatternActivity.EXTRA_PATTERN, savedPattern);
            startActivityForResult(intent, REQ_ENTER_PATTERN);
        } else {
            startActivity(new Intent(BaseActivity.this, OverviewActivity.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_ENTER_PATTERN: {
                switch (resultCode) {
                    case RESULT_OK:
                        startActivity(new Intent(BaseActivity.this, OverviewActivity.class));
                        finish();
                        break;
                    case RESULT_CANCELED:
                        break;
                    case LockPatternActivity.RESULT_FAILED:
                        break;
                    case LockPatternActivity.RESULT_FORGOT_PATTERN:
                        break;
                }
                int retryCount = data.getIntExtra(
                        LockPatternActivity.EXTRA_RETRY_COUNT, 0);

                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
