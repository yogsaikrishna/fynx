package com.fynxapp;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.SimpleSpinnerAdapter;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;


public class AddRecurring extends ActionBarActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String TAG = "AddIncomeActivity";

    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_accounts, R.drawable.ic_categories, R.drawable.ic_summary,
            R.drawable.ic_reports, R.drawable.ic_action_objective,
            R.drawable.ic_action_settings};

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mNavAdapter;
    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private String[] mTileColors;
    private List<Category> mCategories;
    private List<Account> mAccounts;
    private Spinner mSpCategory, mSpAccount, mSpFrequency;
    private EditText mDate, mTitle, mDescription;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrDate, mHdrCategory, mHdrAccount, mHdrFrequency, mHdrAmount;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Amount mEditRecurring, mRecurring;
    private RadioButton mIncome, mExpense;
    private RadioGroup mRadioGroup;
    private String mType;
    private List<String> mFreqList;
    private int year, month, date;
    private int editPosition = 0;
    private String mNextDate;
    private NumberFormat nf;
    private boolean purchased;
    private IabHelper mHelper;
    private String daily, weekly, monthly, yearly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_recurring);

        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        db = new DBHelper(this);
        mAccounts = db.getAccountsList();

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        daily = getString(R.string.daily);
        weekly = getString(R.string.weekly);
        monthly = getString(R.string.monthly);
        yearly = getString(R.string.yearly);

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditRecurring = db.getRecurringById(id);
                getSupportActionBar().setTitle(getString(R.string.edit_recurring));
                mType = mEditRecurring.getType();
                mCategories = db.getCategoriesByType(mType);
                String tmpDate[] = mEditRecurring.getDate().split("-");
                this.date = Integer.parseInt(tmpDate[0]);
                this.month = DateUtils.getMonth(tmpDate[1]);
                this.year = Integer.parseInt(tmpDate[2]);
            }
        } else {
            mCategories = db.getCategoriesByType("income");
            mType = "income";
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mTileColors = getResources().getStringArray(R.array.tile_colors);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mSpCategory = (Spinner) this.findViewById(R.id.sp_categories);
        mSpAccount = (Spinner) this.findViewById(R.id.sp_account);
        mSpFrequency = (Spinner) this.findViewById(R.id.sp_frequency);
        mDate = (EditText) this.findViewById(R.id.et_date);
        mTitle = (EditText) this.findViewById(R.id.et_title);
        mDescription = (EditText) this.findViewById(R.id.et_description);
        mAmount = (TextView) this.findViewById(R.id.tv_amount);

        mHdrDetails = (TextView) this.findViewById(R.id.hdr_details);
        mHdrDate = (TextView) this.findViewById(R.id.hdr_date);
        mHdrCategory = (TextView) this.findViewById(R.id.hdr_category);
        mHdrAccount = (TextView) this.findViewById(R.id.hdr_account);
        mHdrFrequency = (TextView) this.findViewById(R.id.hdr_frequency);
        mHdrAmount = (TextView) this.findViewById(R.id.hdr_amount);

        mIncome = (RadioButton) this.findViewById(R.id.rb_income);
        mExpense = (RadioButton) this.findViewById(R.id.rb_expense);
        mRadioGroup = (RadioGroup) this.findViewById(R.id.radiogroup);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        updateCategoryList();

        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();

        if (mAccounts != null
                && mAccounts.size() > 0) {
            for (int i = 0; i < mAccounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String account = mAccounts.get(i).getName();
                mRowData = new RowData(account, "" + account.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, mData);
            mSpAccount.setAdapter(mListAdapter);
        }

        updateFrequency();

        mDate.setText("Today");
        mDate.setFocusable(false);

        mDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrDate.setTypeface(mRobotoLight);
        mHdrCategory.setTypeface(mRobotoLight);
        mHdrAccount.setTypeface(mRobotoLight);
        mHdrFrequency.setTypeface(mRobotoLight);
        mHdrAmount.setTypeface(mRobotoLight);

        mIncome.setTypeface(mRoboto);
        mExpense.setTypeface(mRoboto);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mRadioGroup.check(R.id.rb_income);

        mSpCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(AddRecurring.this, GenericList.class);
                    intent.putExtra("type", mType);
                    startActivityForResult(intent, 0);
                }
                return true;
            }
        });

        mSpAccount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(AddRecurring.this, GenericList.class);
                    intent.putExtra("type", "account");
                    startActivityForResult(intent, 1);
                }
                return true;
            }
        });

        npb = new NumberPickerBuilder()
                .setFragmentManager(getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditRecurring.getAmount()) + " " + currency);
            mTitle.setText(mEditRecurring.getTitle());
            mDescription.setText(mEditRecurring.getDescription());
            mDate.setText(mEditRecurring.getDate());
            mSpCategory.setSelection(getCategoryIndex(mEditRecurring.getCategory()));
            mSpAccount.setSelection(getAccountIndex(mEditRecurring.getAccount()));
            mSpFrequency.setSelection(getFrequencyIndex(mEditRecurring.getFrequency()));
            if (mType.equals("income")) {
                mRadioGroup.check(R.id.rb_income);
            } else {
                mRadioGroup.check(R.id.rb_expense);
            }
        } else {
            npb.show();
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_income:
                        mType = "income";
                        break;
                    case R.id.rb_expense:
                        mType = "expense";
                        break;
                }
                mCategories = db.getCategoriesByType(mType);
                updateCategoryList();
            }
        });

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_recurring, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String date = mDate.getText().toString();
                int posCategory = mSpCategory.getSelectedItemPosition();
                int posAccount = mSpAccount.getSelectedItemPosition();
                int posFrequency = mSpFrequency.getSelectedItemPosition();
                String category = null;
                String account = null;

                if (mCategories != null
                        && mCategories.size() > 0) {
                    category = mCategories.get(posCategory).getName();
                }
                if (mAccounts != null
                        && mAccounts.size() > 0) {
                    account = mAccounts.get(posAccount).getName();
                }

                if (amount.startsWith("0.00")) {
                    Toast.makeText(this, getString(R.string.err_amt), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (title.equals("")) {
                    Toast.makeText(this, getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (date.equals(getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        date = df.format(today.getTime());
                        String tmpDate[] = date.split("-");
                        this.date = Integer.parseInt(tmpDate[0]);
                        this.month = DateUtils.getMonth(tmpDate[1]);
                        this.year = Integer.parseInt(tmpDate[2]);
                    }
                    mRecurring = new Amount();
                    mRecurring.setCategory(category);
                    mRecurring.setAccount(account);
                    mRecurring.setTitle(title);
                    mRecurring.setDescription(description);
                    mRecurring.setDate(date);
                    try {
                        mRecurring.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {
                        Log.d(TAG, "Exception occurred: " + e.toString());
                    }
                    mRecurring.setType(mType);
                    mRecurring.setFrequency(getFrequency(posFrequency));
                    mRecurring.setUpdatedDate(date);
                    if (mEdit) {
                        mRecurring.setId(mEditRecurring.getId());
                        mRecurring.setRecurringId(mEditRecurring.getId());
                        showUpdateDialog(mRecurring);
                    } else {
                        long _id = db.saveRecurring(mRecurring);
                        mRecurring.setRecurringId((int) _id);
                        addRecurringToDate(mRecurring);
                    }
                }
                break;
            case R.id.cancel:
                this.finish();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setMessage(getString(R.string.info_delete))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteRecurringById(mEditRecurring.getId());
                                AlarmHelper.cancelRecurringAlarm(AddRecurring.this, mEditRecurring.getId());
                                AddRecurring.this.finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavAdapter = (ListView) findViewById(R.id.left_drawer);
        initializeNavList();
        addNavigationDrawer();
        mNavAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_date:
                DatePickerBuilder dpb = new DatePickerBuilder()
                        .setFragmentManager(getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                dpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(AddRecurring.this, AddIncomeActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(AddRecurring.this, AddExpenseActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(AddRecurring.this, AccountsActivity.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                intent = new Intent(AddRecurring.this, CategoriesActivity.class);
                startActivity(intent);
                finish();
                break;
            case 4:
                intent = new Intent(AddRecurring.this, AccSummaryActivity.class);
                startActivity(intent);
                finish();
                break;
            case 5:
                intent = new Intent(AddRecurring.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 6:
                intent = new Intent(AddRecurring.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 7:
                intent = new Intent(AddRecurring.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        this.year = year;
        this.month = month + 1;
        this.date = date;

        mDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0
                && resultCode == RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpCategory.setSelection(position);
        } else if (requestCode == 1
                && resultCode == RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpAccount.setSelection(position);
        } else if (requestCode == 10001) {
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private int getCategoryIndex(String category) {
        int index = 0;
        if (mCategories != null) {
            for (int i = 0; i < mCategories.size(); i++) {
                if (mCategories.get(i).getName().equals(category)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getAccountIndex(String account) {
        int index = 0;
        if (mAccounts != null) {
            for (int i = 0; i < mAccounts.size(); i++) {
                if (mAccounts.get(i).getName().equals(account)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getFrequencyIndex(String frequency) {
        int index = 0;
        if (mFreqList != null) {
            for (int i = 0; i < mFreqList.size(); i++) {
                if (mFreqList.get(i).equals(frequency)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private String getFrequency(int position) {
        switch (position) {
            case 0:
                return "Daily";
            case 1:
                return "Weekly";
            case 2:
                return "Monthly";
            case 4:
                return "Yearly";
        }
        return "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void updateCategoryList() {
        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();

        if (mCategories != null
                && mCategories.size() > 0) {
            for (int i = 0; i < mCategories.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String category = mCategories.get(i).getName();
                mRowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, mData);
            mSpCategory.setAdapter(mListAdapter);
            mListAdapter.notifyDataSetChanged();
        }
    }

    private void updateFrequency() {
        mFreqList = new ArrayList<String>();
        mFreqList.add(daily);
        mFreqList.add(weekly);
        mFreqList.add(monthly);
        mFreqList.add(yearly);
        SimpleSpinnerAdapter mFreqAdapter = new SimpleSpinnerAdapter(this,R.layout.simple_spinner_item,
                mFreqList);
        mFreqAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpFrequency.setAdapter(mFreqAdapter);
        mSpFrequency.setSelection(2);
    }

    private void setAlarm(String frequency, int id) {
        long interval = AlarmManager.INTERVAL_DAY * 30;

        if (frequency.equalsIgnoreCase("daily")) {
            interval = AlarmManager.INTERVAL_DAY;
        } else if (frequency.equalsIgnoreCase("weekly")) {
            interval = AlarmManager.INTERVAL_DAY * 7;
        } else if (frequency.equalsIgnoreCase("monthly")) {
            interval = AlarmManager.INTERVAL_DAY * 30;
        } else if (frequency.equalsIgnoreCase("yearly")) {
            interval = AlarmManager.INTERVAL_DAY * 365;
        }

        AlarmHelper.setRecurringAlarm(this, interval, id, mNextDate);
        mRecurring.setId(id);
        mRecurring.setDate(mRecurring.getUpdatedDate());
        mRecurring.setUpdatedDate(mNextDate);
        db.saveRecurring(mRecurring);
    }

    private void addRecurringToDate(Amount recurring) {
        String[] today = DateUtils.getTodayDate(Constants.DD_MM_YYYY).split("-");

        int currDate = Integer.parseInt(today[0]);
        int currMonth = Integer.parseInt(today[1]);
        int currYear = Integer.parseInt(today[2]);

        int iterations = 0;

        String frequency = recurring.getFrequency();

        if (frequency.equalsIgnoreCase("daily")) {
            if (this.year == currYear) {
                iterations -= this.date;
                if (this.month < currMonth) {
                    for (int m = this.month; m < currMonth; m++) {
                        iterations += DateUtils.getDaysInMonth(m, currYear);
                    }
                } else if (this.month == currMonth) {
                    iterations += currDate;
                }
            } else if (this.year < currYear) {
                for (int y = this.year; y < currYear; y++) {
                    iterations -= this.date;
                    for (int m = this.month; m <= 12; m++) {
                        iterations += DateUtils.getDaysInMonth(m, y);
                    }
                }
                for (int m = 1; m < currMonth; m++) {
                    iterations += DateUtils.getDaysInMonth(m, currYear);
                }
            }
        } else if (frequency.equalsIgnoreCase("weekly")) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DATE, this.date);
            calendar.set(Calendar.MONTH, this.month - 1);
            calendar.set(Calendar.YEAR, this.year);
            int week = calendar.get(Calendar.WEEK_OF_YEAR);

            calendar.set(Calendar.DATE, currDate);
            calendar.set(Calendar.MONTH, currMonth - 1);
            calendar.set(Calendar.YEAR, currYear);
            int currWeek = calendar.get(Calendar.WEEK_OF_YEAR);

            if (this.year == currYear
                    && week < currWeek) {
                iterations = currWeek - week + 1;
            } else if (this.year < currYear) {
                iterations -= week;
                for (int y = this.year; y < currYear; y++) {
                    iterations += 52;
                }
                iterations += currWeek + 1;
            }

        } else if (frequency.equalsIgnoreCase("monthly")) {
            if (this.year == currYear
                    && this.month < currMonth) {
                iterations = currMonth - this.month + 1;
            } else if (this.year < currYear) {
                for (int y = this.year; y < currYear; y++) {
                    for (int m = this.month; m <= 12; m++) {
                        iterations += 1;
                    }
                }
                iterations += currMonth;
            }
        } else if (frequency.equalsIgnoreCase("yearly")) {
            for (int y = this.year; y < currYear; y++) {
                iterations += 1;
            }
            iterations = 1;
        }
        updateData(recurring, iterations, frequency);
        Log.d(TAG, "Iterations: " + iterations);
    }

    private void updateData(final Amount recurring, final int iterations,
                            final String frequency) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String dbDate = null;
                for (int i = 0; i < iterations; i++) {
                    recurring.setId(-1);
                    if (recurring.getType().equalsIgnoreCase("income")) {
                        db.saveIncome(recurring);
                    } else {
                        db.saveExpense(recurring);
                    }
                    String date = DateUtils.convertStringToDate(recurring.getDate());
                    db.updateGoals(date, recurring.getAmount(), recurring.getType());
                    dbDate = getNextSyncDate(frequency, recurring.getDate());
                    recurring.setDate(dbDate);
                }
                mNextDate = dbDate == null ? mRecurring.getDate() : dbDate;
                setAlarm(mRecurring.getFrequency(), recurring.getRecurringId());
                AddRecurring.this.finish();
            }
        }).start();
    }

    private String getNextSyncDate(String frequency, String dbDate) {
        String[] tmpDate = dbDate.split("-");

        int date = Integer.parseInt(tmpDate[0]);
        int month = DateUtils.getMonth(tmpDate[1]);
        int year = Integer.parseInt(tmpDate[2]);

        if (frequency != null) {
            if (frequency.equalsIgnoreCase("daily")) {
                date += 1;
                if (month == 2 && (date > 28)) {
                    if (date > 29) {
                        date = 1;
                        month += 1;
                    }
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date = 1;
                    month += 1;
                } else if (date > 31) {
                    date = 1;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("weekly")) {
                date += 7;
                if (month == 2 && (date > 28)) {
                    date -= 28;
                    month += 1;
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date -= 30;
                    month += 1;
                } else if (date > 31) {
                    date -= 31;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("monthly")) {
                month += 1;
                if (month == 2 && (date > 28)) {
                    date = 28;
                }
                if (month > 12) {
                    month = 1;
                    year += 1;
                }
            } else if (frequency.equalsIgnoreCase("yearly")) {
                year += 1;
            }
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        calendar.set(Calendar.DATE, date);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        return df.format(calendar.getTime());
    }

    private void showUpdateDialog(final Amount recurring) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddRecurring.this);
        builder.setTitle(getString(R.string.update_from))
                .setSingleChoiceItems(R.array.update_from, 0,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                editPosition = i;
                            }
                        })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AddRecurring.this.finish();
                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch (editPosition) {
                            case 0:
                                boolean update = db.updateAmountByRecurringId(recurring.getRecurringId(),
                                        recurring.getType());
                                if (update) {
                                    addRecurringToDate(recurring);
                                }
                                break;
                            case 1:
                                mNextDate = mEditRecurring.getUpdatedDate();
                                setAlarm(mRecurring.getFrequency(), mEditRecurring.getId());
                                AddRecurring.this.finish();
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(AddRecurring.this, tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                    startActivity(intent);
                    finish();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(AddRecurring.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    private void addNavigationDrawer() {
        mData = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            if (i == 5 || i == 6) {
                if (purchased) {
                    mRowData = new RowData(mListText[i], mImgResources[i], false);
                } else {
                    mRowData = new RowData(mListText[i], mImgResources[i], true);
                }
            } else {
                mRowData = new RowData(mListText[i], mImgResources[i], false);
            }
            mData.add(mRowData);
        }
        mListAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, mData);
        mNavAdapter.setAdapter(mListAdapter);
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.add_expense);
        mListText[2] = getString(R.string.accounts);
        mListText[3] = getString(R.string.categories);
        mListText[4] = getString(R.string.title_activity_acc_summary);
        mListText[5] = getString(R.string.reports);
        mListText[6] = getString(R.string.goals);
        mListText[7] = getString(R.string.action_settings);
    }
}