package com.fynxapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddCategory;
import com.fynxapp.R;
import com.fynxapp.categories.CategoryExpense;
import com.fynxapp.categories.CategoryIncome;
import com.fynxapp.constants.Constants;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.TypefaceSpan;

import java.util.Vector;

public class CategoriesFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private static String[] titles;
    private PagerAdapter pagerAdapter;
    private PagerTabStrip pagerTabStrip;
    private ViewPager viewPager;
    private Typeface mRoboto;
    private ActionBarActivity mActivity;

    public static CategoriesFragment newInstance(int index) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public CategoriesFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        mActivity = (ActionBarActivity) getActivity();
        titles = new String[2];
        titles[0] = mActivity.getString(R.string.income);
        titles[1] = mActivity.getString(R.string.expense);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories, container, false);

        pagerAdapter = new PagerAdapter(getChildFragmentManager());

        pagerTabStrip = (PagerTabStrip) view.findViewById(R.id.pager_title_strip);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        pagerTabStrip.setTabIndicatorColorResource(R.color.tab);
        for (int i = 0; i < pagerTabStrip.getChildCount(); i++) {
            View nextChild = pagerTabStrip.getChildAt(i);
            if (nextChild instanceof TextView) {
                TextView textView = (TextView) nextChild;
                textView.setTypeface(mRoboto);
            }
        }

        SpannableString s = new SpannableString(mActivity.getString(R.string.categories));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.create) {
            startActivity(new Intent(getActivity(), AddCategory.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.categories, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public static class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = null;
            switch (i) {
                case 0:
                    fragment = new CategoryIncome();
                    return fragment;
                case 1:
                    fragment = new CategoryExpense();
                    return fragment;
                default:
                    fragment = new CategoryIncome();
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
