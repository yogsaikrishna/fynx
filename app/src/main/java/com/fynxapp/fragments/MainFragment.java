package com.fynxapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.utils.TypefaceSpan;

public class MainFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private ActionBarActivity mActivity;

    public static MainFragment newInstance(int index) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SpannableString s = new SpannableString(mActivity.getString(R.string.overview));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return inflater.inflate(R.layout.fragment_main, container, false);
    }
}
