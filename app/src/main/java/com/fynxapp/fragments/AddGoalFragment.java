package com.fynxapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

public class AddGoalFragment extends Fragment implements View.OnClickListener,
        DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String ARG_INDEX = "index";
    private static final String TAG = "AddGoal";

    private ActionBarActivity mActivity;
    private EditText mTitle, mDescription, mStartDate, mEndDate;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrStartDate, mHdrAmount, mHdrEndDate;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Goal mEditGoal, mGoal;
    private RadioButton mIncome, mExpense;
    private RadioGroup mRadioGroup;
    private String mType;
    private NumberFormat nf;

    public static AddGoalFragment newInstance(int index) {
        AddGoalFragment fragment = new AddGoalFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public AddGoalFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mBundle = getArguments();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditGoal = db.getGoalById(id);
                String s = mActivity.getString(R.string.edit_goal);
                mActivity.getSupportActionBar().setTitle(CommonsUtils.getSpannableString(mActivity, s));
                mType = mEditGoal.getType();
            }
        } else {
            mType = "income";
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_goal, container, false);
        mTitle = (EditText) view.findViewById(R.id.et_title);
        mDescription = (EditText) view.findViewById(R.id.et_description);
        mAmount = (TextView) view.findViewById(R.id.tv_amount);
        mStartDate = (EditText) view.findViewById(R.id.et_start_date);
        mEndDate = (EditText) view.findViewById(R.id.et_end_date);

        mHdrDetails = (TextView) view.findViewById(R.id.hdr_details);
        mHdrStartDate = (TextView) view.findViewById(R.id.hdr_start_date);
        mHdrAmount = (TextView) view.findViewById(R.id.hdr_goal_amount);
        mHdrEndDate = (TextView) view.findViewById(R.id.hdr_end_date);

        mIncome = (RadioButton) view.findViewById(R.id.rb_income);
        mExpense = (RadioButton) view.findViewById(R.id.rb_expense);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.radiogroup);

        mStartDate.setText(mActivity.getString(R.string.today));
        mStartDate.setFocusable(false);
        mEndDate.setFocusable(false);

        mStartDate.setOnClickListener(this);
        mEndDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mStartDate.setTypeface(mRoboto);
        mEndDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrStartDate.setTypeface(mRobotoLight);
        mHdrAmount.setTypeface(mRobotoLight);
        mHdrEndDate.setTypeface(mRobotoLight);

        mIncome.setTypeface(mRoboto);
        mExpense.setTypeface(mRoboto);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mRadioGroup.check(R.id.rb_income);

        npb = new NumberPickerBuilder()
                .setFragmentManager(mActivity.getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setTargetFragment(this)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditGoal.getAmount()) + " " + currency);
            mTitle.setText(mEditGoal.getTitle());
            mDescription.setText(mEditGoal.getDescription());
            mStartDate.setText(mEditGoal.getStartDate());
            mEndDate.setText(mEditGoal.getEndDate());
            if (mType.equals("income")) {
                mRadioGroup.check(R.id.rb_income);
            } else {
                mRadioGroup.check(R.id.rb_expense);
            }
            mIncome.setClickable(false);
            mExpense.setClickable(false);
        } else {
            npb.show();
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_income:
                        mType = "income";
                        break;
                    case R.id.rb_expense:
                        mType = "expense";
                        break;
                }
            }
        });

        SpannableString s = new SpannableString(mActivity.getString(R.string.title_activity_add_goal));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_goal, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Fragment fragment = new GoalsFragment();
        final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String startDate = mStartDate.getText().toString();
                String endDate = mEndDate.getText().toString();

                if (amount.startsWith("0.00")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_amt), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (title.equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (endDate.equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_goal_end_dt),
                            Toast.LENGTH_SHORT).show();
                    return true;
                } else if (DateUtils.getDaysBetweenDates(startDate, endDate) <= 0) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_end_start_dt),
                            Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (startDate.equals(mActivity.getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        startDate = df.format(today.getTime());
                    }
                    mGoal = new Goal();
                    mGoal.setTitle(title);
                    mGoal.setDescription(description);
                    mGoal.setStartDate(startDate);
                    mGoal.setEndDate(endDate);
                    try {
                        mGoal.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {
                        Log.d(TAG, "Exception occurred: " + e.toString());
                    }
                    mGoal.setType(mType);
                    if (mEdit) {
                        mGoal.setId(mEditGoal.getId());
                        mGoal.setGoalPercent(mEditGoal.getGoalPercent());
                        mGoal.setGoalAmount(mEditGoal.getGoalAmount());
                        mGoal.setCreatedDate(mEditGoal.getCreatedDate());
                    }
                    db.saveGoal(mGoal);
                }
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.cancel:
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(mActivity)
                        .setMessage(mActivity.getString(R.string.info_delete))
                        .setPositiveButton(mActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteGoalById(mEditGoal.getId());
                                ft.replace(R.id.detail_fragment, fragment);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                ft.commit();
                            }
                        })
                        .setNegativeButton(mActivity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        switch (i) {
            case R.id.et_start_date:
                mStartDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
                break;
            case R.id.et_end_date:
                mEndDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
                break;
        }
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_start_date:
                DatePickerBuilder sdpb = new DatePickerBuilder()
                        .setFragmentManager(mActivity.getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                sdpb.setReference(R.id.et_start_date)
                .setTargetFragment(this);
                sdpb.show();
                break;
            case R.id.et_end_date:
                DatePickerBuilder edpb = new DatePickerBuilder()
                        .setFragmentManager(mActivity.getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                edpb.setReference(R.id.et_end_date)
                .setTargetFragment(this);
                edpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
