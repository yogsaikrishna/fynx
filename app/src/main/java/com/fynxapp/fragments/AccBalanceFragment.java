package com.fynxapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;

import java.text.NumberFormat;
import java.util.List;

public class AccBalanceFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private TextView mHdrAccounts, mHdrBalance;
    private TextView mTotal, mAmtTotal;
    private TextView mNoData;
    private LinearLayout mAccLayout;
    private DBHelper db;
    private ActionBarActivity mActivity;
    private Typeface mRoboto;
    private SharedPreferences mSharedPreferences;
    private String mCurrency;
    private List<Account> mAccounts;

    public static AccBalanceFragment newInstance(int index) {
        AccBalanceFragment fragment = new AccBalanceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public AccBalanceFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.acc_balance, container, false);

        mHdrAccounts = (TextView) view.findViewById(R.id.tv_hdr_accounts);
        mHdrBalance = (TextView) view.findViewById(R.id.tv_hdr_balance);
        mTotal = (TextView) view.findViewById(R.id.tv_total);
        mAmtTotal = (TextView) view.findViewById(R.id.tv_amt_total);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);
        mAccLayout = (LinearLayout) view.findViewById(R.id.account_layout);

        mHdrAccounts.setTypeface(mRoboto);
        mHdrBalance.setTypeface(mRoboto);
        mTotal.setTypeface(mRoboto);
        mAmtTotal.setTypeface(mRoboto);
        mNoData.setTypeface(mRoboto);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mCurrency = mSharedPreferences.getString("data_currency", "\u0024");

        if (mAccounts != null
                && mAccounts.size() > 0) {
            mAccLayout.removeViews(1, mAccounts.size());
        }
        mAccounts = db.getAccountsList();

        double total = 0;
        if (mAccounts != null
                && mAccounts.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < mAccounts.size(); i++) {
                Account account = mAccounts.get(i);

                View accView = View.inflate(getActivity(), R.layout.account_item, null);
                TextView accName = (TextView) accView.findViewById(R.id.tv_acc_name);
                TextView accBalance = (TextView) accView.findViewById(R.id.tv_acc_balance);

                accName.setTypeface(mRoboto);
                accBalance.setTypeface(mRoboto);

                double balance = db.getAccountBalance(account.getName());
                total += balance;

                accName.setText(account.getName());
                accBalance.setText(Html.fromHtml("" + nf.format(balance) +
                        " <small>" + mCurrency + "</small>"));
                mAccLayout.addView(accView);
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
        mAmtTotal.setText(Html.fromHtml("" + nf.format(total) +
                " <small>" + mCurrency + "</small>"));
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
