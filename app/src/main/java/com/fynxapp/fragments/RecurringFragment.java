package com.fynxapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddRecurring;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.TypefaceSpan;

import java.util.List;
import java.util.Random;
import java.util.Vector;

public class RecurringFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private String[] tileColors;
    private ListView listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter listAdapter;
    private TextView mNoData;
    private DBHelper db;
    private List<Amount> recurringList;
    private Typeface mRoboto;
    private ActionBarActivity mActivity;

    public static RecurringFragment newInstance(int index) {
        RecurringFragment fragment = new RecurringFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public RecurringFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(getActivity());
        recurringList = db.getRecurringList();

        tileColors = getResources().getStringArray(R.array.tile_colors);

        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recurring, container, false);

        listView = (ListView) view.findViewById(R.id.listView);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);

        mNoData.setTypeface(mRoboto);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("edit", true);
                bundle.putInt("id", recurringList.get(i).getId());
                Fragment fragment = new AddRecurringFragment();
                fragment.setArguments(bundle);
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        });

        SpannableString s = new SpannableString(mActivity.getString(R.string.recurring));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.create) {
            Fragment fragment = new AddRecurringFragment();
            FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.detail_fragment, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.recurring, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        recurringList = db.getRecurringList();

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        data = new Vector<RowData>();

        if (recurringList != null
                && recurringList.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < recurringList.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = recurringList.get(i).getTitle();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(mActivity, R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
