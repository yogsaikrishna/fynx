package com.fynxapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fynxapp.R;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;

import java.util.Vector;

public class ReportsListFragment extends Fragment implements AdapterView.OnItemClickListener {
    private OnReportSelectionListener mListener;

    public ReportsListFragment() {
    }
    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_action_pie_dark, R.drawable.ic_action_line_dark,
            R.drawable.ic_action_bar_dark};
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter navAdapter;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListText = getActivity().getResources().getStringArray(R.array.reports_list);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reports_list, container, false);
        listView = (ListView) view.findViewById(R.id.left_drawer);
        data = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            rowData = new RowData(mListText[i], mImgResources[i], false);
            data.add(rowData);
        }
        navAdapter = new ListAdapter(getActivity(), R.layout.report_list_item, R.id.tv_primary, data);

        listView.setAdapter(navAdapter);
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnReportSelectionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (null != mListener) {
            mListener.onReportSelection(position);
            listView.setItemChecked(position, true);
        }
    }

    public interface OnReportSelectionListener {
        public void onReportSelection(int position);
    }

}
