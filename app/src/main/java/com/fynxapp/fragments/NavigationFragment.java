package com.fynxapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fynxapp.R;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class NavigationFragment extends Fragment implements AdapterView.OnItemClickListener {
    private OnFragmentInteractionListener mListener;

    public NavigationFragment() {
    }
    private String[] mListText;
    private Integer[] imgResources = {R.drawable.ic_action_overview, R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_accounts, R.drawable.ic_categories, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_objective, R.drawable.ic_action_recurring};
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter navAdapter;
    private ListView listView;
    private boolean purchased;
    private IabHelper mHelper;
    private ActionBarActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        purchased = CommonsUtils.getPrefBoolean(getActivity(), "purchased");
        mActivity = (ActionBarActivity) getActivity();
        initializeNavList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nav_list_layout, container, false);
        listView = (ListView) view.findViewById(R.id.left_drawer);
        addNavigationDrawer();
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (null != mListener) {
            mListener.onFragmentInteraction(position);
            listView.setItemChecked(position, true);
//            if (purchased) {
//                mListener.onFragmentInteraction(position);
//                listView.setItemChecked(position, true);
//            } else {
//                if (position == 6 || position == 7) {
//                    startIAP();
//                } else {
//                    mListener.onFragmentInteraction(position);
//                    listView.setItemChecked(position, true);
//                }
//            }
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(int position);
    }

    private void addNavigationDrawer() {
        data = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            if (i == 6 || i == 7) {
                if (purchased) {
                    rowData = new RowData(mListText[i], imgResources[i], false);
                } else {
                    rowData = new RowData(mListText[i], imgResources[i], false);
                }
            } else {
                rowData = new RowData(mListText[i], imgResources[i], false);
            }
            data.add(rowData);
        }
        navAdapter = new ListAdapter(getActivity(), R.layout.nav_list_item, R.id.tv_primary, data);
        listView.setAdapter(navAdapter);
        navAdapter.notifyDataSetChanged();
    }

    private void startIAP() {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(getActivity(), base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    if (result.getMessage().contains("Item Already Owned")) {
                        Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                        tmp.put("purchased", true);
                        CommonsUtils.putPrefBooleans(mActivity, tmp);
                        addNavigationDrawer();
                    }
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(getActivity(), tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                } else {
                    mHelper.launchPurchaseFlow(getActivity(), SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    private void initializeNavList() {
        mListText = new String[9];
        mListText[0] = mActivity.getString(R.string.overview);
        mListText[1] = mActivity.getString(R.string.add_income);
        mListText[2] = mActivity.getString(R.string.add_expense);
        mListText[3] = mActivity.getString(R.string.accounts);
        mListText[4] = mActivity.getString(R.string.categories);
        mListText[5] = mActivity.getString(R.string.title_activity_acc_summary);
        mListText[6] = mActivity.getString(R.string.reports);
        mListText[7] = mActivity.getString(R.string.goals);
        mListText[8] = mActivity.getString(R.string.recurring);
    }
}
