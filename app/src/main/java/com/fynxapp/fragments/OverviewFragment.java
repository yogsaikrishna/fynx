package com.fynxapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;

import java.text.NumberFormat;

public class OverviewFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private TextView mBalance, mAmtBalance;
    private TextView mExpense, mAmtExpense;
    private TextView mIncome, mAmtIncome;
    private TextView mOverview, mGroup;
    private TextView mRollover, mAmtRollover;
    private TextView mPrevious, mAmtPrevious;
    private TextView mTotal, mAmtTotal;
    private DBHelper db;
    private ActionBarActivity mActivity;
    private Typeface mRoboto;
    private SharedPreferences mSharedPreferences;
    private String mCurrency, mGrouping;

    public static OverviewFragment newInstance(int index) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public OverviewFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amount_overview, container, false);

        mGroup = (TextView) view.findViewById(R.id.tv_group);
        mOverview = (TextView) view.findViewById(R.id.tv_hdr_overview);
        mIncome = (TextView) view.findViewById(R.id.tv_income);
        mAmtIncome = (TextView) view.findViewById(R.id.tv_amt_income);
        mExpense = (TextView) view.findViewById(R.id.tv_expense);
        mAmtExpense = (TextView) view.findViewById(R.id.tv_amt_expense);
        mBalance = (TextView) view.findViewById(R.id.tv_balance);
        mAmtBalance = (TextView) view.findViewById(R.id.tv_amt_balance);
        mPrevious = (TextView) view.findViewById(R.id.tv_previous);
        mAmtPrevious = (TextView) view.findViewById(R.id.tv_amt_previous);
        mTotal = (TextView) view.findViewById(R.id.tv_total);
        mAmtTotal = (TextView) view.findViewById(R.id.tv_amt_total);

        mGroup.setTypeface(mRoboto);
        mOverview.setTypeface(mRoboto);
        mIncome.setTypeface(mRoboto);
        mAmtIncome.setTypeface(mRoboto);
        mExpense.setTypeface(mRoboto);
        mAmtExpense.setTypeface(mRoboto);
        mBalance.setTypeface(mRoboto);
        mAmtBalance.setTypeface(mRoboto);
        mPrevious.setTypeface(mRoboto);
        mAmtPrevious.setTypeface(mRoboto);
        mTotal.setTypeface(mRoboto);
        mAmtTotal.setTypeface(mRoboto);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mCurrency = mSharedPreferences.getString("data_currency", "\u0024");
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");

        if (mGrouping.equalsIgnoreCase("weekly")) {
            mGroup.setText(mActivity.getString(R.string.this_week));
        } else if (mGrouping.equalsIgnoreCase("monthly")) {
            mGroup.setText(mActivity.getString(R.string.this_month));
        } else {
            mGroup.setText(mActivity.getString(R.string.this_year));
        }


        double totalIncome = db.getTotalIncome(mGrouping);
        double totalExpense = db.getTotalExpense(mGrouping);
        double rollover = db.getRollOverBalance();
        double balance = totalIncome - totalExpense;
        double total = balance + rollover;

        mAmtIncome.setText(Html.fromHtml("" + nf.format(totalIncome)
                + " <small>" + mCurrency + "</small>"));
        mAmtExpense.setText(Html.fromHtml("" + nf.format(totalExpense) +
                " <small>" + mCurrency + "</small>"));
        mAmtBalance.setText(Html.fromHtml("" + nf.format(balance) +
                " <small>" + mCurrency + "</small>"));
        mAmtPrevious.setText(Html.fromHtml("" + nf.format(rollover) +
                " <small>" + mCurrency + "</small>"));
        mAmtTotal.setText(Html.fromHtml("" + nf.format(total) +
                " <small>" + mCurrency + "</small>"));

    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
