package com.fynxapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.GenericList;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.SimpleSpinnerAdapter;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.widget.FynxWidget;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class AddIncomeFragment extends Fragment implements View.OnClickListener,
        DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String ARG_INDEX = "index";

    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private String[] mTileColors;
    private List<Category> mIncomeCategories;
    private List<Account> mAccounts;
    private List<String> mTitleList;
    private Spinner mSpCategory, mSpAccount;
    private EditText mDate, mDescription;
    private AutoCompleteTextView mTitle;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrDate, mHdrCategory, mHdrAccount, mHdrIncome;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Amount mEditIncome;
    private NumberFormat nf;
    private ActionBarActivity mActivity;

    public static AddIncomeFragment newInstance(int index) {
        AddIncomeFragment fragment = new AddIncomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public AddIncomeFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);
        mIncomeCategories = db.getCategoriesByType("income");
        mAccounts = db.getAccountsList();
        mTitleList = db.getTitlesList("income");

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mBundle = getArguments();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditIncome = db.getIncomeById(id);
                String s = mActivity.getString(R.string.edit_income);
                mActivity.getSupportActionBar().setTitle(CommonsUtils.getSpannableString(mActivity, s));
            }
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mTileColors = getResources().getStringArray(R.array.tile_colors);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_income, container, false);
        mSpCategory = (Spinner) view.findViewById(R.id.sp_categories);
        mSpAccount = (Spinner) view.findViewById(R.id.sp_account);
        mDate = (EditText) view.findViewById(R.id.et_date);
        mTitle = (AutoCompleteTextView) view.findViewById(R.id.at_title);
        mDescription = (EditText) view.findViewById(R.id.et_description);
        mAmount = (TextView) view.findViewById(R.id.tv_amount);

        mHdrDetails = (TextView) view.findViewById(R.id.hdr_details);
        mHdrDate = (TextView) view.findViewById(R.id.hdr_date);
        mHdrCategory = (TextView) view.findViewById(R.id.hdr_category);
        mHdrAccount = (TextView) view.findViewById(R.id.hdr_account);
        mHdrIncome = (TextView) view.findViewById(R.id.hdr_amount);

        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();
        for (int i = 0; i < mIncomeCategories.size(); i++) {
            int rNum = random.nextInt(high - low) + low;
            String color = mTileColors[rNum];
            String category = mIncomeCategories.get(i).getName();
            mRowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
            mData.add(mRowData);
        }

        mListAdapter = new ListAdapter(mActivity, R.layout.list_item, R.id.tv_primary, mData);
        mSpCategory.setAdapter(mListAdapter);

        mData = new Vector<RowData>();
        for (int i = 0; i < mAccounts.size(); i++) {
            int rNum = random.nextInt(high - low) + low;
            String color = mTileColors[rNum];
            String account = mAccounts.get(i).getName();
            mRowData = new RowData(account, "" + account.charAt(0), Color.parseColor(color), false);
            mData.add(mRowData);
        }

        mListAdapter = new ListAdapter(mActivity, R.layout.list_item, R.id.tv_primary, mData);
        mSpAccount.setAdapter(mListAdapter);

        mDate.setText(mActivity.getString(R.string.today));
        mDate.setFocusable(false);

        mDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrDate.setTypeface(mRobotoLight);
        mHdrCategory.setTypeface(mRobotoLight);
        mHdrAccount.setTypeface(mRobotoLight);
        mHdrIncome.setTypeface(mRobotoLight);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mTitle.setAdapter(new SimpleSpinnerAdapter(mActivity,R.layout.simple_spinner_item,
                mTitleList));

        mSpCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(mActivity, GenericList.class);
                    intent.putExtra("type", "income");
                    startActivityForResult(intent, 0);
                }
                return true;
            }
        });

        mSpAccount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(mActivity, GenericList.class);
                    intent.putExtra("type", "account");
                    startActivityForResult(intent, 1);
                }
                return true;
            }
        });

        npb = new NumberPickerBuilder()
                .setFragmentManager(mActivity.getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setTargetFragment(this)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditIncome.getAmount()) + " " + currency);
            mTitle.setText(mEditIncome.getTitle());
            mDescription.setText(mEditIncome.getDescription());
            mDate.setText(mEditIncome.getDate());
            mSpCategory.setSelection(getCategoryIndex(mEditIncome.getCategory()));
            mSpAccount.setSelection(getAccountIndex(mEditIncome.getAccount()));
        } else {
            npb.show();
        }

        SpannableString s = new SpannableString(mActivity.getString(R.string.add_income));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_income, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Intent intent = new Intent(mActivity, FynxWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {R.xml.fynx_widget_info};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);

        final Fragment fragment = new MainFragment();
        final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String date = mDate.getText().toString();
                int posCategory = mSpCategory.getSelectedItemPosition();
                int posAccount = mSpAccount.getSelectedItemPosition();
                String category = null;
                String account = null;

                if (mIncomeCategories != null
                        && mIncomeCategories.size() > 0) {
                    category = mIncomeCategories.get(posCategory).getName();
                }
                if (mAccounts != null
                        && mAccounts.size() > 0) {
                    account = mAccounts.get(posAccount).getName();
                }

                if (amount.startsWith("0.00")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_amt_income), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (title.equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (date.equals(mActivity.getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        date = df.format(today.getTime());
                    }
                    Amount income = new Amount();
                    income.setCategory(category);
                    income.setAccount(account);
                    income.setTitle(title);
                    income.setDescription(description);
                    income.setDate(date);
                    try {
                        income.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {

                    }
                    income.setType("income");
                    if (mEdit) {
                        income.setId(mEditIncome.getId());
                    }
                    db.saveIncome(income);
                    String dbDate = DateUtils.convertStringToDate(date);
                    double goalAmt = income.getAmount();
                    if (mEdit) {
                        if (goalAmt != mEditIncome.getAmount())
                            goalAmt -= mEditIncome.getAmount();
                    }
                    db.updateGoals(dbDate, goalAmt, income.getType());
                    mActivity.sendBroadcast(intent);
                }
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.cancel:
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(mActivity)
                        .setMessage(mActivity.getString(R.string.info_delete))
                        .setPositiveButton(mActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteIncomeById(mEditIncome.getId());
                                String dbDate = DateUtils.convertStringToDate(mEditIncome.getDate());
                                db.updateGoals(dbDate, -mEditIncome.getAmount(), mEditIncome.getType());
                                mActivity.sendBroadcast(intent);
                                ft.replace(R.id.detail_fragment, fragment);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                ft.commit();
                            }
                        })
                        .setNegativeButton(mActivity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        mDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_date:
                DatePickerBuilder dpb = new DatePickerBuilder()
                        .setFragmentManager(mActivity.getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                        .setTargetFragment(this);
                dpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0
                && resultCode == mActivity.RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpCategory.setSelection(position);
        } else if (requestCode == 1
                && resultCode == mActivity.RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpAccount.setSelection(position);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private int getCategoryIndex(String category) {
        int index = 0;
        if (mIncomeCategories != null) {
            for (int i = 0; i < mIncomeCategories.size(); i++) {
                if (mIncomeCategories.get(i).getName().equals(category)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getAccountIndex(String account) {
        int index = 0;
        if (mAccounts != null) {
            for (int i = 0; i < mAccounts.size(); i++) {
                if (mAccounts.get(i).getName().equals(account)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
