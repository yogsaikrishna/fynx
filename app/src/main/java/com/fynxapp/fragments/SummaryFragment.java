package com.fynxapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddExpenseActivity;
import com.fynxapp.AddIncomeActivity;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Category;
import com.fynxapp.db.objects.Summary;
import com.fynxapp.drawer.AccListAdapter;
import com.fynxapp.drawer.AccRowData;
import com.fynxapp.drawer.HeaderRow;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.ExpandableListAdapter;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.FileUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class SummaryFragment extends Fragment {
    private static final String ARG_INDEX = "index";
    private static final int CATEGORY_MENU_ID = 128;
    private static final int ACCOUNT_MENU_ID = 256;

    private Vector<AccRowData> mAccData;
    private AccRowData mAccRowData;
    private AccListAdapter mAdapter;
    private ListView mList;
    private TextView mNoData;
    private DBHelper db;
    private List<Summary> mSummaryList;
    private List<Summary> headerData;
    private List<Summary> summaryData;
    private List<Account> mAccounts;
    private List<Category> mCategories;
    private HashMap<String, List<Summary>> mExpListData;
    private SharedPreferences mSharedPreferences;
    private String mGrouping;
    private Typeface mTypeface;
    private ExpandableListAdapter mAccListAdapter;
    private ExpandableListView mListView;
    private SpannableString mTitle;
    private SpannableString mSummary;
    private ActionBarActivity mActivity;
    private List<String> mCheckedAccounts, mCheckedCategories;

    public static SummaryFragment newInstance(int index) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public SummaryFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");

        db = new DBHelper(getActivity());
        headerData = db.getDataHeader(mGrouping);
        summaryData = db.getDataSummary(mGrouping);
        mAccounts = db.getAccountsList();
        mCategories = db.getCategoriesByType(null);

        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        mCheckedAccounts = new ArrayList<String>();
        mCheckedCategories = new ArrayList<String>();

        setHasOptionsMenu(true);

        mActivity = (ActionBarActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.expandable_listview, container, false);

        mList = (ListView) view.findViewById(R.id.listView);
        mListView = (ExpandableListView) view.findViewById(R.id.exp_list_view);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);
        mNoData.setTypeface(mTypeface);
        mNoData.setVisibility(View.GONE);

        mActivity.getSupportActionBar().setSubtitle(null);

        updateData();

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle;
                Fragment fragment;
                Summary summary = mSummaryList.get(i);
                if (summary.getType().equals("income")) {
                    fragment = new AddIncomeFragment();
                } else {
                    fragment = new AddExpenseFragment();
                }
                bundle = new Bundle();
                bundle.putBoolean("edit", true);
                bundle.putInt("id", summary.getId());
                fragment.setArguments(bundle);
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        });

        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
                Bundle bundle;
                Fragment fragment;
                Summary summary;
                Summary header = headerData.get(i);
                if (mGrouping.equalsIgnoreCase("weekly")) {
                    summary = mExpListData.get(header.getStartDate()).get(i2);
                } else {
                    summary = mExpListData.get(header.getGroup()).get(i2);
                }
                if (summary.getType().equals("income")) {
                    fragment = new AddIncomeFragment();
                } else {
                    fragment = new AddExpenseFragment();
                }
                bundle = new Bundle();
                bundle.putBoolean("edit", true);
                bundle.putInt("id", summary.getId());
                fragment.setArguments(bundle);
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                return true;
            }
        });

        mTitle = new SpannableString(mActivity.getString(R.string.title_activity_acc_summary));
        mTitle.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mTitle.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(mTitle);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.acc_summary, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Menu accSubMenu = menu.findItem(R.id.by_account).getSubMenu();
        Menu catSubMenu = menu.findItem(R.id.by_category).getSubMenu();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            MenuItem item = menu.findItem(R.id.all);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.week);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.month);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.year);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.export);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.csv);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.excel);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.by_account);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.by_category);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.by_date);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
        }
        if (mCheckedAccounts.size() == 0)
            accSubMenu.clear();
        if (mAccounts != null
                && mAccounts.size() > 0) {
            if (accSubMenu.findItem(ACCOUNT_MENU_ID) == null) {
                for (int i = 0; i < mAccounts.size(); i++) {
                    Account account = mAccounts.get(i);
                    SpannableString s = CommonsUtils.getSpannableString(mActivity, account.getName());
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, s).setCheckable(true);
                    else
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, account.getName()).setCheckable(true);
                }
            }
        }
        if (mCheckedCategories.size() == 0)
            catSubMenu.clear();
        if (mCategories != null
                && mCategories.size() > 0) {
            if (catSubMenu.findItem(CATEGORY_MENU_ID) == null) {
                for (int i = 0; i < mCategories.size(); i++) {
                    Category category = mCategories.get(i);
                    SpannableString s = CommonsUtils.getSpannableString(mActivity, category.getName());
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
                        catSubMenu.add(0, CATEGORY_MENU_ID, 0, s).setCheckable(true);
                    else
                        catSubMenu.add(0, CATEGORY_MENU_ID, 0, category.getName()).setCheckable(true);
                }
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Uri uri;
        switch (item.getItemId()) {
            case R.id.all:
                mCheckedAccounts.clear();
                mCheckedCategories.clear();
                mActivity.supportInvalidateOptionsMenu();
                updateData();
                mActivity.getSupportActionBar().setSubtitle(null);
                break;
            case R.id.week:
                mSummaryList = db.getAccountSummary(7);
                updateList();

                mSummary = new SpannableString(mActivity.getString(R.string.filter_week));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.month:
                mSummaryList = db.getAccountSummary(30);
                updateList();

                mSummary = new SpannableString(mActivity.getString(R.string.filter_month));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.year:
                mSummaryList = db.getAccountSummary(90);
                updateList();

                mSummary = new SpannableString(mActivity.getString(R.string.filter_year));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.csv:
                mSummaryList = db.getAccountSummary();
                uri = FileUtils.exportToCSV(mSummaryList);
                if (uri != null) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("text/csv");
                    startActivity(Intent.createChooser(intent, mActivity.getString(R.string.export)));
                }
                break;
            case R.id.excel:
                mSummaryList = db.getAccountSummary();
                uri = FileUtils.exportToExcel(mSummaryList);
                if (uri != null) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("application/vnd.ms-excel");
                    startActivity(Intent.createChooser(intent, mActivity.getString(R.string.export)));
                }
                break;
            case ACCOUNT_MENU_ID:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mCheckedAccounts.remove(item.getTitle().toString());
                } else {
                    item.setChecked(true);
                    mCheckedAccounts.add(item.getTitle().toString());
                }

                if (mCheckedAccounts.size() > 0) {
                    mSummaryList = db.getAccountSummary("account", CommonsUtils.joinList(",", mCheckedAccounts));
                    updateList();
                    mSummary = new SpannableString(mActivity.getString(R.string.filtered_account));
                    mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    mActivity.getSupportActionBar().setSubtitle(mSummary);
                } else {
                    updateData();
                    mActivity.getSupportActionBar().setSubtitle(null);
                }
                return true;
            case CATEGORY_MENU_ID:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mCheckedCategories.remove(item.getTitle().toString());
                } else {
                    item.setChecked(true);
                    mCheckedCategories.add(item.getTitle().toString());
                }

                if (mCheckedCategories.size() > 0) {
                    mSummaryList = db.getAccountSummary("category", CommonsUtils.joinList(",", mCheckedCategories));
                    updateList();
                    mSummary = new SpannableString(mActivity.getString(R.string.filtered_category));
                    mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    mActivity.getSupportActionBar().setSubtitle(mSummary);
                } else {
                    updateData();
                    mActivity.getSupportActionBar().setSubtitle(null);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCheckedCategories.clear();
        mCheckedAccounts.clear();
        mActivity.supportInvalidateOptionsMenu();

        mActivity.getSupportActionBar().setSubtitle(null);
        headerData = db.getDataHeader(mGrouping);
        summaryData = db.getDataSummary(mGrouping);
        updateData();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }

    private void updateList() {
        mListView.setVisibility(View.GONE);
        mList.setVisibility(View.VISIBLE);

        mAccData = new Vector<AccRowData>();

        if (mSummaryList != null
                && mSummaryList.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < mSummaryList.size(); i++) {
                Summary summary = mSummaryList.get(i);
                String date[] = summary.getDate().split("-");
                String month = date[1].trim();
                String day = date[0].trim();
                day = day.length() > 1 ? day : "0" + day;
                String year = date[2].trim();
                double amount = summary.getAmount();
                mAccRowData = new AccRowData(month, day + " " + year, amount,
                        summary.getTitle(), summary.getCategory());
                if (summary.getType().equals("income")) {
                    mAccRowData.setType("income");
                } else {
                    mAccRowData.setType("expense");
                }
                mAccData.add(mAccRowData);
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        mAdapter = new AccListAdapter(mActivity, R.layout.account_list_item, R.id.tv_amount, mAccData);
        mList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void updateData() {
        mList.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);

        List<HeaderRow> headerList = new ArrayList<HeaderRow>();
        HashMap<String, List<AccRowData>> childList = new HashMap<String, List<AccRowData>>();
        mExpListData = new HashMap<String, List<Summary>>();

        if (headerData != null
                && headerData.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < headerData.size(); i++) {
                Summary header = headerData.get(i);
                String group = header.getGroup();

                HeaderRow headerRow = new HeaderRow();

                if (mGrouping.equalsIgnoreCase("weekly")) {
                    headerRow.setDate(header.getStartDate());
                } else {
                    headerRow.setDate(group);
                }

                headerRow.setAmount((int) header.getAmount() + "");

                headerList.add(headerRow);
                List<AccRowData> mAccData = new ArrayList<AccRowData>();
                List<Summary> childData = new ArrayList<Summary>();

                for (int j = 0; j < summaryData.size(); j++) {
                    Summary child = summaryData.get(j);
                    String childGroup = child.getGroup();

                    if (childGroup.equals(group)) {
                        String date[] = child.getDate().split("-");
                        String month = date[1].trim();
                        String day = date[0].trim();
                        day = day.length() > 1 ? day : "0" + day;
                        String year = date[2].trim();
                        double amount = child.getAmount();
                        mAccRowData = new AccRowData(month, day + " " + year, amount,
                                child.getTitle(), child.getCategory());
                        if (child.getType().equals("income")) {
                            mAccRowData.setType("income");
                        } else {
                            mAccRowData.setType("expense");
                        }
                        mAccData.add(mAccRowData);
                        childData.add(child);
                    }
                }
                if (mGrouping.equalsIgnoreCase("weekly")) {
                    childList.put(header.getStartDate(), mAccData);
                    mExpListData.put(header.getStartDate(), childData);
                } else {
                    childList.put(group, mAccData);
                    mExpListData.put(group, childData);
                }
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        mAccListAdapter = new ExpandableListAdapter(mActivity, headerList, childList);
        mListView.setAdapter(mAccListAdapter);
        mAccListAdapter.notifyDataSetChanged();
    }
}
