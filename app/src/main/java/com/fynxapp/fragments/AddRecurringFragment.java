package com.fynxapp.fragments;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.GenericList;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.SimpleSpinnerAdapter;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class AddRecurringFragment extends Fragment implements  View.OnClickListener,
        DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String ARG_INDEX = "index";
    private static final String TAG = "AddIncomeActivity";

    private ActionBarActivity mActivity;
    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private String[] mTileColors;
    private List<Category> mCategories;
    private List<Account> mAccounts;
    private Spinner mSpCategory, mSpAccount, mSpFrequency;
    private EditText mDate, mTitle, mDescription;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrDate, mHdrCategory, mHdrAccount, mHdrFrequency, mHdrAmount;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Amount mEditRecurring, mRecurring;
    private RadioButton mIncome, mExpense;
    private RadioGroup mRadioGroup;
    private String mType;
    private List<String> mFreqList;
    private int year, month, date;
    private int editPosition = 0;
    private String mNextDate;
    private NumberFormat nf;
    private String daily, weekly, monthly, yearly;

    public static AddRecurringFragment newInstance(int index) {
        AddRecurringFragment fragment = new AddRecurringFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public AddRecurringFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);
        mAccounts = db.getAccountsList();

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mBundle = getArguments();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditRecurring = db.getRecurringById(id);
                String s = mActivity.getString(R.string.edit_recurring);
                mActivity.getSupportActionBar().setTitle(CommonsUtils.getSpannableString(mActivity, s));
                mType = mEditRecurring.getType();
                mCategories = db.getCategoriesByType(mType);
                String tmpDate[] = mEditRecurring.getDate().split("-");
                this.date = Integer.parseInt(tmpDate[0]);
                this.month = DateUtils.getMonth(tmpDate[1]);
                this.year = Integer.parseInt(tmpDate[2]);
            }
        } else {
            mCategories = db.getCategoriesByType("income");
            mType = "income";
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        daily = mActivity.getString(R.string.daily);
        weekly = mActivity.getString(R.string.weekly);
        monthly = mActivity.getString(R.string.monthly);
        yearly = mActivity.getString(R.string.yearly);

        mTileColors = getResources().getStringArray(R.array.tile_colors);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_recurring, container, false);
        mSpCategory = (Spinner) view.findViewById(R.id.sp_categories);
        mSpAccount = (Spinner) view.findViewById(R.id.sp_account);
        mSpFrequency = (Spinner) view.findViewById(R.id.sp_frequency);
        mDate = (EditText) view.findViewById(R.id.et_date);
        mTitle = (EditText) view.findViewById(R.id.et_title);
        mDescription = (EditText) view.findViewById(R.id.et_description);
        mAmount = (TextView) view.findViewById(R.id.tv_amount);

        mHdrDetails = (TextView) view.findViewById(R.id.hdr_details);
        mHdrDate = (TextView) view.findViewById(R.id.hdr_date);
        mHdrCategory = (TextView) view.findViewById(R.id.hdr_category);
        mHdrAccount = (TextView) view.findViewById(R.id.hdr_account);
        mHdrFrequency = (TextView) view.findViewById(R.id.hdr_frequency);
        mHdrAmount = (TextView) view.findViewById(R.id.hdr_amount);

        mIncome = (RadioButton) view.findViewById(R.id.rb_income);
        mExpense = (RadioButton) view.findViewById(R.id.rb_expense);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.radiogroup);

        updateCategoryList();

        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();

        if (mAccounts != null
                && mAccounts.size() > 0) {
            for (int i = 0; i < mAccounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String account = mAccounts.get(i).getName();
                mRowData = new RowData(account, "" + account.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(mActivity, R.layout.list_item, R.id.tv_primary, mData);
            mSpAccount.setAdapter(mListAdapter);
        }

        updateFrequency();

        mDate.setText(mActivity.getString(R.string.today));
        mDate.setFocusable(false);

        mDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrDate.setTypeface(mRobotoLight);
        mHdrCategory.setTypeface(mRobotoLight);
        mHdrAccount.setTypeface(mRobotoLight);
        mHdrFrequency.setTypeface(mRobotoLight);
        mHdrAmount.setTypeface(mRobotoLight);

        mIncome.setTypeface(mRoboto);
        mExpense.setTypeface(mRoboto);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mRadioGroup.check(R.id.rb_income);

        mSpCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(mActivity, GenericList.class);
                    intent.putExtra("type", mType);
                    startActivityForResult(intent, 0);
                }
                return true;
            }
        });

        mSpAccount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(mActivity, GenericList.class);
                    intent.putExtra("type", "account");
                    startActivityForResult(intent, 1);
                }
                return true;
            }
        });

        npb = new NumberPickerBuilder()
                .setFragmentManager(mActivity.getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setTargetFragment(this)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditRecurring.getAmount()) + " " + currency);
            mTitle.setText(mEditRecurring.getTitle());
            mDescription.setText(mEditRecurring.getDescription());
            mDate.setText(mEditRecurring.getDate());
            mSpCategory.setSelection(getCategoryIndex(mEditRecurring.getCategory()));
            mSpAccount.setSelection(getAccountIndex(mEditRecurring.getAccount()));
            mSpFrequency.setSelection(getFrequencyIndex(mEditRecurring.getFrequency()));
            if (mType.equals("income")) {
                mRadioGroup.check(R.id.rb_income);
            } else {
                mRadioGroup.check(R.id.rb_expense);
            }
        } else {
            npb.show();
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_income:
                        mType = "income";
                        break;
                    case R.id.rb_expense:
                        mType = "expense";
                        break;
                }
                mCategories = db.getCategoriesByType(mType);
                updateCategoryList();
            }
        });

        SpannableString s = new SpannableString(mActivity.getString(R.string.title_activity_add_recurring));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_recurring, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Fragment fragment = new RecurringFragment();
        final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String date = mDate.getText().toString();
                int posCategory = mSpCategory.getSelectedItemPosition();
                int posAccount = mSpAccount.getSelectedItemPosition();
                int posFrequency = mSpFrequency.getSelectedItemPosition();
                String category = null;
                String account = null;

                if (mCategories != null
                        && mCategories.size() > 0) {
                    category = mCategories.get(posCategory).getName();
                }
                if (mAccounts != null
                        && mAccounts.size() > 0) {
                    account = mAccounts.get(posAccount).getName();
                }

                if (amount.startsWith("0.00")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_amt), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (title.equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (date.equals(mActivity.getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        date = df.format(today.getTime());
                        String tmpDate[] = date.split("-");
                        this.date = Integer.parseInt(tmpDate[0]);
                        this.month = DateUtils.getMonth(tmpDate[1]);
                        this.year = Integer.parseInt(tmpDate[2]);
                    }
                    mRecurring = new Amount();
                    mRecurring.setCategory(category);
                    mRecurring.setAccount(account);
                    mRecurring.setTitle(title);
                    mRecurring.setDescription(description);
                    mRecurring.setDate(date);
                    try {
                        mRecurring.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {
                        Log.d(TAG, "Exception occurred: " + e.toString());
                    }
                    mRecurring.setType(mType);
                    mRecurring.setFrequency(getFrequency(posFrequency));
                    mRecurring.setUpdatedDate(date);
                    if (mEdit) {
                        mRecurring.setId(mEditRecurring.getId());
                        mRecurring.setRecurringId(mEditRecurring.getId());
                        showUpdateDialog(mRecurring);
                    } else {
                        long _id = db.saveRecurring(mRecurring);
                        mRecurring.setRecurringId((int) _id);
                        addRecurringToDate(mRecurring);
                    }
                }
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.cancel:
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(mActivity)
                        .setMessage(mActivity.getString(R.string.info_delete))
                        .setPositiveButton(mActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteRecurringById(mEditRecurring.getId());
                                AlarmHelper.cancelRecurringAlarm(mActivity, mEditRecurring.getId());
                                ft.replace(R.id.detail_fragment, fragment);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                ft.commit();
                            }
                        })
                        .setNegativeButton(mActivity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_date:
                DatePickerBuilder dpb = new DatePickerBuilder()
                        .setFragmentManager(mActivity.getSupportFragmentManager())
                        .setTargetFragment(this)
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                dpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        this.year = year;
        this.month = month + 1;
        this.date = date;

        mDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0
                && resultCode == mActivity.RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpCategory.setSelection(position);
        } else if (requestCode == 1
                && resultCode == mActivity.RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpAccount.setSelection(position);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private int getCategoryIndex(String category) {
        int index = 0;
        if (mCategories != null) {
            for (int i = 0; i < mCategories.size(); i++) {
                if (mCategories.get(i).getName().equals(category)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getAccountIndex(String account) {
        int index = 0;
        if (mAccounts != null) {
            for (int i = 0; i < mAccounts.size(); i++) {
                if (mAccounts.get(i).getName().equals(account)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getFrequencyIndex(String frequency) {
        int index = 0;
        if (mFreqList != null) {
            for (int i = 0; i < mFreqList.size(); i++) {
                if (mFreqList.get(i).equals(frequency)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private String getFrequency(int position) {
        switch (position) {
            case 0:
                return "Daily";
            case 1:
                return "Weekly";
            case 2:
                return "Monthly";
            case 4:
                return "Yearly";
        }
        return "";
    }

    private void updateCategoryList() {
        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();

        if (mCategories != null
                && mCategories.size() > 0) {
            for (int i = 0; i < mCategories.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String category = mCategories.get(i).getName();
                mRowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(mActivity, R.layout.list_item, R.id.tv_primary, mData);
            mSpCategory.setAdapter(mListAdapter);
            mListAdapter.notifyDataSetChanged();
        }
    }

    private void updateFrequency() {
        mFreqList = new ArrayList<String>();
        mFreqList.add(daily);
        mFreqList.add(weekly);
        mFreqList.add(monthly);
        mFreqList.add(yearly);
        SimpleSpinnerAdapter mFreqAdapter = new SimpleSpinnerAdapter(mActivity,R.layout.simple_spinner_item,
                mFreqList);
        mFreqAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpFrequency.setAdapter(mFreqAdapter);
        mSpFrequency.setSelection(2);
    }

    private void setAlarm(String frequency, int id) {
        long interval = AlarmManager.INTERVAL_DAY * 30;

        if (frequency.equalsIgnoreCase("daily")) {
            interval = AlarmManager.INTERVAL_DAY;
        } else if (frequency.equalsIgnoreCase("weekly")) {
            interval = AlarmManager.INTERVAL_DAY * 7;
        } else if (frequency.equalsIgnoreCase("monthly")) {
            interval = AlarmManager.INTERVAL_DAY * 30;
        } else if (frequency.equalsIgnoreCase("yearly")) {
            interval = AlarmManager.INTERVAL_DAY * 365;
        }

        AlarmHelper.setRecurringAlarm(mActivity, interval, id, mNextDate);
        mRecurring.setId(id);
        mRecurring.setDate(mRecurring.getUpdatedDate());
        mRecurring.setUpdatedDate(mNextDate);
        db.saveRecurring(mRecurring);
    }

    private void addRecurringToDate(Amount recurring) {
        String[] today = DateUtils.getTodayDate(Constants.DD_MM_YYYY).split("-");

        int currDate = Integer.parseInt(today[0]);
        int currMonth = Integer.parseInt(today[1]);
        int currYear = Integer.parseInt(today[2]);

        int iterations = 0;

        String frequency = recurring.getFrequency();

        if (frequency.equalsIgnoreCase("daily")) {
            if (this.year == currYear) {
                iterations -= this.date;
                if (this.month < currMonth) {
                    for (int m = this.month; m < currMonth; m++) {
                        iterations += DateUtils.getDaysInMonth(m, currYear);
                    }
                } else if (this.month == currMonth) {
                    iterations += currDate;
                }
            } else if (this.year < currYear) {
                for (int y = this.year; y < currYear; y++) {
                    iterations -= this.date;
                    for (int m = this.month; m <= 12; m++) {
                        iterations += DateUtils.getDaysInMonth(m, y);
                    }
                }
                for (int m = 1; m < currMonth; m++) {
                    iterations += DateUtils.getDaysInMonth(m, currYear);
                }
            }
        } else if (frequency.equalsIgnoreCase("weekly")) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DATE, this.date);
            calendar.set(Calendar.MONTH, this.month - 1);
            calendar.set(Calendar.YEAR, this.year);
            int week = calendar.get(Calendar.WEEK_OF_YEAR);

            calendar.set(Calendar.DATE, currDate);
            calendar.set(Calendar.MONTH, currMonth - 1);
            calendar.set(Calendar.YEAR, currYear);
            int currWeek = calendar.get(Calendar.WEEK_OF_YEAR);

            if (this.year == currYear
                    && week < currWeek) {
                iterations = currWeek - week + 1;
            } else if (this.year < currYear) {
                iterations -= week;
                for (int y = this.year; y < currYear; y++) {
                    iterations += 52;
                }
                iterations += currWeek + 1;
            }

        } else if (frequency.equalsIgnoreCase("monthly")) {
            if (this.year == currYear
                    && this.month < currMonth) {
                iterations = currMonth - this.month + 1;
            } else if (this.year < currYear) {
                for (int y = this.year; y < currYear; y++) {
                    for (int m = this.month; m <= 12; m++) {
                        iterations += 1;
                    }
                }
                iterations += currMonth;
            }
        } else if (frequency.equalsIgnoreCase("yearly")) {
            for (int y = this.year; y < currYear; y++) {
                iterations += 1;
            }
            iterations = 1;
        }
        updateData(recurring, iterations, frequency);
        Log.d(TAG, "Iterations: " + iterations);
    }

    private void updateData(final Amount recurring, final int iterations,
                            final String frequency) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String dbDate = null;
                for (int i = 0; i < iterations; i++) {
                    recurring.setId(-1);
                    if (recurring.getType().equalsIgnoreCase("income")) {
                        db.saveIncome(recurring);
                    } else {
                        db.saveExpense(recurring);
                    }
                    String date = DateUtils.convertStringToDate(recurring.getDate());
                    db.updateGoals(date, recurring.getAmount(), recurring.getType());
                    dbDate = getNextSyncDate(frequency, recurring.getDate());
                    recurring.setDate(dbDate);
                }
                mNextDate = dbDate == null ? mRecurring.getDate() : dbDate;
                setAlarm(mRecurring.getFrequency(), recurring.getRecurringId());
                Fragment fragment = new RecurringFragment();
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        }).start();
    }

    private String getNextSyncDate(String frequency, String dbDate) {
        String[] tmpDate = dbDate.split("-");

        int date = Integer.parseInt(tmpDate[0]);
        int month = DateUtils.getMonth(tmpDate[1]);
        int year = Integer.parseInt(tmpDate[2]);

        if (frequency != null) {
            if (frequency.equalsIgnoreCase("daily")) {
                date += 1;
                if (month == 2 && (date > 28)) {
                    if (date > 29) {
                        date = 1;
                        month += 1;
                    }
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date = 1;
                    month += 1;
                } else if (date > 31) {
                    date = 1;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("weekly")) {
                date += 7;
                if (month == 2 && (date > 28)) {
                    date -= 28;
                    month += 1;
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date -= 30;
                    month += 1;
                } else if (date > 31) {
                    date -= 31;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("monthly")) {
                month += 1;
                if (month == 2 && (date > 28)) {
                    date = 28;
                }
                if (month > 12) {
                    month = 1;
                    year += 1;
                }
            } else if (frequency.equalsIgnoreCase("yearly")) {
                year += 1;
            }
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        calendar.set(Calendar.DATE, date);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        return df.format(calendar.getTime());
    }

    private void showUpdateDialog(final Amount recurring) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        final Fragment fragment = new RecurringFragment();
        final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
        builder.setTitle(mActivity.getString(R.string.update_from))
                .setSingleChoiceItems(R.array.update_from, 0,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                editPosition = i;
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ft.replace(R.id.detail_fragment, fragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    }
                })
                .setPositiveButton(mActivity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch (editPosition) {
                            case 0:
                                boolean update = db.updateAmountByRecurringId(recurring.getRecurringId(),
                                        recurring.getType());
                                if (update) {
                                    addRecurringToDate(recurring);
                                }
                                break;
                            case 1:
                                mNextDate = mEditRecurring.getUpdatedDate();
                                setAlarm(mRecurring.getFrequency(), mEditRecurring.getId());
                                ft.replace(R.id.detail_fragment, fragment);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                ft.commit();
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
