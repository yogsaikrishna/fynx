package com.fynxapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddGoal;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.TypefaceSpan;

import java.util.List;
import java.util.Vector;

public class GoalsFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private ListView listView;
    private ListAdapter listAdapter;
    private TextView mNoData;
    private DBHelper db;
    private List<Goal> mGoalList;
    private Typeface mRoboto;
    private ActionBarActivity mActivity;

    public static GoalsFragment newInstance(int index) {
        GoalsFragment fragment = new GoalsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public GoalsFragment() {
    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        db = new DBHelper(mActivity);
        mGoalList = db.getGoalsList();

        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.goals, container, false);

        listView = (ListView) view.findViewById(R.id.listView);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);

        mNoData.setTypeface(mRoboto);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                Fragment fragment = new AddGoalFragment();
                bundle.putBoolean("edit", true);
                bundle.putInt("id", mGoalList.get(i).getId());
                fragment.setArguments(bundle);
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        });

        SpannableString s = new SpannableString(mActivity.getString(R.string.goals));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);
        mActivity.getSupportActionBar().setSubtitle(null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.goals, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.create) {
            Fragment fragment = new AddGoalFragment();
            FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.detail_fragment, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoalList = db.getGoalsList();

        if (mGoalList != null
                && mGoalList.size() > 0) {
            mNoData.setVisibility(View.GONE);
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                listAdapter = new ListAdapter(mActivity, R.layout.goal_list_item, R.id.tv_amount, mGoalList);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(listAdapter);
                        listAdapter.notifyDataSetChanged();
                    }
                });

            }
        }).start();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
