package com.fynxapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.analytics.objects.DataPie;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.utils.Animate;

import java.text.NumberFormat;
import java.util.List;

public class TopExpensesFragment extends Fragment {
    private TextView mHdrExpense;
    private TextView mExpenseOne, mAmtExpenseOne;
    private TextView mExpenseTwo, mAmtExpenseTwo;
    private TextView mExpenseThree, mAmtExpenseThree;
    private TextView mExpenseFour, mAmtExpenseFour;
    private TextView mExpenseFive, mAmtExpenseFive;
    private TextView mNoData;
    private ProgressBar mPbOne, mPbTwo, mPbThree, mPbFour, mPbFive;
    private LinearLayout mLayoutOne, mLayoutTwo, mLayoutThree, mLayoutFour, mLayoutFive;
    private Typeface mTypeface;
    private ActionBarActivity mActivity;
    private DBHelper db;
    private List<DataPie> dataPie;
    private NumberFormat nf;
    private SharedPreferences mSharedPreferences;
    private String mCurrency, mGrouping;

    public static TopExpensesFragment newInstance() {
        TopExpensesFragment fragment = new TopExpensesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public TopExpensesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        db = new DBHelper(mActivity);
        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_expenses, container, false);
        mHdrExpense = (TextView) view.findViewById(R.id.tv_top_expenses);

        mExpenseOne = (TextView) view.findViewById(R.id.tv_expense_one);
        mAmtExpenseOne = (TextView) view.findViewById(R.id.tv_amt_expense_one);
        mExpenseTwo = (TextView) view.findViewById(R.id.tv_expense_two);
        mAmtExpenseTwo = (TextView) view.findViewById(R.id.tv_amt_expense_two);
        mExpenseThree = (TextView) view.findViewById(R.id.tv_expense_three);
        mAmtExpenseThree = (TextView) view.findViewById(R.id.tv_amt_expense_three);
        mExpenseFour = (TextView) view.findViewById(R.id.tv_expense_four);
        mAmtExpenseFour = (TextView) view.findViewById(R.id.tv_amt_expense_four);
        mExpenseFive = (TextView) view.findViewById(R.id.tv_expense_five);
        mAmtExpenseFive = (TextView) view.findViewById(R.id.tv_amt_expense_five);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);

        mLayoutOne = (LinearLayout) view.findViewById(R.id.ll_expense_one);
        mLayoutTwo = (LinearLayout) view.findViewById(R.id.ll_expense_two);
        mLayoutThree = (LinearLayout) view.findViewById(R.id.ll_expense_three);
        mLayoutFour = (LinearLayout) view.findViewById(R.id.ll_expense_four);
        mLayoutFive = (LinearLayout) view.findViewById(R.id.ll_expense_five);

        mPbOne = (ProgressBar) view.findViewById(R.id.pb_expense_one);
        mPbTwo = (ProgressBar) view.findViewById(R.id.pb_expense_two);
        mPbThree = (ProgressBar) view.findViewById(R.id.pb_expense_three);
        mPbFour = (ProgressBar) view.findViewById(R.id.pb_expense_four);
        mPbFive = (ProgressBar) view.findViewById(R.id.pb_expense_five);

        mHdrExpense.setTypeface(mTypeface);
        mExpenseOne.setTypeface(mTypeface);
        mAmtExpenseOne.setTypeface(mTypeface);
        mExpenseTwo.setTypeface(mTypeface);
        mAmtExpenseTwo.setTypeface(mTypeface);
        mExpenseThree.setTypeface(mTypeface);
        mAmtExpenseThree.setTypeface(mTypeface);
        mExpenseFour.setTypeface(mTypeface);
        mAmtExpenseFour.setTypeface(mTypeface);
        mExpenseFive.setTypeface(mTypeface);
        mAmtExpenseFive.setTypeface(mTypeface);
        mNoData.setTypeface(mTypeface);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");

        dataPie = db.getExpensePieData(-1, null, mGrouping);
        double totalAmount = 0;

        mNoData.setVisibility(View.GONE);
        if (dataPie != null
                && dataPie.size() > 0) {
            switch (dataPie.size()) {
                case 1:
                    mLayoutOne.setVisibility(View.VISIBLE);
                    mLayoutTwo.setVisibility(View.GONE);
                    mLayoutThree.setVisibility(View.GONE);
                    mLayoutFour.setVisibility(View.GONE);
                    mLayoutFive.setVisibility(View.GONE);
                    break;
                case 2:
                    mLayoutOne.setVisibility(View.VISIBLE);
                    mLayoutTwo.setVisibility(View.VISIBLE);
                    mLayoutThree.setVisibility(View.GONE);
                    mLayoutFour.setVisibility(View.GONE);
                    mLayoutFive.setVisibility(View.GONE);
                    break;
                case 3:
                    mLayoutOne.setVisibility(View.VISIBLE);
                    mLayoutTwo.setVisibility(View.VISIBLE);
                    mLayoutThree.setVisibility(View.VISIBLE);
                    mLayoutFour.setVisibility(View.GONE);
                    mLayoutFive.setVisibility(View.GONE);
                    break;
                case 4:
                    mLayoutOne.setVisibility(View.VISIBLE);
                    mLayoutTwo.setVisibility(View.VISIBLE);
                    mLayoutThree.setVisibility(View.VISIBLE);
                    mLayoutFour.setVisibility(View.VISIBLE);
                    mLayoutFive.setVisibility(View.GONE);
                    break;
            }

            for (int i = 0; i < dataPie.size(); i++) {
                totalAmount += dataPie.get(i).getAmount();
            }

            for (int i = 0; i < dataPie.size(); i++) {
                DataPie data = dataPie.get(i);
                switch (i) {
                    case 0:
                        mExpenseOne.setText(data.getCategory());
                        mAmtExpenseOne.setText(nf.format(data.getAmount()) + " " + mCurrency);
                        mPbOne.setProgress((int) ((data.getAmount() / totalAmount) * 100));
                        new Animate(mActivity, mPbOne);
                        break;
                    case 1:
                        mExpenseTwo.setText(data.getCategory());
                        mAmtExpenseTwo.setText(nf.format(data.getAmount()) + " " + mCurrency);
                        mPbTwo.setProgress((int) ((data.getAmount() / totalAmount) * 100));
                        new Animate(mActivity, mPbTwo);
                        break;
                    case 2:
                        mExpenseThree.setText(data.getCategory());
                        mAmtExpenseThree.setText(nf.format(data.getAmount()) + " " + mCurrency);
                        mPbThree.setProgress((int) ((data.getAmount() / totalAmount) * 100));
                        new Animate(mActivity, mPbThree);
                        break;
                    case 3:
                        mExpenseFour.setText(data.getCategory());
                        mAmtExpenseFour.setText(nf.format(data.getAmount()) + " " + mCurrency);
                        mPbFour.setProgress((int) ((data.getAmount() / totalAmount) * 100));
                        new Animate(mActivity, mPbFour);
                        break;
                    case 4:
                        mExpenseFive.setText(data.getCategory());
                        mAmtExpenseFive.setText(nf.format(data.getAmount()) + " " + mCurrency);
                        mPbFive.setProgress((int) ((data.getAmount() / totalAmount) * 100));
                        new Animate(mActivity, mPbFive);
                        break;
                }
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
            mLayoutOne.setVisibility(View.GONE);
            mLayoutTwo.setVisibility(View.GONE);
            mLayoutThree.setVisibility(View.GONE);
            mLayoutFour.setVisibility(View.GONE);
            mLayoutFive.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
