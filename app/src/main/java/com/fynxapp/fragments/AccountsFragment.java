package com.fynxapp.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddAccount;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.TypefaceSpan;

import java.util.List;
import java.util.Random;
import java.util.Vector;

public class AccountsFragment extends Fragment {
    private String[] tileColors;
    private ListView listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter listAdapter;
    private TextView mNoData;
    private DBHelper db;
    private List<Account> accounts;
    private Typeface mTypeface;
    private ActionBarActivity mActivity;

    private static final String ARG_INDEX = "index";

    public static AccountsFragment newInstance(int index) {
        AccountsFragment fragment = new AccountsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public AccountsFragment() {

    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(getActivity());
        accounts = db.getAccountsList();
        tileColors = getResources().getStringArray(R.array.tile_colors);
        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);
        mActivity = (ActionBarActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.accounts, container, false);

        listView = (ListView) view.findViewById(R.id.listView);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);

        data = new Vector<RowData>();
        mNoData.setTypeface(mTypeface);

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        if (accounts != null
                && accounts.size() > 0) {
            mNoData.setVisibility(View.GONE);
            data = new Vector<RowData>();
            for (int i = 0; i < accounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = accounts.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(getActivity(), R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), AddAccount.class);
                intent.putExtra("edit", true);
                intent.putExtra("id", accounts.get(i).getId());
                startActivity(intent);
            }
        });

        SpannableString s = new SpannableString(mActivity.getString(R.string.accounts));
        s.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivity.getSupportActionBar().setTitle(s);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.create) {
            startActivity(new Intent(getActivity(), AddAccount.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.categories, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        accounts = db.getAccountsList();

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        data = new Vector<RowData>();

        if (accounts != null
                && accounts.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < accounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = accounts.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(getActivity(), R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
