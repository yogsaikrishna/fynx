package com.fynxapp;

import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.lists.SimpleSpinnerAdapter;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;
import com.fynxapp.widget.FynxWidget;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;


public class AddExpenseActivity extends ActionBarActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String TAG = "AddIncomeActivity";
    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_add, R.drawable.ic_accounts,
            R.drawable.ic_categories, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_objective, R.drawable.ic_action_recurring,
            R.drawable.ic_action_settings};

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mNavAdapter;
    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private String[] mTileColors;
    private List<Category> mExpenseCategories;
    private List<Account> mAccounts;
    private List<String> mTitleList;
    private Spinner mSpCategory, mSpAccount;
    private EditText mDate, mDescription;
    private AutoCompleteTextView mTitle;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrDate, mHdrCategory, mHdrAccount, mHdrExpense;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Amount mEditExpense;
    private NumberFormat nf;
    private boolean purchased;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_expense);

        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        db = new DBHelper(this);
        mExpenseCategories = db.getCategoriesByType("expense");
        mAccounts = db.getAccountsList();
        mTitleList = db.getTitlesList("expense");

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditExpense = db.getExpenseById(id);
                getSupportActionBar().setTitle(getString(R.string.edit_expense));
            }
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mTileColors = getResources().getStringArray(R.array.tile_colors);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mSpCategory = (Spinner) this.findViewById(R.id.sp_categories);
        mSpAccount = (Spinner) this.findViewById(R.id.sp_account);
        mDate = (EditText) this.findViewById(R.id.et_date);
        mTitle = (AutoCompleteTextView) this.findViewById(R.id.at_title);
        mDescription = (EditText) this.findViewById(R.id.et_description);
        mAmount = (TextView) this.findViewById(R.id.tv_amount);

        mHdrDetails = (TextView) this.findViewById(R.id.hdr_details);
        mHdrDate = (TextView) this.findViewById(R.id.hdr_date);
        mHdrCategory = (TextView) this.findViewById(R.id.hdr_category);
        mHdrAccount = (TextView) this.findViewById(R.id.hdr_account);
        mHdrExpense = (TextView) this.findViewById(R.id.hdr_amount);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Random random = new Random();
        int low = 0;
        int high = mTileColors.length;

        mData = new Vector<RowData>();

        if (mExpenseCategories != null
                && mExpenseCategories.size() > 0) {
            for (int i = 0; i < mExpenseCategories.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String category = mExpenseCategories.get(i).getName();
                mRowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, mData);
            mSpCategory.setAdapter(mListAdapter);
        }

        mData = new Vector<RowData>();

        if (mAccounts != null
                && mAccounts.size() > 0) {
            for (int i = 0; i < mAccounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = mTileColors[rNum];
                String account = mAccounts.get(i).getName();
                mRowData = new RowData(account, "" + account.charAt(0), Color.parseColor(color), false);
                mData.add(mRowData);
            }

            mListAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, mData);
            mSpAccount.setAdapter(mListAdapter);
        }

        mDate.setText(getString(R.string.today));
        mDate.setFocusable(false);

        mDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrDate.setTypeface(mRobotoLight);
        mHdrCategory.setTypeface(mRobotoLight);
        mHdrAccount.setTypeface(mRobotoLight);
        mHdrExpense.setTypeface(mRobotoLight);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mTitle.setAdapter(new SimpleSpinnerAdapter(this,R.layout.simple_spinner_item,
                mTitleList));

        mSpCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(AddExpenseActivity.this, GenericList.class);
                    intent.putExtra("type", "expense");
                    startActivityForResult(intent, 0);
                }
                return true;
            }
        });

        mSpAccount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(AddExpenseActivity.this, GenericList.class);
                    intent.putExtra("type", "account");
                    startActivityForResult(intent, 1);
                }
                return true;
            }
        });

        npb = new NumberPickerBuilder()
                .setFragmentManager(getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditExpense.getAmount()) + " " + currency);
            mTitle.setText(mEditExpense.getTitle());
            mDescription.setText(mEditExpense.getDescription());
            mDate.setText(mEditExpense.getDate());
            mSpCategory.setSelection(getCategoryIndex(mEditExpense.getCategory()));
            mSpAccount.setSelection(getAccountIndex(mEditExpense.getAccount()));
        } else {
            npb.show();
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_income, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        final Intent intent = new Intent(this, FynxWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {R.xml.fynx_widget_info};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String date = mDate.getText().toString();
                int posCategory = mSpCategory.getSelectedItemPosition();
                int posAccount = mSpAccount.getSelectedItemPosition();
                String category = null;
                String account = null;

                if (mExpenseCategories != null
                        && mExpenseCategories.size() > 0) {
                    category = mExpenseCategories.get(posCategory).getName();
                }
                if (mAccounts != null
                        && mAccounts.size() > 0) {
                    account = mAccounts.get(posAccount).getName();
                }

                if (amount.startsWith("0.00")) {
                    Toast.makeText(this, getString(R.string.err_amt_expense), Toast.LENGTH_SHORT).show();
                    return true;
                } if (title.equals("")) {
                    Toast.makeText(this, getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (date.equals(getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        date = df.format(today.getTime());
                    }
                    Amount expense = new Amount();
                    expense.setCategory(category);
                    expense.setAccount(account);
                    expense.setTitle(title);
                    expense.setDescription(description);
                    expense.setDate(date);
                    try {
                        expense.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {
                        Log.d(TAG, "Exception occurred: " + e.toString());
                    }
                    expense.setType("expense");
                    if (mEdit) {
                        expense.setId(mEditExpense.getId());
                    }
                    db.saveExpense(expense);
                    String dbDate = DateUtils.convertStringToDate(date);
                    double goalAmt = expense.getAmount();
                    if (mEdit) {
                        if (goalAmt != mEditExpense.getAmount())
                            goalAmt -= mEditExpense.getAmount();
                    }
                    db.updateGoals(dbDate, goalAmt, expense.getType());
                    sendBroadcast(intent);

                    this.finish();
                }
                break;
            case R.id.cancel:
                this.finish();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setMessage(getString(R.string.info_delete))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteExpenseById(mEditExpense.getId());
                                String dbDate = DateUtils.convertStringToDate(mEditExpense.getDate());
                                db.updateGoals(dbDate, -mEditExpense.getAmount(), mEditExpense.getType());
                                sendBroadcast(intent);
                                AddExpenseActivity.this.finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavAdapter = (ListView) findViewById(R.id.left_drawer);
        initializeNavList();
        addNavigationDrawer();
        mNavAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_date:
                DatePickerBuilder dpb = new DatePickerBuilder()
                        .setFragmentManager(getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                dpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(AddExpenseActivity.this, AddIncomeActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(AddExpenseActivity.this, AccountsActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(AddExpenseActivity.this, CategoriesActivity.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                intent = new Intent(AddExpenseActivity.this, AccSummaryActivity.class);
                startActivity(intent);
                finish();
                break;
            case 4:
                intent = new Intent(AddExpenseActivity.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 5:
                intent = new Intent(AddExpenseActivity.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 6:
                intent = new Intent(AddExpenseActivity.this, RecurringActivity.class);
                startActivity(intent);
                finish();
                break;
            case 7:
                intent = new Intent(AddExpenseActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        mDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0
                && resultCode == RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpCategory.setSelection(position);
        } else if (requestCode == 1
                && resultCode == RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            mSpAccount.setSelection(position);
        } else if (requestCode == 10001) {
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private int getCategoryIndex(String category) {
        int index = 0;
        if (mExpenseCategories != null) {
            for (int i = 0; i < mExpenseCategories.size(); i++) {
                if (mExpenseCategories.get(i).getName().equals(category)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private int getAccountIndex(String account) {
        int index = 0;
        if (mAccounts != null) {
            for (int i = 0; i < mAccounts.size(); i++) {
                if (mAccounts.get(i).getName().equals(account)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(AddExpenseActivity.this, tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                    startActivity(intent);
                    finish();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(AddExpenseActivity.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    private void addNavigationDrawer() {
        mData = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            if (i == 4 || i == 5) {
                if (purchased) {
                    mRowData = new RowData(mListText[i], mImgResources[i], false);
                } else {
                    mRowData = new RowData(mListText[i], mImgResources[i], true);
                }
            } else {
                mRowData = new RowData(mListText[i], mImgResources[i], false);
            }
            mData.add(mRowData);
        }
        mListAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, mData);
        mNavAdapter.setAdapter(mListAdapter);
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.accounts);
        mListText[2] = getString(R.string.categories);
        mListText[3] = getString(R.string.title_activity_acc_summary);
        mListText[4] = getString(R.string.reports);
        mListText[5] = getString(R.string.goals);
        mListText[6] = getString(R.string.recurring);
        mListText[7] = getString(R.string.action_settings);
    }
}
