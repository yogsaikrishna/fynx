package com.fynxapp.constants;

public class Constants {
    public static final String PREFERENCE_NAME = "fynx_prefs";

    public static final String FONT_ROBOTO_LIGHT = "fonts/robotoc_regular.ttf";
    public static final String FONT_ROBOTO = "fonts/robotoc_regular.ttf";

    public static final String FILE_DIR_NAME = "Fynx";

    public static final String DD_MMM_YYYY = "dd-MMM-yyyy";
    public static final String DD_MM_YYYY = "dd-MM-yyyy";

    public static final String DEFAULT_CURRENCY = "\u0024";

    public static final String SYNC_DRIVE_DATE = "sync_drive_date";
    public static final String SYNC_DRIVE_MONTH = "sync_drive_month";
    public static final String SYNC_DRIVE_YEAR = "sync_drive_year";

    public static final String SYNC_BOX_DATE = "sync_box_date";
    public static final String SYNC_BOX_MONTH = "sync_box_month";
    public static final String SYNC_BOX_YEAR = "sync_box_year";

    public static final String REMAINDER_DATE = "remainder_date";
    public static final String REMAINDER_MONTH = "remainder_month";
    public static final String REMAINDER_YEAR = "remainder_year";

    public static final int AUTH_REQUEST = 2;
    public static final int REQUEST_CODE_RESOLUTION = 3;

    public static final String APP_KEY = "d6ffuqhmwbkx7c9";
    public static final String APP_SECRET = "zjz33oi37yjyrx8";

    public static final String ACTION_DRIVE_SYNC = "com.fynxapp.services.action.DRIVE_SYNC";
    public static final String ACTION_DROPBOX_SYNC = "com.fynxapp.services.action.DROPBOX_SYNC";

    public static final String SYNC_TYPE = "sync_type";

    public static final String ACCESS_KEY_NAME = "key_name";
    public static final String ACCESS_SECRET_NAME = "secret_name";

    public static final String PREF_LAST_DRIVE_SYNC = "drive_sync_date";
    public static final String PREF_LAST_BOX_SYNC = "dropbox_sync_date";
    public static final String PREF_DRIVE_FOLDER = "drive_folder";
    public static final String PREF_STORED_FREQUENCY = "stored_frequency";
    public static final String PREF_NOTIFY_TIME = "notify_time";
    public static final String PREF_DATA_CURRENCY = "data_currency";

    public static final String RECURRENCE_ID = "id";

    public static final int DRIVE_REQUEST_CODE = 1990;
    public static final int BOX_REQUEST_CODE = 2890;
    public static final int NOTIFICATION_ID = 890;

}
