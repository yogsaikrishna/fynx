package com.fynxapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.categories.CategoryExpense;
import com.fynxapp.categories.CategoryIncome;
import com.fynxapp.constants.Constants;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class CategoriesActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "CategoriesActivity";

    private String[] mListText;
    private Integer[] imgResources = {R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_accounts, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_objective, R.drawable.ic_action_recurring,
            R.drawable.ic_action_settings};
    private static String[] titles;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter navAdapter;
    private PagerAdapter pagerAdapter;
    private PagerTabStrip pagerTabStrip;
    private ViewPager viewPager;
    private Typeface mRoboto;
    private boolean purchased;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);

        titles = new String[2];
        titles[0] = getString(R.string.income);
        titles[1] = getString(R.string.expense);

        purchased = CommonsUtils.getPrefBoolean(this, "purchased");
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        listView = (ListView) findViewById(R.id.left_drawer);

        initializeNavList();
        addNavigationDrawer();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        listView.setOnItemClickListener(this);

        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        pagerTabStrip = (PagerTabStrip) this.findViewById(R.id.pager_title_strip);
        viewPager = (ViewPager) this.findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        pagerTabStrip.setTabIndicatorColorResource(R.color.tab);
        for (int i = 0; i < pagerTabStrip.getChildCount(); i++) {
            View nextChild = pagerTabStrip.getChildAt(i);
            if (nextChild instanceof TextView) {
                TextView textView = (TextView) nextChild;
                textView.setTypeface(mRoboto);
            }
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.create) {
            startActivity(new Intent(CategoriesActivity.this, AddCategory.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(CategoriesActivity.this, AddIncomeActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(CategoriesActivity.this, AddExpenseActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(CategoriesActivity.this, AccountsActivity.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                intent = new Intent(CategoriesActivity.this, AccSummaryActivity.class);
                startActivity(intent);
                finish();
                break;
            case 4:
                intent = new Intent(CategoriesActivity.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 5:
                intent = new Intent(CategoriesActivity.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 6:
                intent = new Intent(CategoriesActivity.this, RecurringActivity.class);
                startActivity(intent);
                finish();
                break;
            case 7:
                intent = new Intent(CategoriesActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        drawerLayout.closeDrawers();
    }

    public static class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = null;
            switch (i) {
                case 0:
                    fragment = new CategoryIncome();
                    return fragment;
                case 1:
                    fragment = new CategoryExpense();
                    return fragment;
                default:
                    fragment = new CategoryIncome();
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(CategoriesActivity.this, tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                    startActivity(intent);
                    finish();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(CategoriesActivity.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addNavigationDrawer() {
        data = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            if (i == 4 || i == 5) {
                if (purchased) {
                    rowData = new RowData(mListText[i], imgResources[i], false);
                } else {
                    rowData = new RowData(mListText[i], imgResources[i], true);
                }
            } else {
                rowData = new RowData(mListText[i], imgResources[i], false);
            }
            data.add(rowData);
        }
        navAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, data);
        listView.setAdapter(navAdapter);
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.add_expense);
        mListText[2] = getString(R.string.accounts);
        mListText[3] = getString(R.string.title_activity_acc_summary);
        mListText[4] = getString(R.string.reports);
        mListText[5] = getString(R.string.goals);
        mListText[6] = getString(R.string.recurring);
        mListText[7] = getString(R.string.action_settings);
    }
}