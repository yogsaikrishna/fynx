package com.fynxapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.TypefaceSpan;
import java.util.List;
import java.util.Vector;


public class GoalsActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private String[] mListText;
    private Integer[] imgResources = {R.drawable.ic_add, R.drawable.ic_add, R.drawable.ic_accounts,
            R.drawable.ic_categories, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_recurring, R.drawable.ic_action_settings};
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView navList, listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter navAdapter, listAdapter;
    private TextView mNoData;
    private DBHelper db;
    private List<Goal> mGoalList;
    private Typeface mRoboto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goals);

        db = new DBHelper(this);
        mGoalList = db.getGoalsList();

        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        listView = (ListView) this.findViewById(R.id.listView);
        navList = (ListView) this.findViewById(R.id.left_drawer);
        mNoData = (TextView) this.findViewById(R.id.tv_no_data);

        initializeNavList();
        data = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            rowData = new RowData(mListText[i], imgResources[i], false);
            data.add(rowData);
        }
        navAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, data);
        navList.setAdapter(navAdapter);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        navList.setOnItemClickListener(this);

        mNoData.setTypeface(mRoboto);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(GoalsActivity.this, AddGoal.class);
                intent.putExtra("edit", true);
                intent.putExtra("id", mGoalList.get(i).getId());
                startActivity(intent);
            }
        });

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.create) {
            startActivity(new Intent(GoalsActivity.this, AddGoal.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.goals, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(GoalsActivity.this, AddIncomeActivity.class);
                break;
            case 1:
                intent = new Intent(GoalsActivity.this, AddExpenseActivity.class);
                break;
            case 2:
                intent = new Intent(GoalsActivity.this, AccountsActivity.class);
                break;
            case 3:
                intent = new Intent(GoalsActivity.this, CategoriesActivity.class);
                break;
            case 4:
                intent = new Intent(GoalsActivity.this, AccSummaryActivity.class);
                break;
            case 5:
                intent = new Intent(GoalsActivity.this, ReportsActivity.class);
                break;
            case 6:
                intent = new Intent(GoalsActivity.this, RecurringActivity.class);
                break;
            case 7:
                intent = new Intent(GoalsActivity.this, SettingsActivity.class);
                break;
        }
        startActivity(intent);
        drawerLayout.closeDrawers();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoalList = db.getGoalsList();

        if (mGoalList != null
                && mGoalList.size() > 0) {
            mNoData.setVisibility(View.GONE);
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                listAdapter = new ListAdapter(GoalsActivity.this, R.layout.goal_list_item, R.id.tv_amount, mGoalList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(listAdapter);
                        listAdapter.notifyDataSetChanged();
                    }
                });

            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.add_expense);
        mListText[2] = getString(R.string.accounts);
        mListText[3] = getString(R.string.categories);
        mListText[4] = getString(R.string.title_activity_acc_summary);
        mListText[5] = getString(R.string.reports);
        mListText[6] = getString(R.string.recurring);
        mListText[7] = getString(R.string.action_settings);
    }
}