package com.fynxapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Category;
import com.fynxapp.fragments.AccountsFragment;
import com.fynxapp.fragments.AddExpenseFragment;
import com.fynxapp.fragments.AddIncomeFragment;
import com.fynxapp.fragments.CategoriesFragment;
import com.fynxapp.fragments.GoalsFragment;
import com.fynxapp.fragments.MainFragment;
import com.fynxapp.fragments.NavigationFragment;
import com.fynxapp.fragments.OverviewFragment;
import com.fynxapp.fragments.RecurringFragment;
import com.fynxapp.fragments.ReportsFragment;
import com.fynxapp.fragments.ReportsListFragment;
import com.fynxapp.fragments.SummaryFragment;
import com.fynxapp.reports.BarChartFragment;
import com.fynxapp.reports.LineChartFragment;
import com.fynxapp.reports.PieChartFragment;
import com.fynxapp.reports.PieChartNormal;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OverviewActivity extends ActionBarActivity implements View.OnClickListener,
        NavigationFragment.OnFragmentInteractionListener, ReportsListFragment.OnReportSelectionListener {
    private static final String TAG = "OverviewActivity";

    private Button mAddIncome;
    private Button mAddExpense;
    private Button mAccSummary;
    private Button mReports;
    private Button mAccounts;
    private Button mCategories;
    private Button mGoals;
    private Button mRecurring;
    private ImageView mLock, mLockTwo;
    private boolean mUpdated;
    private DBHelper db;
    private Typeface mRoboto;
    private int mInCategoryCount, mExCategoryCount;
    private boolean isMultiPane;
    private boolean purchased;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview);

        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        View multiPaneView = findViewById(R.id.navigation_fragment);
        if (multiPaneView == null) {
            isMultiPane = false;
        } else {
            isMultiPane = true;
        }

        if (!isMultiPane) {
            mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

            mAddIncome = (Button) this.findViewById(R.id.add_income);
            mAddExpense = (Button) this.findViewById(R.id.add_expense);
            mAccSummary = (Button) this.findViewById(R.id.acc_summary);
            mReports = (Button) this.findViewById(R.id.reports);
            mAccounts = (Button) this.findViewById(R.id.accounts);
            mCategories = (Button) this.findViewById(R.id.categories);
            mGoals = (Button) this.findViewById(R.id.bt_goals);
            mRecurring = (Button) this.findViewById(R.id.bt_recurring);

            mLock = (ImageView) this.findViewById(R.id.iv_lock);
            mLockTwo = (ImageView) this.findViewById(R.id.iv_lock_2);

            if (purchased) {
                mLock.setVisibility(View.GONE);
                mLockTwo.setVisibility(View.GONE);
            }

            mAddIncome.setOnClickListener(this);
            mAddExpense.setOnClickListener(this);
            mAccSummary.setOnClickListener(this);
            mReports.setOnClickListener(this);
            mAccounts.setOnClickListener(this);
            mCategories.setOnClickListener(this);
            mRecurring.setOnClickListener(this);
            mGoals.setOnClickListener(this);

            mAddIncome.setTypeface(mRoboto);
            mAddExpense.setTypeface(mRoboto);
            mAccSummary.setTypeface(mRoboto);
            mReports.setTypeface(mRoboto);
            mAccounts.setTypeface(mRoboto);
            mCategories.setTypeface(mRoboto);
            mGoals.setTypeface(mRoboto);
            mRecurring.setTypeface(mRoboto);
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    private void initializeDefaults() {
        String[] incomeCategories = getResources().getStringArray(R.array.income_categories);
        String[] expenseCategories = getResources().getStringArray(R.array.expense_categories);
        String[] accounts = getResources().getStringArray(R.array.accounts_array);
        List<Category> categoryList = new ArrayList<Category>();

        for (int i = 0; i < incomeCategories.length; i++) {
            Category category = new Category();
            category.setType("income");
            category.setName(incomeCategories[i]);
            categoryList.add(category);
        }

        for (int i = 0; i < expenseCategories.length; i++) {
            Category category = new Category();
            category.setType("expense");
            category.setName(expenseCategories[i]);
            categoryList.add(category);
        }

        for (int i = 0; i < accounts.length; i++) {
            Account account = new Account();
            account.setName(accounts[i]);
            db.saveAccount(account);
        }

        db.updateDefaultCategories(categoryList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.global, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            MenuItem settings = menu.findItem(R.id.action_settings);
            settings.setTitle(CommonsUtils.getSpannableString(this, settings.getTitle()));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(OverviewActivity.this, SettingsActivity.class));
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.add_income:
                if (mInCategoryCount <= 0) {
                    Toast.makeText(this, getString(R.string.err_no_cat_income),
                            Toast.LENGTH_SHORT).show();
                } else {
                    intent = new Intent(OverviewActivity.this, AddIncomeActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.add_expense:
                if (mExCategoryCount <= 0) {
                    Toast.makeText(this, getString(R.string.err_no_cat_expense),
                            Toast.LENGTH_SHORT).show();
                } else {
                    intent = new Intent(OverviewActivity.this, AddExpenseActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.accounts:
                intent = new Intent(OverviewActivity.this, AccountsActivity.class);
                startActivity(intent);
                break;
            case R.id.categories:
                intent = new Intent(OverviewActivity.this, CategoriesActivity.class);
                startActivity(intent);
                break;
            case R.id.acc_summary:
                intent = new Intent(OverviewActivity.this, AccSummaryActivity.class);
                startActivity(intent);
                break;
            case R.id.reports:
                intent = new Intent(OverviewActivity.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                } else {
                    startIAP(intent);
                }
                break;
            case R.id.bt_recurring:
                intent = new Intent(OverviewActivity.this, RecurringActivity.class);
                startActivity(intent);
                break;
            case R.id.bt_goals:
                intent = new Intent(OverviewActivity.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                } else {
                    startIAP(intent);
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        db = new DBHelper(this);
        mUpdated = CommonsUtils.getPrefBoolean(this, "firstRun");

        if (!mUpdated) {
            initializeDefaults();
            Map<String, Boolean> tmpMap = new HashMap<String, Boolean>();
            tmpMap.put("firstRun", true);
            CommonsUtils.putPrefBooleans(this, tmpMap);
            AlarmHelper.setNotificationAlarm(this);
        }

        if (!isMultiPane) {
            purchased = CommonsUtils.getPrefBoolean(this, "purchased");
            mInCategoryCount = db.getCategoryCount("income");
            mExCategoryCount = db.getCategoryCount("expense");
            if (purchased) {
                mLock.setVisibility(View.GONE);
                mLockTwo.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isMultiPane) {
            db.close();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    @Override
    public void onFragmentInteraction(int position) {
        if (isMultiPane) {
            boolean shown = false;
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.detail_fragment);
            switch (position) {
                case 0:
                    MainFragment overview = null;
                    if (fragment instanceof  MainFragment) {
                        overview = (MainFragment) fragment;
                        if (overview == null || overview.getShownIndex() != position) {
                            fragment = MainFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = MainFragment.newInstance(position);
                    }
                    break;
                case 1:
                    AddIncomeFragment addIncome = null;
                    if (fragment instanceof AddIncomeFragment) {
                        addIncome = (AddIncomeFragment) fragment;
                        if (addIncome == null || addIncome.getShownIndex() != position) {
                            fragment = AddIncomeFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = AddIncomeFragment.newInstance(position);
                    }
                    break;
                case 2:
                    if (fragment instanceof AddExpenseFragment) {
                        AddExpenseFragment addExpense = (AddExpenseFragment) fragment;
                        if (addExpense == null || addExpense.getShownIndex() != position) {
                            fragment = AddExpenseFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = AddExpenseFragment.newInstance(position);
                    }
                    break;
                case 3:
                    if (fragment instanceof AccountsFragment) {
                        AccountsFragment accounts = (AccountsFragment) fragment;
                        if (accounts == null || accounts.getShownIndex() != position) {
                            fragment = AccountsFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = AccountsFragment.newInstance(position);
                    }
                    break;
                case 4:
                    if (fragment instanceof CategoriesFragment) {
                        CategoriesFragment categories = (CategoriesFragment) fragment;
                        if (categories == null || categories.getShownIndex() != position) {
                            fragment = CategoriesFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = CategoriesFragment.newInstance(position);
                    }
                    break;
                case 5:
                    if (fragment instanceof SummaryFragment) {
                        SummaryFragment summary = (SummaryFragment) fragment;
                        if (summary == null || summary.getShownIndex() != position) {
                            fragment = SummaryFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = SummaryFragment.newInstance(position);
                    }
                    break;
                case 6:
                    if (fragment instanceof ReportsFragment) {
                        ReportsFragment reports = (ReportsFragment) fragment;
                        if (reports == null || reports.getShownIndex() != position) {
                            fragment = ReportsFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = ReportsFragment.newInstance(position);
                    }
                    break;
                case 7:
                    if (fragment instanceof GoalsFragment) {
                        GoalsFragment goals = (GoalsFragment) fragment;
                        if (goals == null || goals.getShownIndex() != position) {
                            fragment = GoalsFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = GoalsFragment.newInstance(position);
                    }
                    break;
                case 8:
                    if (fragment instanceof RecurringFragment) {
                        RecurringFragment recurring = (RecurringFragment) fragment;
                        if (recurring == null || recurring.getShownIndex() != position) {
                            fragment = RecurringFragment.newInstance(position);
                        } else {
                            shown = true;
                        }
                    } else {
                        fragment = RecurringFragment.newInstance(position);
                    }
                    break;
            }
            if (fragment != null && !shown) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        }
    }

    @Override
    public void onReportSelection(int position) {
        if (isMultiPane) {
            ReportsFragment reportsFragment = (ReportsFragment) getSupportFragmentManager().
                    findFragmentById(R.id.detail_fragment);
            if (reportsFragment != null) {
                boolean shown = false;
                Fragment fragment = reportsFragment.getChildFragmentManager().findFragmentById(R.id.child_fragment);
                switch (position) {
                    case 0:
                        if (fragment instanceof PieChartFragment) {
                            PieChartNormal pieChart = (PieChartNormal) fragment;
                            if (pieChart == null || pieChart.getShownIndex() != position) {
                                fragment = PieChartNormal.newInstance(position);
                            } else {
                                shown = true;
                            }
                        } else {
                            fragment = PieChartNormal.newInstance(position);
                        }
                        break;
                    case 1:
                        if (fragment instanceof LineChartFragment) {
                            LineChartFragment lineChart = (LineChartFragment) fragment;
                            if (lineChart == null || lineChart.getShownIndex() != position) {
                                fragment = LineChartFragment.newInstance(position);
                            } else {
                                shown = true;
                            }
                        } else {
                            fragment = LineChartFragment.newInstance(position);
                        }
                        break;
                    case 2:
                        if (fragment instanceof BarChartFragment) {
                            BarChartFragment barChart = (BarChartFragment) fragment;
                            if (barChart == null || barChart.getShownIndex() != position) {
                                fragment = BarChartFragment.newInstance(position);
                            } else {
                                shown = true;
                            }
                        } else {
                            fragment = BarChartFragment.newInstance(position);
                        }
                        break;
                }
                if (fragment != null && !shown) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.child_fragment, fragment);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                }
            }
        }
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    if (result.getMessage().contains("Item Already Owned")) {
                        Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                        tmp.put("purchased", true);
                        CommonsUtils.putPrefBooleans(OverviewActivity.this, tmp);
                        mLock.setVisibility(View.GONE);
                        mLockTwo.setVisibility(View.GONE);
                        if (intent != null)
                            startActivity(intent);
                    }
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(OverviewActivity.this, tmp);
                    //mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    mLock.setVisibility(View.GONE);
                    mLockTwo.setVisibility(View.GONE);
                    if (intent != null)
                        startActivity(intent);
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(OverviewActivity.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null) {
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
