package com.fynxapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.fynxapp.utils.inapp.IabHelper;
import com.fynxapp.utils.inapp.IabResult;
import com.fynxapp.utils.inapp.Purchase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;


public class AccountsActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "AccountsActivity";

    private String[] listText;
    private Integer[] imgResources = {R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_categories, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_objective, R.drawable.ic_action_recurring,
            R.drawable.ic_action_settings};
    private String[] tileColors;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView navList, listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter navAdapter, listAdapter;
    private TextView mNoData;
    private DBHelper db;
    private List<Account> accounts;
    private Typeface mTypeface;
    private boolean purchased;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accounts);

        db = new DBHelper(this);
        accounts = db.getAccountsList();

        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        tileColors = getResources().getStringArray(R.array.tile_colors);

        mTypeface = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        listView = (ListView) this.findViewById(R.id.listView);
        navList = (ListView) this.findViewById(R.id.left_drawer);
        mNoData = (TextView) this.findViewById(R.id.tv_no_data);

        initializeNavList();
        addNavigationDrawer();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        navList.setOnItemClickListener(this);

        mNoData.setTypeface(mTypeface);

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        if (accounts != null
                && accounts.size() > 0) {
            mNoData.setVisibility(View.GONE);
            data = new Vector<RowData>();
            for (int i = 0; i < accounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = accounts.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(AccountsActivity.this, AddAccount.class);
                intent.putExtra("edit", true);
                intent.putExtra("id", accounts.get(i).getId());
                startActivity(intent);
            }
        });

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.create) {
            startActivity(new Intent(AccountsActivity.this, AddAccount.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.categories, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT > 10) {
            MenuItem item = menu.findItem(R.id.create);
            item.setTitle(CommonsUtils.getSpannableString(this, item.getTitle()));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(AccountsActivity.this, AddIncomeActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(AccountsActivity.this, AddExpenseActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(AccountsActivity.this, CategoriesActivity.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                intent = new Intent(AccountsActivity.this, AccSummaryActivity.class);
                startActivity(intent);
                finish();
                break;
            case 4:
                intent = new Intent(AccountsActivity.this, ReportsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 5:
                intent = new Intent(AccountsActivity.this, GoalsActivity.class);
                if (purchased) {
                    startActivity(intent);
                    finish();
                } else {
                    startIAP(intent);
                }
                break;
            case 6:
                intent = new Intent(AccountsActivity.this, RecurringActivity.class);
                startActivity(intent);
                finish();
                break;
            case 7:
                intent = new Intent(AccountsActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        drawerLayout.closeDrawers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accounts = db.getAccountsList();

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        data = new Vector<RowData>();

        if (accounts != null
                && accounts.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < accounts.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = accounts.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void startIAP(final Intent intent) {
        String base64Key = getString(R.string.base64_key);

        mHelper = new IabHelper(this, base64Key);

        final String SKU_FULL = "full_access";

        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {

                        } else {

                        }
                    }
                };

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    return;
                } else if (purchase.getSku().equals(SKU_FULL)) {
                    Map<String, Boolean> tmp = new HashMap<String, Boolean>();
                    tmp.put("purchased", true);
                    CommonsUtils.putPrefBooleans(AccountsActivity.this, tmp);
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    addNavigationDrawer();
                    startActivity(intent);
                    finish();
                }
            }
        };


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                } else {
                    mHelper.launchPurchaseFlow(AccountsActivity.this, SKU_FULL, 10001, mPurchaseFinishedListener, "");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addNavigationDrawer() {
        data = new Vector<RowData>();
        for (int i = 0; i < listText.length; i++) {
            if (i == 4 || i == 5) {
                if (purchased) {
                    rowData = new RowData(listText[i], imgResources[i], false);
                } else {
                    rowData = new RowData(listText[i], imgResources[i], true);
                }
            } else {
                rowData = new RowData(listText[i], imgResources[i], false);
            }
            data.add(rowData);
        }
        navAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, data);
        navList.setAdapter(navAdapter);
        navAdapter.notifyDataSetChanged();
    }

    private void initializeNavList() {
        listText = new String[8];
        listText[0] = getString(R.string.add_income);
        listText[1] = getString(R.string.add_expense);
        listText[2] = getString(R.string.categories);
        listText[3] = getString(R.string.title_activity_acc_summary);
        listText[4] = getString(R.string.reports);
        listText[5] = getString(R.string.goals);
        listText[6] = getString(R.string.recurring);
        listText[7] = getString(R.string.action_settings);
    }
}