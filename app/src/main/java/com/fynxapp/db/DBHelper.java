package com.fynxapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fynxapp.R;
import com.fynxapp.analytics.objects.DataPie;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.db.objects.Category;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.db.objects.Summary;
import com.fynxapp.db.objects.YearSummary;
import com.google.android.gms.drive.internal.am;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "fynxdb";

    private static final String TBL_CATEGORIES = "categories";
    private static final String TBL_ACCOUNTS = "accounts";
    private static final String TBL_LEDGER = "ledger";
    private static final String TBL_RECURRING = "recurring";
    private static final String TBL_GOALS = "goals";

    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_NAME = "name";
    private static final String KEY_DATE = "date";
    private static final String KEY_CREATED_DATE = "created_date";
    private static final String KEY_UPDATED_DATE = "updated_date";
    private static final String KEY_NEXT_DATE = "next_date";
    private static final String KEY_START_DATE = "start_date";
    private static final String KEY_END_DATE = "end_date";
    private static final String KEY_GOAL_AMOUNT = "goal_amount";
    private static final String KEY_GOAL_PERCENT = "goal_percent";

    private static final String KEY_CATEGORY_TYPE = "category_type";

    private static final String KEY_CATEGORY = "category";
    private static final String KEY_ACCOUNT = "account";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_TYPE = "type";
    private static final String KEY_FREQUENCY = "frequency";
    private static final String KEY_RECURRING_ID = "recurring_id";

    private static final String CREATE_TBL_CATEGORIES = "CREATE TABLE "
            + TBL_CATEGORIES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " Text," + KEY_CATEGORY_TYPE + " Text, " + KEY_CREATED_DATE + " DATETIME,"
            + KEY_UPDATED_DATE + " DATETIME" + ")";

    private static final String CREATE_TBL_ACCOUNTS = "CREATE TABLE "
            + TBL_ACCOUNTS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " Text," + KEY_CREATED_DATE + " DATETIME," + KEY_UPDATED_DATE
            + " DATETIME" + ")";

    private static final String CREATE_TBL_LEDGER = "CREATE TABLE "
            + TBL_LEDGER + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORY
            + " TEXT," + KEY_ACCOUNT + " TEXT," + KEY_TITLE
            + " Text," + KEY_DESCRIPTION + " Text,"
            + KEY_DATE + " Text," + KEY_AMOUNT + " REAL," + KEY_CREATED_DATE
            + " DATETIME," + KEY_UPDATED_DATE + " DATETIME," + KEY_TYPE + " Text, "
            + KEY_RECURRING_ID + " INTEGER" + ")";

    private static final String CREATE_TBL_RECURRING = "CREATE TABLE "
            + TBL_RECURRING + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORY
            + " TEXT," + KEY_ACCOUNT + " TEXT," + KEY_TITLE
            + " Text," + KEY_DESCRIPTION + " Text,"
            + KEY_DATE + " Text," + KEY_AMOUNT + " REAL," + KEY_CREATED_DATE
            + " DATETIME," + KEY_NEXT_DATE + " DATETIME," + KEY_TYPE + " Text, "
            + KEY_FREQUENCY + " Text" + ")";

    private static final String CREATE_TBL_GOALS = "CREATE TABLE "
            + TBL_GOALS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TYPE
            + " TEXT," + KEY_TITLE + " Text," + KEY_DESCRIPTION + " Text,"
            + KEY_START_DATE + " Text," + KEY_END_DATE + " Text, " + KEY_AMOUNT
            + " REAL," + KEY_GOAL_AMOUNT + " REAL, " + KEY_GOAL_PERCENT + " REAL, "
            + KEY_CREATED_DATE + " DATETIME" + ")";


    private Context context;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TBL_CATEGORIES);
        sqLiteDatabase.execSQL(CREATE_TBL_ACCOUNTS);
        sqLiteDatabase.execSQL(CREATE_TBL_LEDGER);
        sqLiteDatabase.execSQL(CREATE_TBL_RECURRING);
        sqLiteDatabase.execSQL(CREATE_TBL_GOALS);
        Log.d(TAG, "Tables Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_CATEGORIES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_ACCOUNTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_LEDGER);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_RECURRING);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_GOALS);
    }

    public long saveCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, category.getName());
        values.put(KEY_CATEGORY_TYPE, category.getType());
        values.put(KEY_UPDATED_DATE, getCurrentDate());
        if (category.getId() > 0) {
            return db.update(TBL_CATEGORIES, values, "id = " + category.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_CATEGORIES, null, values);
    }

    public long saveAccount(Account account) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, account.getName());
        values.put(KEY_UPDATED_DATE, getCurrentDate());
        if (account.getId() > 0) {
            return db.update(TBL_ACCOUNTS, values, "id = " + account.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_ACCOUNTS, null, values);
    }

    public long saveIncome(Amount income) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, income.getCategory());
        values.put(KEY_ACCOUNT, income.getAccount());
        values.put(KEY_TITLE, income.getTitle());
        values.put(KEY_DESCRIPTION, income.getDescription());
        values.put(KEY_DATE, convertStringToDate(income.getDate()));
        values.put(KEY_AMOUNT, income.getAmount());
        values.put(KEY_UPDATED_DATE, getCurrentDate());
        values.put(KEY_TYPE, income.getType());
        if (income.getRecurringId() > 0) {
            values.put(KEY_RECURRING_ID, income.getRecurringId());
        }
        if (income.getId() > 0) {
            return db.update(TBL_LEDGER, values, "id = " + income.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_LEDGER, null, values);
    }

    public long saveExpense(Amount expense) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, expense.getCategory());
        values.put(KEY_ACCOUNT, expense.getAccount());
        values.put(KEY_TITLE, expense.getTitle());
        values.put(KEY_DESCRIPTION, expense.getDescription());
        values.put(KEY_DATE, convertStringToDate(expense.getDate()));
        values.put(KEY_AMOUNT, expense.getAmount());
        values.put(KEY_UPDATED_DATE, getCurrentDate());
        values.put(KEY_TYPE, expense.getType());
        if (expense.getRecurringId() > 0) {
            values.put(KEY_RECURRING_ID, expense.getRecurringId());
        }
        if (expense.getId() > 0) {
            return db.update(TBL_LEDGER, values, "id = " + expense.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_LEDGER, null, values);
    }

    public long saveRecurring(Amount amount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, amount.getCategory());
        values.put(KEY_ACCOUNT, amount.getAccount());
        values.put(KEY_TITLE, amount.getTitle());
        values.put(KEY_DESCRIPTION, amount.getDescription());
        values.put(KEY_DATE, convertStringToDate(amount.getDate()));
        values.put(KEY_AMOUNT, amount.getAmount());
        values.put(KEY_NEXT_DATE, amount.getUpdatedDate());
        values.put(KEY_TYPE, amount.getType());
        values.put(KEY_FREQUENCY, amount.getFrequency());
        if (amount.getId() > 0) {
            Log.d(TAG, "Id: " + amount.getId());
            return db.update(TBL_RECURRING, values, "id = " + amount.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_RECURRING, null, values);
    }

    public long saveGoal(Goal goal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TYPE, goal.getType());
        values.put(KEY_TITLE, goal.getTitle());
        values.put(KEY_DESCRIPTION, goal.getDescription());
        values.put(KEY_START_DATE, convertStringToDate(goal.getStartDate()));
        values.put(KEY_END_DATE, convertStringToDate(goal.getEndDate()));
        values.put(KEY_AMOUNT, goal.getAmount());
        if (goal.getId() > 0) {
            double percent = (goal.getGoalAmount() / goal.getAmount()) * 100;
            if (goal.getGoalPercent() <= 100) {
                values.put(KEY_GOAL_AMOUNT, goal.getGoalAmount());
                if (percent > 100)
                    percent = 100;
                values.put(KEY_GOAL_PERCENT, percent);
            }
            return db.update(TBL_GOALS, values, "id = " + goal.getId(), null);
        }
        values.put(KEY_CREATED_DATE, getCurrentDate());
        return db.insert(TBL_GOALS, null, values);
    }

    public List<Category> getCategoriesByType(String type) {
        List<Category> categories = new ArrayList<Category>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        if (type == null)
            query = "SELECT DISTINCT * FROM " + TBL_CATEGORIES;
        else
            query = "SELECT * FROM " + TBL_CATEGORIES + " WHERE " + KEY_CATEGORY_TYPE
                    + " = '" + type + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                category.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                category.setType(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY_TYPE)));
                category.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
                category.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                categories.add(category);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categories;
    }

    public Category getCategoriesById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        query = "SELECT * FROM " + TBL_CATEGORIES + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Category category = new Category();
        if (cursor.moveToFirst()) {
            category.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            category.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            category.setType(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY_TYPE)));
            category.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
            category.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
        }
        cursor.close();
        return category;
    }

    public boolean deleteCategoryById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_CATEGORIES, KEY_ID + " = " + id, null) > 0;
    }

    public int getCategoryCount(String type) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        if (type == null)
            query = "SELECT * FROM " + TBL_CATEGORIES;
        else
            query = "SELECT * FROM " + TBL_CATEGORIES + " WHERE " + KEY_CATEGORY_TYPE
                    + " = '" + type + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null)
            return cursor.getCount();
        cursor.close();
        return 0;
    }

    public List<Amount> getIncomeList() {
        List<Amount> incomeList = new ArrayList<Amount>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_TYPE + " = 'income'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Amount income = new Amount();
                income.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                income.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                income.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
                income.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                income.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                income.setDate(convertISODateToString(date));
                income.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                income.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
                income.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                income.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                incomeList.add(income);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return incomeList;
    }

    public Amount getIncomeById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Amount income = new Amount();
        if (cursor.moveToFirst()) {
            income.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            income.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
            income.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
            income.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
            income.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
            String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
            income.setDate(convertISODateToString(date));
            income.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
            income.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
            income.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
            income.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
        }
        cursor.close();
        return income;
    }

    public boolean deleteIncomeById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_LEDGER, KEY_ID + " = " + id, null) > 0;
    }

    public List<Amount> getExpenseList() {
        List<Amount> expenseList = new ArrayList<Amount>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_TYPE + " = 'expense'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Amount expense = new Amount();
                expense.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                expense.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                expense.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
                expense.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                expense.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                expense.setDate(convertISODateToString(date));
                expense.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                expense.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
                expense.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                expense.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                expenseList.add(expense);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return expenseList;
    }

    public Amount getExpenseById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Amount expense = new Amount();
        if (cursor.moveToFirst()) {
            expense.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            expense.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
            expense.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
            expense.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
            expense.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
            String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
            expense.setDate(convertISODateToString(date));
            expense.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
            expense.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
            expense.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
            expense.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
        }
        cursor.close();
        return expense;
    }

    public int getTodayExpenses() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_UPDATED_DATE
                + " = date('now', 'localtime') AND " + KEY_TYPE + " = 'expense'";
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public boolean deleteExpenseById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_LEDGER, KEY_ID + " = " + id, null) > 0;
    }

    public List<Amount> getRecurringList() {
        List<Amount> expenseList = new ArrayList<Amount>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_RECURRING;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Amount recurring = new Amount();
                recurring.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                recurring.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                recurring.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
                recurring.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                recurring.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                recurring.setDate(convertISODateToString(date));
                recurring.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                recurring.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
                recurring.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_NEXT_DATE)));
                recurring.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                recurring.setFrequency(cursor.getString(cursor.getColumnIndex(KEY_FREQUENCY)));
                expenseList.add(recurring);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return expenseList;
    }

    public Amount getRecurringById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_RECURRING + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Amount recurring = new Amount();
        if (cursor.moveToFirst()) {
            recurring.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            recurring.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
            recurring.setAccount(cursor.getString(cursor.getColumnIndex(KEY_ACCOUNT)));
            recurring.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
            recurring.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
            String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
            recurring.setDate(convertISODateToString(date));
            recurring.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
            recurring.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
            recurring.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_NEXT_DATE)));
            recurring.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
            recurring.setFrequency(cursor.getString(cursor.getColumnIndex(KEY_FREQUENCY)));
        }
        cursor.close();
        return recurring;
    }

    public boolean updateAmountByRecurringId(int id, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM " + TBL_LEDGER + " WHERE " +
                KEY_TYPE + " = '" + type + "' AND " + KEY_RECURRING_ID + " = " + id, null);
        if (cursor.getCount() == 0)
            return true;
        cursor.close();
        return db.delete(TBL_LEDGER, KEY_RECURRING_ID + " = " + id, null) > 0;
    }

    public boolean deleteRecurringById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_RECURRING, KEY_ID + " = " + id, null) > 0;
    }

    public List<Account> getAccountsList() {
        List<Account> accountList = new ArrayList<Account>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_ACCOUNTS;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Account account = new Account();
                account.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                account.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                account.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
                account.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                accountList.add(account);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return accountList;
    }

    public Account getAccountById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        query = "SELECT * FROM " + TBL_ACCOUNTS + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Account account = new Account();
        if (cursor.moveToFirst()) {
            account.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            account.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            account.setCreatedDate(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
            account.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
        }
        cursor.close();
        return account;
    }

    public boolean deleteAccountById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_ACCOUNTS, KEY_ID + " = " + id, null) > 0;
    }

    public void updateDefaultCategories(List<Category> categories) {
        for (int i = 0; i < categories.size(); i++) {
            saveCategory(categories.get(i));
        }
    }

    public double getTotalIncome(String grouping) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;

        if (grouping.equalsIgnoreCase("weekly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'income' AND strftime('%Y-%W'," + KEY_DATE + ") = strftime('%Y-%W', " +
                    " date('now', '-6 days', 'weekday 1'))";
        } else if (grouping.equalsIgnoreCase("monthly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'income' AND strftime('%Y-%m'," + KEY_DATE + ") = strftime('%Y-%m', " +
                    " date('now', 'start of month'))";
        } else if (grouping.equalsIgnoreCase("yearly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'income' AND strftime('%Y'," + KEY_DATE + ") = strftime('%Y', " +
                    " date('now', 'start of year'))";
        }
        if (query != null) {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                return cursor.getDouble(0);
            }
            cursor.close();
        }
        return 0;
    }

    public double getTotalExpense(String grouping) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;
        if (grouping.equalsIgnoreCase("weekly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'expense' AND strftime('%Y-%W'," + KEY_DATE + ") = strftime('%Y-%W', " +
                    " date('now', '-6 days', 'weekday 1'))";
        } else if (grouping.equalsIgnoreCase("monthly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'expense' AND strftime('%Y-%m'," + KEY_DATE + ") = strftime('%Y-%m', " +
                    " date('now', 'start of month'))";
        } else if (grouping.equalsIgnoreCase("yearly")) {
            query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                    KEY_TYPE + " = 'expense' AND strftime('%Y'," + KEY_DATE + ") = strftime('%Y', " +
                    " date('now', 'start of year'))";
        }
        if (query != null) {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                return cursor.getDouble(0);
            }
            cursor.close();
        }
        return 0;
    }

    public double getRollOverBalance() {
        SQLiteDatabase db = this.getReadableDatabase();
        double totalIncome = 0, totalExpense = 0;
        String query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                KEY_TYPE + " = 'income' AND strftime('%Y-%m-%d'," + KEY_DATE + ") < strftime('%Y-%m-%d', " +
                " date('now', 'start of month'))";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            totalIncome = cursor.getDouble(0);
        }
        query = "SELECT SUM(" + KEY_AMOUNT + ") total FROM " + TBL_LEDGER + " WHERE " +
                KEY_TYPE + " = 'expense' AND strftime('%Y-%m-%d'," + KEY_DATE + ") < strftime('%Y-%m-%d', " +
                " date('now', 'start of month'))";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            totalExpense = cursor.getDouble(0);
        }
        cursor.close();
        if (totalIncome > totalExpense)
            return (totalIncome - totalExpense);
        return 0;
    }

    public List<Summary> getAccountSummary() {
        List<Summary> summaryList = new ArrayList<Summary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " ORDER BY " + KEY_DATE + " DESC," +
                " " + KEY_ID + " DESC";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Summary summary = new Summary();
                summary.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                summary.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                summary.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                summary.setDate(convertISODateToString(date));
                summary.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                summary.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                summaryList.add(summary);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return summaryList;
    }

    public List<Summary> getAccountSummary(int days) {
        List<Summary> summaryList = new ArrayList<Summary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_DATE
                + " BETWEEN datetime('now', '" + -days + " days') AND datetime('now', 'localtime')"
                + " ORDER BY " + KEY_DATE + " DESC," +
                " " + KEY_ID + " DESC";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Summary summary = new Summary();
                summary.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                summary.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                summary.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                summary.setDate(convertISODateToString(date));
                summary.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                summary.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                summaryList.add(summary);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return summaryList;
    }

    public List<Summary> getAccountSummary(String type, String name) {
        List<Summary> summaryList = new ArrayList<Summary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;
        if (type.equalsIgnoreCase("account")) {
            query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_ACCOUNT
                    + " IN (" + name + ")" + " ORDER BY " + KEY_DATE + " DESC," +
                    " " + KEY_ID + " DESC";
        } else if (type.equalsIgnoreCase("category")) {
            query = "SELECT * FROM " + TBL_LEDGER + " WHERE " + KEY_CATEGORY
                    + " IN (" + name + ")" + " ORDER BY " + KEY_DATE + " DESC," +
                    " " + KEY_ID + " DESC";
        }
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Summary summary = new Summary();
                summary.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                summary.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                summary.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                summary.setDate(convertISODateToString(date));
                summary.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                summary.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                summaryList.add(summary);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return summaryList;
    }

    public List<YearSummary> getAccountSummary(String year) {
        List<YearSummary> summaryList = new ArrayList<YearSummary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT SUM(" + KEY_AMOUNT + ") total, strftime('%m', " +
                KEY_DATE + ") as month FROM " + TBL_LEDGER + " WHERE strftime('%Y', " +
                KEY_DATE + ") = '" + year + "' AND " + KEY_TYPE + " = 'expense' GROUP BY month";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                YearSummary summary = new YearSummary();
                summary.setAmount(cursor.getDouble(cursor.getColumnIndex("total")));
                summary.setMonth(cursor.getString(cursor.getColumnIndex("month")));
                summaryList.add(summary);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return summaryList;
    }

    public double getAccountBalance(String account) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT (IFNULL(q1.income, 0) - IFNULL(q2.expense, 0)) balance from (SELECT "
                    + " SUM(" + KEY_AMOUNT + ") income FROM " + TBL_LEDGER + " WHERE " + KEY_TYPE
                    + " = 'income' AND " + KEY_ACCOUNT + " = '" + account + "') q1"
                    + " LEFT JOIN (SELECT SUM(" + KEY_AMOUNT + ") expense FROM " + TBL_LEDGER
                    + " WHERE " + KEY_TYPE + " = 'expense' AND " + KEY_ACCOUNT + " = '" + account + "') q2";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            return cursor.getDouble(0);
        }
        return 0;
    }

    public void updateLedger(String type, String newName, String origName, String catType) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        if (type.equals("account")) {
            cursor = db.rawQuery("UPDATE " + TBL_LEDGER + " SET " + KEY_ACCOUNT + " = '" + newName + "'"
                + " WHERE " + KEY_ACCOUNT + " = '" + origName + "'", null);
        } else {
            if (catType.equals("income")) {
                cursor = db.rawQuery("UPDATE " + TBL_LEDGER + " SET " + KEY_CATEGORY + " = '" + newName + "'"
                        + " WHERE " + KEY_CATEGORY + " = '" + origName + "'" + " AND "
                        + KEY_TYPE + " = 'income'", null);
            } else {
                cursor = db.rawQuery("UPDATE " + TBL_LEDGER + " SET " + KEY_CATEGORY + " = '" + newName + "'"
                        + " WHERE " + KEY_CATEGORY + " = '" + origName + "'" + " AND "
                        + KEY_TYPE + " = 'expense'", null);
            }
        }
        cursor.moveToFirst();
        cursor.close();
    }

    public List<DataPie> getExpensePieData(int days, String account, String grouping) {
        List<DataPie> pieData = new ArrayList<DataPie>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;
        String whereClause = null;
        if (grouping.equalsIgnoreCase("weekly")) {
            if (account != null) {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND " + KEY_ACCOUNT + " IN(" + account
                        + ") AND strftime('%Y-%W'," + KEY_DATE + ") = strftime('%Y-%W', " +
                        " date('now', '-6 days', 'weekday 1')) GROUP BY " + KEY_CATEGORY
                        + " ORDER BY " + KEY_AMOUNT + " DESC";
            } else {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND strftime('%Y-%W',"
                        + KEY_DATE + ") = strftime('%Y-%W', " +
                        " date('now', '-6 days', 'weekday 1')) GROUP BY " + KEY_CATEGORY
                        + " ORDER BY " + KEY_AMOUNT + " DESC";
            }
        } else if (grouping.equalsIgnoreCase("monthly")) {
            if (account != null) {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND " + KEY_ACCOUNT + " IN(" + account
                        + ") AND strftime('%Y-%m'," + KEY_DATE + ") = strftime('%Y-%m', " +
                        " date('now', 'start of month')) GROUP BY " + KEY_CATEGORY + " ORDER BY "
                        + KEY_AMOUNT + " DESC";
            } else {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND strftime('%Y-%m',"
                        + KEY_DATE + ") = strftime('%Y-%m', " +
                        " date('now', 'start of month')) GROUP BY " + KEY_CATEGORY + " ORDER BY "
                        + KEY_AMOUNT + " DESC";
            }
        } else if (grouping.equalsIgnoreCase("yearly")) {
            if (account != null) {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND " + KEY_ACCOUNT + " IN(" + account
                        + ") AND strftime('%Y'," + KEY_DATE + ") = strftime('%Y', " +
                        " date('now', 'start of year')) GROUP BY " + KEY_CATEGORY + " ORDER BY "
                        + KEY_AMOUNT + " DESC";
            } else {
                whereClause = " WHERE " + KEY_TYPE + " = 'expense' AND strftime('%Y',"
                        + KEY_DATE + ") = strftime('%Y', " +
                        " date('now', 'start of year')) GROUP BY " + KEY_CATEGORY + " ORDER BY "
                        + KEY_AMOUNT + " DESC";
            }
        }
        if (account != null) {
            query = "SELECT SUM(" + KEY_AMOUNT + ")" + KEY_AMOUNT + ", " +
                    KEY_CATEGORY + " from " + TBL_LEDGER + whereClause;
        } else if (days == -1) {
            query = "SELECT SUM(" + KEY_AMOUNT + ")" + KEY_AMOUNT + ", " +
                    KEY_CATEGORY + " from " + TBL_LEDGER + whereClause;
        } else {
            query = "SELECT SUM(" + KEY_AMOUNT + ")" + KEY_AMOUNT + ", " +
                    KEY_CATEGORY + " from " + TBL_LEDGER + " WHERE " + KEY_TYPE +
                    " = 'expense' AND " + KEY_DATE
                    + " BETWEEN datetime('now', '" + -days + " days') AND datetime('now', 'localtime') "
                    + " GROUP BY " + KEY_CATEGORY
                    + " ORDER BY " + KEY_AMOUNT + " DESC";
        }
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                DataPie data = new DataPie();
                data.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                data.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                pieData.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return pieData;
    }

    public List<Summary> getDataHeader(String grouping) {
        List<Summary> summaryList = new ArrayList<Summary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = getDataHeaderQuery(grouping);
        if (query != null) {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Summary summary = new Summary();
                    summary.setAmount(cursor.getInt(0));
                    summary.setGroup(cursor.getString(1));
                    if (grouping.equalsIgnoreCase("weekly")) {
                        String startDate = cursor.getString(cursor.getColumnIndex("start_date"));
                        summary.setStartDate(startDate);
                    }
                    summaryList.add(summary);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return summaryList;
    }

    public List<Summary> getDataSummary(String grouping) {
        List<Summary> summaryList = new ArrayList<Summary>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;
        if (grouping.equalsIgnoreCase("weekly")) {
            query = " SELECT *, " + "strftime('%Y-%W', " + KEY_DATE + ") day, " +
                    " date(" + KEY_DATE + ", '-6 days', 'weekday 1')" +
                    " start_date FROM " + TBL_LEDGER + " ORDER BY " + KEY_DATE + " DESC," +
                    KEY_ID + " DESC";
        } else if (grouping.equalsIgnoreCase("monthly")) {
            query = " SELECT *, " + "strftime('%Y-%m', " + KEY_DATE + ") day " +
                    " FROM " + TBL_LEDGER + " ORDER BY " + KEY_DATE + " DESC," +
                    KEY_ID + " DESC";
        } else if (grouping.equalsIgnoreCase("yearly")) {
            query = " SELECT *, " + "strftime('%Y', " + KEY_DATE + ") day " +
                    " FROM " + TBL_LEDGER + " ORDER BY " + KEY_DATE + " DESC," +
                    KEY_ID + " DESC";
        }
        if (query != null) {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Summary summary = new Summary();
                    summary.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    summary.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                    summary.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                    String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                    summary.setDate(convertISODateToString(date));
                    summary.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                    summary.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                    summary.setGroup(cursor.getString(cursor.getColumnIndex("day")));
                    if (grouping.equalsIgnoreCase("weekly")) {
                        String startDate = cursor.getString(cursor.getColumnIndex("start_date"));
                        summary.setStartDate(startDate);
                    }
                    summaryList.add(summary);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return summaryList;
    }

    public List<Goal> getGoalsList() {
        ArrayList<Goal> goals = new ArrayList<Goal>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TBL_GOALS, null);
        if (cursor.moveToFirst()) {
            do {
                Goal goal = new Goal();
                goal.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                goal.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                goal.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                goal.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                String date = cursor.getString(cursor.getColumnIndex(KEY_START_DATE));
                goal.setStartDate(convertISODateToString(date));
                date = cursor.getString(cursor.getColumnIndex(KEY_END_DATE));
                goal.setEndDate(convertISODateToString(date));
                goal.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                goal.setGoalAmount(cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_AMOUNT)));
                goal.setGoalPercent(cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_PERCENT)));
                goals.add(goal);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return goals;
    }

    public Goal getGoalById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TBL_GOALS + " WHERE " + KEY_ID
                + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Goal goal = new Goal();
        if (cursor.moveToFirst()) {
            goal.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            goal.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
            goal.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
            goal.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
            String date = cursor.getString(cursor.getColumnIndex(KEY_START_DATE));
            goal.setStartDate(convertISODateToString(date));
            date = cursor.getString(cursor.getColumnIndex(KEY_END_DATE));
            goal.setEndDate(convertISODateToString(date));
            goal.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
            goal.setGoalAmount(cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_AMOUNT)));
            goal.setGoalPercent(cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_PERCENT)));
        }
        cursor.close();
        return goal;
    }

    public boolean deleteGoalById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TBL_GOALS, KEY_ID + " = " + id, null) > 0;
    }

    public void updateGoals(String date, double goalAmt, String type) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TBL_GOALS + " WHERE '" + date + "' >= " + KEY_START_DATE
                + " AND '" + date + "' <= " + KEY_END_DATE;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    double goalAmount = cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_AMOUNT));
                    double amount = cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT));
                    String goalType = cursor.getString(cursor.getColumnIndex(KEY_TYPE));
                    double goalPercent = cursor.getDouble(cursor.getColumnIndex(KEY_GOAL_PERCENT));
                    double percent = 0;

                    Goal goal = new Goal();
                    goal.setId(id);
                    goal.setType(goalType);
                    goal.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                    goal.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                    String dbDate = cursor.getString(cursor.getColumnIndex(KEY_START_DATE));
                    goal.setStartDate(convertISODateToString(dbDate));
                    dbDate = cursor.getString(cursor.getColumnIndex(KEY_END_DATE));
                    goal.setEndDate(convertISODateToString(dbDate));
                    goal.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                    if (goalPercent < 100
                            && goalType.equalsIgnoreCase(type)) {
                        goalAmount += goalAmt;
                        goal.setGoalAmount(goalAmount);
                        percent = (goalAmount / amount) * 100;
                        if (percent > 100) {
                            percent = 100;
                        }
                        goal.setGoalPercent(percent);
                    } else {
                        goalAmount -= goalAmt;
                        goal.setGoalAmount(goalAmount);
                        percent = (goalAmount / amount) * 100;
                        if (percent < 0) {
                            percent = 0;
                        }
                        goal.setGoalPercent(percent);
                    }
                    saveGoal(goal);
                } while (cursor.moveToNext());
            }
        }
    }

    public List<String> getTitlesList(String type) {
        List<String> titles = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT DISTINCT " + KEY_TITLE + " FROM " + TBL_LEDGER + " WHERE " + KEY_TYPE
                + " = '" + type + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null
                && cursor.moveToFirst()) {
            do {
                titles.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return titles;
    }

    private String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String convertStringToDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String convertedDate = null;
        try {
            Date dt = df.parse(date);
            SimpleDateFormat isoDate = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            convertedDate = isoDate.format(dt);
        } catch (Exception e) {
            Log.d(TAG, "Exception occurred: " + e.getMessage());
        }
        return convertedDate;
    }

    private String convertISODateToString(String date) {
        SimpleDateFormat isoDate = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String convertedDate = null;
        try {
            Date dt = isoDate.parse(date);
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            convertedDate = df.format(dt);
        } catch (Exception e) {
            Log.d(TAG, "Exception occurred: " + e.getMessage());
        }
        return convertedDate;
    }

    private String convertDateToString(String date) {
        SimpleDateFormat isoDate = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String convertedDate = null;
        try {
            Date dt = isoDate.parse(date);
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            convertedDate = df.format(dt);
        } catch (Exception e) {
            Log.d(TAG, "Exception occurred: " + e.getMessage());
        }
        return convertedDate;
    }

    private String getDataHeaderQuery(String grouping) {
        String query = null;
        if (grouping.equalsIgnoreCase("weekly")) {
            query = "SELECT count(*) count, strftime('%Y-%W', " + KEY_DATE + ") week, date("
                    + KEY_DATE + ", '-6 days', 'weekday 1') start_date FROM " + TBL_LEDGER
                    + " GROUP BY week ORDER BY week DESC";
        } else if (grouping.equalsIgnoreCase("monthly")) {
            query = "SELECT count(*) count, strftime('%Y-%m', " + KEY_DATE + ") month FROM "
                    + TBL_LEDGER + " GROUP BY month ORDER BY month DESC";
        } else if (grouping.equalsIgnoreCase("yearly")) {
            query = "SELECT count(*) count, strftime('%Y', " + KEY_DATE + ") year FROM "
                    + TBL_LEDGER + " GROUP BY year ORDER BY year DESC";
        }
        return query;
    }

}
