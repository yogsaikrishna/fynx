package com.fynxapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.util.List;
import java.util.Random;
import java.util.Vector;


public class GenericList extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private ListView listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter listAdapter;
    private List<Category> categoryList;
    private List<Account> accountList;
    private DBHelper db;
    private String[] tileColors;
    private TextView mNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > 10) {
            this.overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        }
        setContentView(R.layout.list_layout);

        Bundle bundle = getIntent().getExtras();
        String type = bundle.getString("type");

        db = new DBHelper(this);

        tileColors = getResources().getStringArray(R.array.tile_colors);

        listView = (ListView) this.findViewById(R.id.listView);
        mNoData = (TextView) this.findViewById(R.id.tv_no_data);
        mNoData.setVisibility(View.GONE);

        getSupportActionBar().setTitle(CommonsUtils.getSpannableString(this, this.getTitle()));
        if (type.equals("income")) {
            categoryList = db.getCategoriesByType("income");
        } else if (type.equals("expense")) {
            categoryList = db.getCategoriesByType("expense");
        } else if (type.equals("account")) {
            getSupportActionBar().setTitle(CommonsUtils.getSpannableString(this, getString(R.string.accounts)));
            accountList = db.getAccountsList();
        }

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        data = new Vector<RowData>();
        if (type.equals("income")
                || type.equals("expense")) {
            if (categoryList != null
                    && categoryList.size() > 0) {
                for (int i = 0; i < categoryList.size(); i++) {
                    int rNum = random.nextInt(high - low) + low;
                    String color = tileColors[rNum];
                    String category = categoryList.get(i).getName();
                    rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                    data.add(rowData);
                }
            }
        } else if (type.equals("account")) {
            if (accountList != null
                    && accountList.size() > 0) {
                for (int i = 0; i < accountList.size(); i++) {
                    int rNum = random.nextInt(high - low) + low;
                    String color = tileColors[rNum];
                    String account = accountList.get(i).getName();
                    rowData = new RowData(account, "" + account.charAt(0), Color.parseColor(color), false);
                    data.add(rowData);
                }
            }
        }

        listAdapter = new ListAdapter(this, R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent();
        intent.putExtra("position", i);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
        if (Build.VERSION.SDK_INT > 10) {
            this.overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
}
