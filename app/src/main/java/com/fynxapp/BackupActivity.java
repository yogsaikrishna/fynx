package com.fynxapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fynxapp.constants.Constants;
import com.fynxapp.utils.TypefaceSpan;
import com.ipaulpro.afilechooser.utils.FileUtils;


public class BackupActivity extends ActionBarActivity {
    private static final String TAG = "BackupActivity";
    private static final int REQUEST_CODE = 6384;

    private Button mBackup;
    private Button mRestore;
    private TextView mWarning, mNote;
    private TextView mHdrBackup, mHdrRestore;
    private Typeface mRobotoLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBackup = (Button) this.findViewById(R.id.bt_backup);
        mRestore = (Button) this.findViewById(R.id.bt_restore);
        mWarning = (TextView) this.findViewById(R.id.tv_warning);
        mNote = (TextView) this.findViewById(R.id.tv_backup_note);
        mHdrBackup = (TextView) this.findViewById(R.id.tv_hdr_backup);
        mHdrRestore = (TextView) this.findViewById(R.id.tv_header_restore);

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mBackup.setTypeface(mRobotoLight);
        mRestore.setTypeface(mRobotoLight);

        mWarning.setTypeface(mRobotoLight);
        mNote.setTypeface(mRobotoLight);

        mHdrRestore.setTypeface(mRobotoLight);
        mHdrBackup.setTypeface(mRobotoLight);

        mBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri export = com.fynxapp.utils.FileUtils.exportDB();
                if (export != null) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, export);
                    intent.setType("application/x-sqlite3");
                    startActivity(Intent.createChooser(intent, getString(R.string.backup_location)));
                }
            }
        });

        mRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooser();
            }
        });

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    private void showChooser() {
        Intent target = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(
                target, getString(R.string.chooser_title));
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception occurred: " + e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        final Uri uri = data.getData();
                        Log.i(TAG, "Uri = " + uri.toString());
                        try {
                            final String path = FileUtils.getPath(this, uri);
                            boolean status = com.fynxapp.utils.FileUtils.importDB(path);
                            if (status) {
                                Toast.makeText(BackupActivity.this,
                                        getString(R.string.db_imported), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(BackupActivity.this,
                                        getString(R.string.db_import_fail), Toast.LENGTH_LONG).show();

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "File select error", e);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
