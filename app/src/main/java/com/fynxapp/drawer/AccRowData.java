package com.fynxapp.drawer;

public class AccRowData {
    private String month;
    private String date;
    private double amount;
    private String title;
    private String category;
    private String type;

    public AccRowData(String month, String date, double amount, String title, String category) {
        this.month = month;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.category = category;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
