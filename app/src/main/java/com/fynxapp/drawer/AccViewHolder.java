package com.fynxapp.drawer;

import android.view.View;
import android.widget.TextView;

import com.fynxapp.R;

public class AccViewHolder {
    private View view;
    private TextView month;
    private TextView date;
    private TextView amount;
    private TextView title;
    private TextView category;

    public AccViewHolder(View view) {
        this.view = view;
    }

    public TextView getMonth() {
        if (month == null) {
            month = (TextView) view.findViewById(R.id.tv_month);
        }
        return month;
    }

    public TextView getDate() {
        if (date == null) {
            date = (TextView) view.findViewById(R.id.tv_date);
        }
        return date;
    }

    public TextView getAmount() {
        if (amount == null) {
            amount = (TextView) view.findViewById(R.id.tv_amount);
        }
        return amount;
    }

    public TextView getTitle() {
        if (title == null) {
            title = (TextView) view.findViewById(R.id.tv_title);
        }
        return title;
    }

    public TextView getCategory() {
        if (category == null) {
            category = (TextView) view.findViewById(R.id.tv_category);
        }
        return category;
    }
}
