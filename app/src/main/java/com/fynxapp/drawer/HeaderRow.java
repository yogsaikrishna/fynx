package com.fynxapp.drawer;

public class HeaderRow {
    private String date;
    private String amount;

    public HeaderRow() {}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
