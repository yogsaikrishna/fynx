package com.fynxapp.drawer;

import android.graphics.Color;
import android.widget.LinearLayout;

public class RowData {
    private String primaryText;
    private String secondaryText;
    private int resource;
    private boolean locked;

    public RowData(String primaryText, boolean locked) {
        this.primaryText = primaryText;
        this.locked = locked;
    }

    public RowData(String primaryText, int resource, boolean locked) {
        this.primaryText = primaryText;
        this.resource = resource;
        this.locked = locked;
    }

    public RowData(String primaryText, String secondaryText, boolean locked) {
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;
        this.locked = locked;
    }

    public RowData(String primaryText, String secondaryText, int resource, boolean locked) {
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;
        this.resource = resource;
        this.locked = locked;
    }

    public String getPrimaryText() {
        return primaryText;
    }

    public void setPrimaryText(String primaryText) {
        this.primaryText = primaryText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
