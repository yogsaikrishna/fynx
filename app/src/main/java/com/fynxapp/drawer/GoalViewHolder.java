package com.fynxapp.drawer;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fynxapp.R;

public class GoalViewHolder {
    private View view;
    private TextView dueDays;
    private TextView days;
    private TextView amount;
    private TextView title;
    private TextView goalPercent;
    private ProgressBar progress;
    private TextView goalAmount;

    public GoalViewHolder(View view) {
        this.view = view;
    }

    public TextView getDueDays() {
        if (dueDays == null) {
            dueDays = (TextView) view.findViewById(R.id.tv_days);
        }
        return dueDays;
    }

    public TextView getDays() {
        if (days == null) {
            days = (TextView) view.findViewById(R.id.tv_date);
        }
        return days;
    }

    public TextView getAmount() {
        if (amount == null) {
            amount = (TextView) view.findViewById(R.id.tv_amount);
        }
        return amount;
    }

    public TextView getTitle() {
        if (title == null) {
            title = (TextView) view.findViewById(R.id.tv_title);
        }
        return title;
    }

    public TextView getGoalPercent() {
        if (goalPercent == null) {
            goalPercent = (TextView) view.findViewById(R.id.tv_percent);
        }
        return goalPercent;
    }

    public ProgressBar getProgress() {
        if (progress == null) {
            progress = (ProgressBar) view.findViewById(R.id.goal_progress);
        }
        return progress;
    }

    public TextView getGoalAmount() {
        if (goalAmount == null) {
            goalAmount = (TextView) view.findViewById(R.id.tv_goal_amount);
        }
        return goalAmount;
    }
}
