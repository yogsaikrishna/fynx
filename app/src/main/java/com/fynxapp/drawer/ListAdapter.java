package com.fynxapp.drawer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.utils.DateUtils;

import java.text.NumberFormat;
import java.util.List;

public class ListAdapter<T> extends ArrayAdapter<T> {
    private static final String TAG = "ListAdapter";

    private LayoutInflater inflater;
    private int resource;
    private Typeface mRoboto, mRobotoCond;
    private SharedPreferences mSharedPreferences;
    private String mCurrency;
    private NumberFormat nf;

    public ListAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        mRoboto = Typeface.createFromAsset(context.getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRobotoCond = Typeface.createFromAsset(context.getResources().getAssets(), Constants.FONT_ROBOTO);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);
        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (resource == R.layout.goal_list_item) {
            return initGoalView(position, convertView, parent);
        } else {
            return initView(position, convertView, parent);
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (resource == R.layout.goal_list_item) {
            return initGoalView(position, convertView, parent);
        } else {
            return initView(position, convertView, parent);
        }
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        TextView primaryText;
        TextView secondaryText;
        ImageView lock;
        ImageView image;
        LinearLayout itemTile;

        RowData rowData = (RowData) getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();

        primaryText = holder.getPrimaryText();
        secondaryText = holder.getSecondaryText();
        lock = holder.getLock();
        image = holder.getImage();

        switch (resource) {
            case R.layout.drawer_list_item:
            case R.layout.report_list_item:
            case R.layout.nav_list_item:
                if (rowData.getPrimaryText() != null) {
                    primaryText.setText(rowData.getPrimaryText());
                    primaryText.setTypeface(mRobotoCond);
                }
                if (rowData.isLocked()) {
                    if (lock != null)
                        lock.setVisibility(View.VISIBLE);
                } else {
                    if (lock != null)
                        lock.setVisibility(View.GONE);
                }
                if (rowData.getResource() != -1) {
                    image.setImageResource(rowData.getResource());
                }
                break;
            case R.layout.list_item:
                itemTile = holder.getItemTile();
                if (rowData.getResource() != -1) {
                    itemTile.setBackgroundColor(rowData.getResource());
                }
                if (rowData.getPrimaryText() != null) {
                    primaryText.setText(rowData.getPrimaryText());
                    primaryText.setTypeface(mRoboto);
                }
                if (rowData.getSecondaryText() != null) {
                    secondaryText.setText(rowData.getSecondaryText());
                    secondaryText.setTypeface(mRoboto);
                }
                break;
        }
        return convertView;
    }

    private View initGoalView(int position, View convertView, ViewGroup parent) {
        GoalViewHolder holder;
        TextView dueDays;
        TextView days;
        TextView amount;
        TextView title;
        TextView goalPercent;
        ProgressBar progress;
        TextView goalAmount;

        Goal goal = (Goal) getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
            holder = new GoalViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder = (GoalViewHolder) convertView.getTag();

        dueDays = holder.getDueDays();
        days = holder.getDays();
        amount = holder.getAmount();
        title = holder.getTitle();
        goalPercent = holder.getGoalPercent();
        goalAmount = holder.getGoalAmount();
        progress = holder.getProgress();

        dueDays.setTypeface(mRoboto);
        days.setTypeface(mRoboto);
        amount.setTypeface(mRoboto);
        title.setTypeface(mRoboto);
        goalPercent.setTypeface(mRoboto);
        goalAmount.setTypeface(mRoboto);

        if (goal.getTitle() != null) {
            title.setText(goal.getTitle());
        }

        amount.setText("" + nf.format(goal.getAmount()) + " " + mCurrency);
        goalAmount.setText("" + nf.format(goal.getGoalAmount()) + " " + mCurrency);
        goalPercent.setText((int) goal.getGoalPercent() + "%");
        progress.setProgress((int) goal.getGoalPercent());
        dueDays.setText("" + DateUtils.getDaysBetweenDates(DateUtils.getTodayDate(Constants.DD_MMM_YYYY)
                , goal.getEndDate()));

        return convertView;
    }

 }
