package com.fynxapp.drawer;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fynxapp.R;

public class ViewHolder {
    private View view;
    private TextView primaryText = null;
    private TextView secondaryText = null;
    private ImageView image = null;
    private ImageView lock = null;
    private LinearLayout itemTile = null;

    public ViewHolder(View view) {
        this.view = view;
    }

    public TextView getPrimaryText() {
        if (primaryText == null) {
            primaryText = (TextView) view.findViewById(R.id.tv_primary);
        }
        return primaryText;
    }

    public TextView getSecondaryText() {
        if (secondaryText == null) {
            secondaryText = (TextView) view.findViewById(R.id.tv_secondary);
        }
        return secondaryText;
    }

    public ImageView getImage() {
        if (image == null) {
            image = (ImageView) view.findViewById(R.id.iv_nav_icon);
        }
        return image;
    }

    public ImageView getLock() {
        if (lock == null) {
            lock = (ImageView) view.findViewById(R.id.iv_secondary);
        }
        return lock;
    }

    public LinearLayout getItemTile() {
        if (itemTile == null) {
            itemTile = (LinearLayout) view.findViewById(R.id.item_tile);
        }
        return itemTile;
    }
}
