package com.fynxapp.drawer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;

import java.text.NumberFormat;
import java.util.List;

public class AccListAdapter extends ArrayAdapter<AccRowData> {
    private LayoutInflater inflater;
    private int resource;
    private Typeface mRoboto;
    private SharedPreferences mSharedPreferences;
    private String mCurrency;
    private NumberFormat nf;

    public AccListAdapter(Context context, int resource, int textViewResourceId, List<AccRowData> objects) {
        super(context, resource, textViewResourceId, objects);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        mRoboto = Typeface.createFromAsset(context.getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);
        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        AccViewHolder holder;
        TextView month;
        TextView date;
        TextView amount;
        TextView title;
        TextView category;

        AccRowData rowData = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
            holder = new AccViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder = (AccViewHolder) convertView.getTag();

        month = holder.getMonth();
        date = holder.getDate();
        amount = holder.getAmount();
        title = holder.getTitle();
        category = holder.getCategory();

        switch (resource) {
            case R.layout.account_list_item:
                if (rowData.getMonth() != null) {
                    month.setText(rowData.getMonth());
                    month.setTypeface(mRoboto);
                }
                if (rowData.getDate() != null) {
                    date.setText(rowData.getDate());
                    date.setTypeface(mRoboto);
                }

                amount.setText(nf.format(rowData.getAmount()) + " " + mCurrency);
                amount.setTypeface(mRoboto);

                if (rowData.getTitle() != null) {
                    title.setText(rowData.getTitle());
                    title.setTypeface(mRoboto);
                }
                if (rowData.getCategory() != null) {
                    category.setText(rowData.getCategory());
                    category.setTypeface(mRoboto);
                }
                if (rowData.getType().equals("income")) {
                    amount.setTextColor(Color.parseColor("#669900"));
                } else {
                    amount.setTextColor(Color.parseColor("#cc0000"));
                }
                break;
        }
        return convertView;
    }
}
