package com.fynxapp.lists;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fynxapp.constants.Constants;

import java.util.List;

public class SimpleSpinnerAdapter extends ArrayAdapter<String> {
    private Typeface typeface;

    public SimpleSpinnerAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        typeface = Typeface.createFromAsset(context.getResources().getAssets(), Constants.FONT_ROBOTO);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(typeface);
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(typeface);
        return view;
    }
}
