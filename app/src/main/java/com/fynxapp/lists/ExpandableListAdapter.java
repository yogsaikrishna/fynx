package com.fynxapp.lists;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.drawer.AccRowData;
import com.fynxapp.drawer.AccViewHolder;
import com.fynxapp.drawer.HeaderRow;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<HeaderRow> headerData;
    private HashMap<String, List<AccRowData>> childData;
    private Typeface mRoboto;
    private SharedPreferences mSharedPreferences;
    private String mCurrency;
    private NumberFormat nf;

    public ExpandableListAdapter(Context context, List<HeaderRow> headerData,
                                 HashMap<String, List<AccRowData>> childData) {
        this.context = context;
        this.headerData = headerData;
        this.childData = childData;
        mRoboto = Typeface.createFromAsset(context.getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);
        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
    }

    @Override
    public int getGroupCount() {
        return this.headerData.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return this.childData.get(headerData.get(i).getDate()).size();
    }

    @Override
    public Object getGroup(int i) {
        return this.headerData.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return this.childData.get(this.headerData.get(i).getDate()).get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        HeaderRow headerRow = (HeaderRow) getGroup(i);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) view.findViewById(R.id.lbl_list_header);
        TextView lblAmount = (TextView) view.findViewById(R.id.tv_amount);

        lblListHeader.setTypeface(mRoboto);
        lblListHeader.setText(headerRow.getDate());

        lblAmount.setTypeface(mRoboto);
        lblAmount.setText(headerRow.getAmount());

        return view;
    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
        final AccRowData rowData = (AccRowData) getChild(i, i2);

        AccViewHolder holder;
        TextView month;
        TextView date;
        TextView amount;
        TextView title;
        TextView category;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.account_list_item, null);
            holder = new AccViewHolder(view);
            view.setTag(holder);
        }

        holder = (AccViewHolder) view.getTag();

        month = holder.getMonth();
        date = holder.getDate();
        amount = holder.getAmount();
        title = holder.getTitle();
        category = holder.getCategory();

        if (rowData.getMonth() != null) {
            month.setText(rowData.getMonth());
            month.setTypeface(mRoboto);
        }
        if (rowData.getDate() != null) {
            date.setText(rowData.getDate());
            date.setTypeface(mRoboto);
        }

        amount.setText(nf.format(rowData.getAmount()) + " " + mCurrency);
        amount.setTypeface(mRoboto);

        if (rowData.getTitle() != null) {
            title.setText(rowData.getTitle());
            title.setTypeface(mRoboto);
        }
        if (rowData.getCategory() != null) {
            category.setText(rowData.getCategory());
            category.setTypeface(mRoboto);
        }
        if (rowData.getType().equals("income")) {
            amount.setTextColor(Color.parseColor("#669900"));
        } else {
            amount.setTextColor(Color.parseColor("#cc0000"));
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
