package com.fynxapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fynxapp.constants.Constants;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.reports.BarChartFragment;
import com.fynxapp.reports.LineChartFragment;
import com.fynxapp.reports.PieChartFragment;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.util.Vector;


public class ReportsActivity extends ActionBarActivity {
    private static final String TAG = "ReportsActivity";

    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_action_pie_dark, R.drawable.ic_action_line_dark,
            R.drawable.ic_action_bar_dark};

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mListView;
    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports);

        mListText = getResources().getStringArray(R.array.reports_list);
        mListView = (ListView) this.findViewById(R.id.left_drawer);

        mData = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            mRowData = new RowData(mListText[i], mImgResources[i], false);
            mData.add(mRowData);
        }
        mListAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, mData);
        mListView.setAdapter(mListAdapter);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mListView.setOnItemClickListener(new DrawerItemClickListener());

        if (savedInstanceState == null) {
            Fragment fragment = new PieChartFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            setTitle(mListText[0]);
        }

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new PieChartFragment();
                    break;
                case 1:
                    fragment = new LineChartFragment();
                    break;
                case 2:
                    fragment = new BarChartFragment();
                    break;
                default:
                    fragment = new PieChartFragment();
                    break;
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            mListView.setItemChecked(position, true);
            setTitle(mListText[position]);
            mDrawerLayout.closeDrawer(mListView);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(CommonsUtils.getSpannableString(this, mTitle));
    }
}
