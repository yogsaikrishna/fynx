package com.fynxapp.reports;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.analytics.objects.DataPie;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Account;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Legend;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PieChartNormal extends Fragment {
    private static final String ARG_INDEX = "index";
    private static final int ACCOUNT_MENU_ID = 256;
    private PieChart mPieChart;
    private Typeface mTypeFace;
    private DBHelper db;
    private List<DataPie> dataPie;
    private List<Account> mAccounts;
    private double totalExpense;
    private LinearLayout mLayout;
    private TextView mNoData;
    private ActionBarActivity mActivity;

    private SharedPreferences mSharedPreferences;
    private String mCurrency, mGrouping;
    private SpannableString mSummary;
    private String[] tileColors;
    private NumberFormat nf;
    private List<String> mCheckedAccounts;

    public static PieChartNormal newInstance(int index) {
        PieChartNormal fragment = new PieChartNormal();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public PieChartNormal() {

    }

    public int getShownIndex() {
        if (getArguments() != null)
            return getArguments().getInt(ARG_INDEX, 0);
        return 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActionBarActivity) getActivity();
        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pie_chart, container, false);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");


        db = new DBHelper(getActivity());
        mAccounts = db.getAccountsList();
        mCheckedAccounts = new ArrayList<String>();

        tileColors = getResources().getStringArray(R.array.tile_colors);

        mPieChart = (PieChart) view.findViewById(R.id.pie_chart);
        mLayout = (LinearLayout) view.findViewById(R.id.pie_chart_layout);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);

        mTypeFace = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mPieChart.setValueTypeface(mTypeFace);
        mNoData.setTypeface(mTypeFace);

        mPieChart.setBackgroundColor(Color.parseColor("#f2f2f2"));
        mPieChart.setHoleRadius(75f);
        mPieChart.setDescription(mActivity.getString(R.string.expenses));
        mPieChart.setDrawCenterText(true);
        mPieChart.setDrawHoleEnabled(true);
        mPieChart.setRotationAngle(0);
        mPieChart.setDrawXValues(false);
        mPieChart.setRotationEnabled(true);
        mPieChart.setUnit(mCurrency);
        mPieChart.setDrawUnitsInChart(true);
        mPieChart.setDrawLegend(false);
        mPieChart.setClickable(false);
        mPieChart.setHighlightEnabled(true);
        mPieChart.setDrawYValues(false);
        mPieChart.setCenterTextTypeface(mTypeFace);
        mPieChart.setNoDataText("");

        updateChart(-1, null);

        mPieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex) {
                DataPie data = dataPie.get(e.getXIndex());
                mPieChart.setCenterText(data.getCategory() + "\n" + nf.format(e.getVal()) + " " + mCurrency);
            }

            @Override
            public void onNothingSelected() {
                mPieChart.setCenterText(getString(R.string.total_expenses) + "\n"
                        + nf.format(totalExpense) + " " + mCurrency);
            }
        });

        return view;
    }

    private void updateChart(int days, String account) {
        int colors[];
        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();

        totalExpense = 0;

        if (dataPie != null
                && dataPie.size() > 0) {
            mLayout.removeViews(1, dataPie.size());
        }

        dataPie = db.getExpensePieData(days, account, mGrouping);

        if (dataPie != null
                && dataPie.size() > 0) {
            mNoData.setVisibility(View.GONE);
            colors = new int[dataPie.size()];

            for (int i = 0; i < dataPie.size(); i++) {
                totalExpense += dataPie.get(i).getAmount();
            }

            for (int i = 0; i < dataPie.size(); i++) {
                DataPie data = dataPie.get(i);
                xVals.add(data.getCategory());
                yVals.add(new Entry((float) data.getAmount(), i));
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                colors[i] = Color.parseColor(color);

                View item = View.inflate(getActivity(), R.layout.pie_chart_item, null);
                TextView mTitle = (TextView) item.findViewById(R.id.tv_name);
                TextView mValue = (TextView) item.findViewById(R.id.tv_value);
                LinearLayout mTile = (LinearLayout) item.findViewById(R.id.item_tile);

                mTitle.setTypeface(mTypeFace);
                mValue.setTypeface(mTypeFace);

                mTitle.setText(data.getCategory());
                mValue.setText(String.format("%.2f", (data.getAmount() / totalExpense) * 100) + "%");
                mValue.setTextColor(colors[i]);
                mTile.setBackgroundColor(colors[i]);
                mLayout.addView(item);
            }

            PieDataSet dataSet = new PieDataSet(yVals, getString(R.string.expenses));
            dataSet.setSliceSpace(3f);
            dataSet.setColors(colors);

            PieData data = new PieData(xVals, dataSet);
            mPieChart.setData(data);

            mPieChart.highlightValues(null);

            mPieChart.setCenterText(getString(R.string.total_expenses) + "\n"
                    + nf.format(totalExpense) + " " + mCurrency);


            Legend l = mPieChart.getLegend();
            l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
            l.setXEntrySpace(7f);
            l.setYEntrySpace(5f);
        } else {
            mNoData.setVisibility(View.VISIBLE);
            PieDataSet dataSet = new PieDataSet(yVals, getString(R.string.expenses));
            PieData data = new PieData(xVals, dataSet);
            mPieChart.setData(data);
        }
        mPieChart.invalidate();
        mPieChart.animateXY(1500, 1500);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.pie_chart, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Menu accSubMenu = menu.findItem(R.id.by_account).getSubMenu();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            MenuItem item = menu.findItem(R.id.all);
            item.setTitle(CommonsUtils.getSpannableString(getActivity(), item.getTitle()));
            item = menu.findItem(R.id.week);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.month);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.year);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.by_account);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
            item = menu.findItem(R.id.by_date);
            item.setTitle(CommonsUtils.getSpannableString(mActivity, item.getTitle()));
        }
        if (mCheckedAccounts != null
                && mCheckedAccounts.size() == 0)
            accSubMenu.clear();
        if (mAccounts != null
                && mAccounts.size() > 0) {
            if (accSubMenu.findItem(ACCOUNT_MENU_ID) == null) {
                for (int i = 0; i < mAccounts.size(); i++) {
                    Account account = mAccounts.get(i);
                    SpannableString s = CommonsUtils.getSpannableString(mActivity, account.getName());
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, s).setCheckable(true);
                    else
                        accSubMenu.add(0, ACCOUNT_MENU_ID, 0, account.getName()).setCheckable(true);
                }
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.all:
                mCheckedAccounts.clear();
                mActivity.supportInvalidateOptionsMenu();
                updateChart(-1, null);
                mActivity.getSupportActionBar().setSubtitle(null);
                break;
            case R.id.week:
                updateChart(7, null);
                mSummary = new SpannableString(mActivity.getString(R.string.filter_week));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.month:
                updateChart(30, null);
                mSummary = new SpannableString(mActivity.getString(R.string.filter_month));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case R.id.year:
                updateChart(90, null);
                mSummary = new SpannableString(mActivity.getString(R.string.filter_year));
                mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mActivity.getSupportActionBar().setSubtitle(mSummary);
                break;
            case ACCOUNT_MENU_ID:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mCheckedAccounts.remove(item.getTitle().toString());
                } else {
                    item.setChecked(true);
                    mCheckedAccounts.add(item.getTitle().toString());
                }

                if (mCheckedAccounts.size() > 0) {
                    updateChart(-1, CommonsUtils.joinList(",", mCheckedAccounts));
                    mSummary = new SpannableString(mActivity.getString(R.string.filtered_account));
                    mSummary.setSpan(new TypefaceSpan(mActivity, Constants.FONT_ROBOTO), 0, mSummary.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    mActivity.getSupportActionBar().setSubtitle(mSummary);
                } else {
                    updateChart(-1, null);
                    mActivity.getSupportActionBar().setSubtitle(null);
                }

                break;

        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCheckedAccounts.clear();
        mActivity.supportInvalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
