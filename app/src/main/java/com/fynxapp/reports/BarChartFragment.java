package com.fynxapp.reports;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.YearSummary;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;

import java.util.ArrayList;
import java.util.List;

public class BarChartFragment extends Fragment {
    private static final String ARG_INDEX = "index";
    private static final String TAG = "BarChartFragment";

    private BarChart mChart;
    private DBHelper db;
    private List<YearSummary> summaryList;

    private SharedPreferences mSharedPreferences;
    private String mCurrency;

    public static BarChartFragment newInstance(int index) {
        BarChartFragment fragment = new BarChartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public BarChartFragment() {

    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bar_chart, container, false);

        db = new DBHelper(getActivity());
        summaryList = db.getAccountSummary("2014");
        db.close();

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mCurrency = mSharedPreferences.getString(Constants.PREF_DATA_CURRENCY, Constants.DEFAULT_CURRENCY);

        mChart = (BarChart) view.findViewById(R.id.bar);
        mChart.setDrawYValues(true);
        mChart.setDescription("");
        mChart.setMaxVisibleValueCount(60);
        mChart.set3DEnabled(false);
        mChart.setPinchZoom(false);
        mChart.setUnit(" " + mCurrency);

        YLabels yLabels = mChart.getYLabels();
        yLabels.setPosition(YLabels.YLabelPosition.BOTH_SIDED);

        XLabels xLabels = mChart.getXLabels();
        xLabels.setPosition(XLabels.XLabelPosition.TOP);
        // mChart.setDrawXLabels(false);

        mChart.setDrawGridBackground(false);
        mChart.setDrawHorizontalGrid(true);
        mChart.setDrawVerticalGrid(false);
        // mChart.setDrawYLabels(false);

        mChart.setValueTextSize(10f);

        mChart.setDrawBorder(false);

        Typeface tf = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);

        XLabels xl = mChart.getXLabels();
        xl.setPosition(XLabels.XLabelPosition.BOTTOM);
        xl.setCenterXLabelText(true);
        xl.setTypeface(tf);

        YLabels yl = mChart.getYLabels();
        yl.setTypeface(tf);
        yl.setLabelCount(8);

        mChart.setValueTypeface(tf);

        setData();

        Legend l = mChart.getLegend();
        if (l != null) {
            l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
            l.setFormSize(8f);
            l.setXEntrySpace(4f);
        }

        mChart.animateXY(1500, 1500);

        return view;
    }

    private void setData() {
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> yVals = new ArrayList<BarEntry>();

        if (summaryList != null
                && summaryList.size() > 0) {
            for (int i = 0; i < summaryList.size(); i++) {
                YearSummary summary = summaryList.get(i);
                int month = Integer.parseInt(summary.getMonth());
                xVals.add(DateUtils.getMonth(month - 1));
                yVals.add(new BarEntry((float) summary.getAmount(), i));
            }
        }

        BarDataSet dataSet = new BarDataSet(yVals, getString(R.string.expenses));
        dataSet.setBarSpacePercent(35f);

        dataSet.setColor(getResources().getColor(R.color.red));

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(dataSet);

        BarData data = new BarData(xVals, dataSets);

        mChart.setData(data);
    }
}
