package com.fynxapp.reports;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fynxapp.R;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Summary;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

public class LineChartFragment extends Fragment {
    private static final String ARG_INDEX = "index";

    private LineChart mChart;
    private DBHelper db;
    private List<Summary> mSummaryList;

    public static LineChartFragment newInstance(int index) {
        LineChartFragment fragment = new LineChartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public LineChartFragment() {

    }

    public int getShownIndex() {
        return getArguments().getInt(ARG_INDEX, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.line_chart, container, false);

        db = new DBHelper(getActivity());
        mSummaryList = db.getAccountSummary(30);
        db.close();

        mChart = (LineChart) view.findViewById(R.id.line_chart);

        mChart.setStartAtZero(false);
        mChart.setDrawYValues(true);

        mChart.setDrawBorder(true);
        mChart.setBorderPositions(new BarLineChartBase.BorderPosition[] {
                BarLineChartBase.BorderPosition.BOTTOM
        });

        mChart.setDescription("");
        mChart.setHighlightEnabled(true);
        mChart.setTouchEnabled(false);
        mChart.setDragScaleEnabled(true);
        mChart.setPinchZoom(true);
        mChart.setHighlightIndicatorEnabled(false);
        mChart.setBackgroundColor(getResources().getColor(R.color.white));

        setData();

        mChart.animateX(1500);

        return view;
    }

    private void setData() {
        if (mSummaryList != null
                && mSummaryList.size() > 0) {

            ArrayList<String> xVals = new ArrayList<String>();
            for (int i = 0; i < mSummaryList.size(); i++) {
                xVals.add(mSummaryList.get(i).getDate());
            }

            ArrayList<Entry> dataIncome = new ArrayList<Entry>();
            ArrayList<Entry> dataExpense = new ArrayList<Entry>();

            int in = 0, ex = 0;
            for (int i = 0; i < mSummaryList.size(); i++) {
                Summary summary = mSummaryList.get(i);
                if (summary.getType().equals("income")) {
                    dataIncome.add(new Entry((float) summary.getAmount(), in));
                    in++;
                } else {
                    dataExpense.add(new Entry((float) summary.getAmount(), ex));
                    ex++;
                }
            }

            LineDataSet dsIncome = new LineDataSet(dataIncome, getString(R.string.income));
            dsIncome.setLineWidth(2.5f);
            dsIncome.setCircleSize(4f);
            dsIncome.setColor(getResources().getColor(R.color.green));
            dsIncome.setCircleColor(getResources().getColor(R.color.green));

            LineDataSet dsExpense = new LineDataSet(dataExpense, getString(R.string.expense));
            dsExpense.setLineWidth(2.5f);
            dsExpense.setCircleSize(4f);
            dsExpense.setColor(getResources().getColor(R.color.red));
            dsExpense.setCircleColor(getResources().getColor(R.color.red));

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(dsIncome);
            dataSets.add(dsExpense);

            LineData data = new LineData(xVals, dataSets);
            mChart.setData(data);
            mChart.invalidate();
        }
    }
}
