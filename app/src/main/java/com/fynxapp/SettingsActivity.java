package com.fynxapp;

import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;
import com.fynxapp.constants.Constants;
import com.fynxapp.settings.PreferenceListener;
import com.fynxapp.settings.TimePreference;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.TypefaceSpan;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.haibison.android.lockpattern.LockPatternActivity;
import com.haibison.android.lockpattern.util.Settings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingsActivity extends PreferenceActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "SettingsActivity";

    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    private static final int REQ_CREATE_PATTERN = 1;
    private char[] savedPattern;

    private GoogleApiClient mGoogleApiClient;

    private DropboxAPI<AndroidAuthSession> mApi;
    private AppKeyPair mAppKeys;
    private AndroidAuthSession mSession;
    private String folder, token;
    private SharedPreferences prefs;
    private Preference boxSync, driveSync, backup,
            syncPreference, time, lock;
    private boolean purchased;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        folder = CommonsUtils.getPrefString(this, Constants.PREF_DRIVE_FOLDER);
        token = CommonsUtils.getPrefString(this, Constants.ACCESS_SECRET_NAME);
        purchased = CommonsUtils.getPrefBoolean(this, "purchased");

        setupSimplePreferencesScreen();

        if (Build.VERSION.SDK_INT > 10) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            SpannableString s = new SpannableString(this.getTitle());
            s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            getActionBar().setTitle(s);
        }
    }

    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        addPreferencesFromResource(R.xml.pref_general);

        PreferenceCategory fakeHeader = new PreferenceCategory(this);
        fakeHeader.setTitle(R.string.pref_header_notifications);
        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_notification);

        fakeHeader = new PreferenceCategory(this);
        fakeHeader.setTitle(R.string.pref_header_data_sync);
        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_data_sync);

        fakeHeader = new PreferenceCategory(this);
        fakeHeader.setTitle(R.string.pref_backup);
        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_backup_restore);

        fakeHeader = new PreferenceCategory(this);
        fakeHeader.setTitle(R.string.pref_feedback);
        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_feedback);


        bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"), this);
        bindPreferenceSummaryToValue(findPreference("data_currency"), this);
        bindPreferenceSummaryToValue(findPreference("sync_frequency"), this);
        bindPreferenceSummaryToValue(findPreference("grouping_list"), this);

        prefs = getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);

        mAppKeys = new AppKeyPair(Constants.APP_KEY, Constants.APP_SECRET);
        mSession = new AndroidAuthSession(mAppKeys);
        mApi = new DropboxAPI<AndroidAuthSession>(mSession);

        boxSync = findPreference("dropbox_sync");
        driveSync = findPreference("drive_sync");
        backup = findPreference("backup_db");
        syncPreference = findPreference("sync_frequency");
        time = findPreference("alarm_time");
        lock = findPreference("startup_lock");


        if (purchased) {
            driveSync.setEnabled(true);
            if (Build.VERSION.SDK_INT > 10)
                driveSync.setIcon(null);
        } else {
            driveSync.setEnabled(false);
            if (Build.VERSION.SDK_INT > 10) {
                driveSync.setIcon(getResources().getDrawable(R.drawable.ic_action_secure));
            }
        }

        savedPattern = Settings.Security.getPattern(this);

        if (savedPattern != null) {
            lock.setTitle(R.string.disable_lock);
        }

        if (folder != null
                || token != null) {
            syncPreference.setEnabled(true);
        } else {
            syncPreference.setEnabled(false);
        }

        String driveLastSync = CommonsUtils.getPrefString(this, Constants.PREF_LAST_DRIVE_SYNC);
        String boxLastSync = CommonsUtils.getPrefString(this, Constants.PREF_LAST_BOX_SYNC);

        if (driveLastSync != null) {
            driveSync.setSummary("Last synced: " + driveLastSync);
        } else {
            driveSync.setSummary("Last synced: Never");
        }

        if (boxLastSync != null) {
            boxSync.setSummary("Last synced: " + boxLastSync);
        } else {
            boxSync.setSummary("Last synced: Never");
        }

        driveSync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (folder == null) {
                    if (mGoogleApiClient == null) {
                        mGoogleApiClient = new GoogleApiClient.Builder(SettingsActivity.this)
                                .addApi(Drive.API)
                                .addScope(Drive.SCOPE_FILE)
                                .addConnectionCallbacks(SettingsActivity.this)
                                .addOnConnectionFailedListener(SettingsActivity.this)
                                .build();
                    }

                    mGoogleApiClient.connect();
                } else {
                    Toast.makeText(SettingsActivity.this, "Already connected to Google Drive"
                            , Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        boxSync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (token == null) {
                    mApi.getSession().startOAuth2Authentication(SettingsActivity.this);
                } else {
                    Toast.makeText(SettingsActivity.this, "Already connected to Dropbox"
                            , Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        backup.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(SettingsActivity.this, BackupActivity.class);
                startActivity(intent);
                return true;
            }
        });

        time.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (preference instanceof TimePreference) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Constants.PREF_NOTIFY_TIME, o.toString());
                    editor.commit();
                    AlarmHelper.setNotificationAlarm(SettingsActivity.this);
                }
                return true;
            }
        });

        lock.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (savedPattern != null) {
                    Settings.Security.setPattern(SettingsActivity.this, null);
                    lock.setTitle(R.string.startup_lock);
                } else {
                    Settings.Security.setAutoSavePattern(SettingsActivity.this, true);
                    Intent intent = new Intent(LockPatternActivity.ACTION_CREATE_PATTERN, null,
                            SettingsActivity.this, LockPatternActivity.class);
                    startActivityForResult(intent, REQ_CREATE_PATTERN);
                }
                return true;
            }
        });

        Preference rateApp = findPreference("rate_app");
        rateApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent rateUsIntent = new Intent(Intent.ACTION_VIEW);
                rateUsIntent.setData(Uri.parse("market://details?id=com.fynxapp"));
                startActivity(rateUsIntent);
                return false;
            }
        });

        Preference feedback = findPreference("feedback");
        feedback.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"yogasaikrishna@gmail.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Fynx - Expense manager Feedback");

                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                return false;
            }
        });
    }

    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !isXLargeTablet(context);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        if (!isSimplePreferences(this)) {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    private static void bindPreferenceSummaryToValue(Preference preference, Context context) {
        Preference.OnPreferenceChangeListener mListener = new PreferenceListener(context);
        preference.setOnPreferenceChangeListener(mListener);
        mListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    public void onConnected(Bundle bundle) {
        String folder = CommonsUtils.getPrefString(this, Constants.PREF_DRIVE_FOLDER);

        if (folder == null) {
            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                    .setTitle(Constants.FILE_DIR_NAME).build();
            Drive.DriveApi.getRootFolder(mGoogleApiClient).createFolder(
                    mGoogleApiClient, changeSet).setResultCallback(folderResultResultCallback);
        }
    }

    ResultCallback<DriveFolder.DriveFolderResult> folderResultResultCallback = new
            ResultCallback<DriveFolder.DriveFolderResult>() {
                @Override
                public void onResult(DriveFolder.DriveFolderResult driveFolderResult) {
                    if (!driveFolderResult.getStatus().isSuccess()) {
                        Toast.makeText(SettingsActivity.this, "Error while trying to create the folder",
                                Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Toast.makeText(SettingsActivity.this, "Created Folder", Toast.LENGTH_SHORT)
                                .show();
                        String folderId = driveFolderResult.getDriveFolder().getDriveId().encodeToString();

                        Map<String, String> tmp = new HashMap<String, String>();
                        tmp.put("drive_folder", folderId);
                        CommonsUtils.putPrefStrings(SettingsActivity.this, tmp);

                        syncPreference.setEnabled(true);
                        setAlarm("drive", SettingsActivity.this);

                        Log.d(TAG, "Folder Id: " + folderId);
                    }
                }
            };

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, Constants.REQUEST_CODE_RESOLUTION);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Exception occurred: " + e.getMessage());
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        private Preference lock;
        private char[] savedPattern;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            bindPreferenceSummaryToValue(findPreference("data_currency"), getActivity());
            bindPreferenceSummaryToValue(findPreference("grouping_list"), getActivity());
            savedPattern = Settings.Security.getPattern(getActivity());
            lock = findPreference("startup_lock");

            if (savedPattern != null) {
                lock.setTitle(R.string.disable_lock);
            }

            lock.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (savedPattern != null) {
                        Settings.Security.setPattern(getActivity(), null);
                        lock.setTitle(R.string.startup_lock);
                    } else {
                        Settings.Security.setAutoSavePattern(getActivity(), true);
                        Intent intent = new Intent(LockPatternActivity.ACTION_CREATE_PATTERN, null,
                                getActivity(), LockPatternActivity.class);
                        startActivityForResult(intent, REQ_CREATE_PATTERN);
                    }
                    return true;
                }
            });
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case REQ_CREATE_PATTERN:
                    if (resultCode == RESULT_OK) {
                        lock.setTitle(R.string.disable_lock);
                        char[] pattern = data.getCharArrayExtra(
                                LockPatternActivity.EXTRA_PATTERN);
                    }
                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);

            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"), getActivity());
            Preference time = findPreference("alarm_time");
            time.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    if (preference instanceof TimePreference) {
                        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.PREFERENCE_NAME,
                                Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constants.PREF_NOTIFY_TIME, o.toString());
                        editor.commit();
                        AlarmHelper.setNotificationAlarm(getActivity());
                    }
                    return true;
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DataSyncPreferenceFragment extends PreferenceFragment
            implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private GoogleApiClient mGoogleApiClient;
        private DropboxAPI<AndroidAuthSession> mApi;
        private AppKeyPair mAppKeys;
        private AndroidAuthSession mSession;
        private static Preference syncPreference;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);

            bindPreferenceSummaryToValue(findPreference("sync_frequency"), getActivity());
            Preference boxSync = findPreference("dropbox_sync");
            Preference driveSync = findPreference("drive_sync");
            syncPreference = findPreference("sync_frequency");

            if (Build.VERSION.SDK_INT > 10)
                driveSync.setIcon(getResources().getDrawable(R.drawable.ic_action_secure));

            final String folder = CommonsUtils.getPrefString(getActivity(), Constants.PREF_DRIVE_FOLDER);
            final String token = CommonsUtils.getPrefString(getActivity(), Constants.ACCESS_SECRET_NAME);
            boolean purchased = CommonsUtils.getPrefBoolean(getActivity(), "purchased");

            if (purchased) {
                driveSync.setEnabled(true);
                if (Build.VERSION.SDK_INT > 10)
                    driveSync.setIcon(null);
            } else {
                driveSync.setEnabled(false);
                if (Build.VERSION.SDK_INT > 10) {
                    driveSync.setIcon(getResources().getDrawable(R.drawable.ic_action_secure));
                }
            }

            if (folder != null
                    || token != null) {
                syncPreference.setEnabled(true);
            } else {
                syncPreference.setEnabled(false);
            }

            String driveLastSync = CommonsUtils.getPrefString(getActivity(), Constants.PREF_LAST_DRIVE_SYNC);
            String boxLastSync = CommonsUtils.getPrefString(getActivity(), Constants.PREF_LAST_BOX_SYNC);

            if (driveLastSync != null) {
                driveSync.setSummary("Last synced: " + driveLastSync);
            } else {
                driveSync.setSummary("Last synced: Never");
            }

            if (boxLastSync != null) {
                boxSync.setSummary("Last synced: " + boxLastSync);
            } else {
                boxSync.setSummary("Last synced: Never");
            }

            mAppKeys = new AppKeyPair(Constants.APP_KEY, Constants.APP_SECRET);
            mSession = new AndroidAuthSession(mAppKeys);
            mApi = new DropboxAPI<AndroidAuthSession>(mSession);

            driveSync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (folder == null) {
                        if (mGoogleApiClient == null) {
                            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                                    .addApi(Drive.API)
                                    .addScope(Drive.SCOPE_FILE)
                                    .addConnectionCallbacks(DataSyncPreferenceFragment.this)
                                    .addOnConnectionFailedListener(DataSyncPreferenceFragment.this)
                                    .build();
                        }

                        mGoogleApiClient.connect();
                    } else {
                        Toast.makeText(getActivity(), "Already connected to Google Drive"
                                , Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });

            boxSync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (token == null) {
                        mApi.getSession().startOAuth2Authentication(getActivity());
                    } else {
                        Toast.makeText(getActivity(), "Already connected to Dropbox"
                                , Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }

        @Override
        public void onConnected(Bundle bundle) {

        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }

        @Override
        public void onResume() {
            super.onResume();
            if (mApi != null) {
                if (mApi.getSession().authenticationSuccessful()) {
                    try {
                        mApi.getSession().finishAuthentication();
                        String oauth2AccessToken = mApi.getSession().getOAuth2AccessToken();
                        if (oauth2AccessToken != null) {
                            String s = CommonsUtils.getPrefString(getActivity(), Constants.ACCESS_SECRET_NAME);
                            Map<String, String> tmp = new HashMap<String, String>();
                            tmp.put(Constants.ACCESS_KEY_NAME, "oauth2:");
                            tmp.put(Constants.ACCESS_SECRET_NAME, oauth2AccessToken);
                            CommonsUtils.putPrefStrings(getActivity(), tmp);
                            if (s == null) {
                                syncPreference.setEnabled(true);
                                setAlarm("box", getActivity());
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception occurred: " + e.toString());
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class BackupPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_backup_restore);
            Preference backup = findPreference("backup_db");

            backup.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), BackupActivity.class);
                    startActivity(intent);
                    return true;
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class FeedbackPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_feedback);

            Preference rateApp = findPreference("rate_app");
            rateApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent rateUsIntent = new Intent(Intent.ACTION_VIEW);
                    rateUsIntent.setData(Uri.parse("market://details?id=com.fynxapp"));
                    startActivity(rateUsIntent);
                    return false;
                }
            });

            Preference feedback = findPreference("feedback");
            feedback.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"yogasaikrishna@gmail.com"});
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Fynx - Expense manager Feedback");

                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    return false;
                }
            });
        }
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName)
                || BackupPreferenceFragment.class.getName().equals(fragmentName);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_RESOLUTION:
                if (resultCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                } else {
                    Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                            new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
                    startActivityForResult(intent, Constants.AUTH_REQUEST);
                }
                break;
            case Constants.AUTH_REQUEST:
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Drive.API)
                        .addScope(Drive.SCOPE_FILE)
                        .setAccountName(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME))
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();
                break;
            case REQ_CREATE_PATTERN:
                if (resultCode == RESULT_OK) {
                    lock.setTitle(R.string.disable_lock);
                    char[] pattern = data.getCharArrayExtra(
                            LockPatternActivity.EXTRA_PATTERN);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mApi != null) {
            if (mApi.getSession().authenticationSuccessful()) {
                try {
                    mApi.getSession().finishAuthentication();
                    String oauth2AccessToken = mApi.getSession().getOAuth2AccessToken();
                    if (oauth2AccessToken != null) {
                        String s = CommonsUtils.getPrefString(this, Constants.ACCESS_SECRET_NAME);
                        Map<String, String> tmp = new HashMap<String, String>();
                        tmp.put(Constants.ACCESS_KEY_NAME, "oauth2:");
                        tmp.put(Constants.ACCESS_SECRET_NAME, oauth2AccessToken);
                        CommonsUtils.putPrefStrings(SettingsActivity.this, tmp);
                        if (s == null) {
                            syncPreference.setEnabled(true);
                            setAlarm("box", this);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception occurred: " + e.toString());
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private static void setAlarm(String type, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        String frequency = prefs.getString(Constants.PREF_STORED_FREQUENCY, null);

        if (frequency != null) {
            if (frequency.equalsIgnoreCase(context.getString(R.string.daily))) {
                if (type.equals("drive")) {
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY);
                } else {
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY);
                }
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.weekly))) {
                if (type.equals("drive")) {
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 7);
                } else {
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 7);
                }
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.bi_weekly))) {
                if (type.equals("drive")) {
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 14);
                } else {
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 14);
                }
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.monthly))) {
                if (type.equals("drive")) {
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 30);
                } else {
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 30);
                }
            }
        }
    }
}
