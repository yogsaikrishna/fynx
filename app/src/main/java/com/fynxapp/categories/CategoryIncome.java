package com.fynxapp.categories;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fynxapp.AddCategory;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Category;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class CategoryIncome extends Fragment implements AdapterView.OnItemClickListener {
    private static final String TAG = "CategoryIncome";

    private String[] tileColors;
    private ListView listView;
    private Vector<RowData> data;
    private RowData rowData;
    private ListAdapter listAdapter;
    private List<Category> incomeCategories;
    private DBHelper db;
    private TextView mNoData;
    private Typeface mTypeface;

    public CategoryIncome() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_layout, container, false);

        db = new DBHelper(getActivity());
        incomeCategories = db.getCategoriesByType("income");

        tileColors = getResources().getStringArray(R.array.tile_colors);

        mTypeface = Typeface.createFromAsset(getActivity().getAssets(), Constants.FONT_ROBOTO_LIGHT);

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        listView = (ListView) view.findViewById(R.id.listView);
        mNoData = (TextView) view.findViewById(R.id.tv_no_data);
        mNoData.setTypeface(mTypeface);

        data = new Vector<RowData>();
        if (incomeCategories != null
                && incomeCategories.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < incomeCategories.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = incomeCategories.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }

        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(getActivity(), R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getActivity(), AddCategory.class);
        intent.putExtra("edit", true);
        intent.putExtra("id", incomeCategories.get(i).getId());
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        incomeCategories = db.getCategoriesByType("income");

        Random random = new Random();
        int low = 0;
        int high = tileColors.length;

        data = new Vector<RowData>();
        if (incomeCategories != null
                && incomeCategories.size() > 0) {
            mNoData.setVisibility(View.GONE);
            for (int i = 0; i < incomeCategories.size(); i++) {
                int rNum = random.nextInt(high - low) + low;
                String color = tileColors[rNum];
                String category = incomeCategories.get(i).getName();
                rowData = new RowData(category, "" + category.charAt(0), Color.parseColor(color), false);
                data.add(rowData);
            }
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

        listAdapter = new ListAdapter(getActivity(), R.layout.list_item, R.id.tv_primary, data);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }
}
