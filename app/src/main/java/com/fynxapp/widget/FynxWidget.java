package com.fynxapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RemoteViews;

import com.fynxapp.AddExpenseActivity;
import com.fynxapp.BaseActivity;
import com.fynxapp.OverviewActivity;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;

import java.text.NumberFormat;

public class FynxWidget extends AppWidgetProvider {
    private static final String TAG = "FynxWidget";

    private double income;
    private double expense;
    private DBHelper db;
    private SharedPreferences mSharedPreferences;
    private String mCurrency, mGrouping;
    private NumberFormat nf;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrency = mSharedPreferences.getString("data_currency", "\u0024");
        mGrouping = mSharedPreferences.getString("grouping_list", "monthly");

        db = new DBHelper(context);
        income = db.getTotalIncome(mGrouping);
        expense = db.getTotalExpense(mGrouping);
        db.close();

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        for (int i = 0; i < N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }


    @Override
    public void onEnabled(Context context) {

    }

    @Override
    public void onDisabled(Context context) {

    }

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
            int appWidgetId) {

        String totalBalance = nf.format(income) + " " + mCurrency;
        String totalExpense = nf.format(expense) + " " + mCurrency;
        String summary = context.getResources().getString(R.string.widget_text);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.fynx_widget);
        views.setImageViewBitmap(R.id.tv_amt_expense, getFontBitmap(context,
                totalExpense + " / " + totalBalance,
                Color.WHITE, 18, Constants.FONT_ROBOTO_LIGHT));

        views.setImageViewBitmap(R.id.tv_description, getFontBitmap(context,
                summary, Color.WHITE, 12, Constants.FONT_ROBOTO_LIGHT));

        Intent intent = new Intent(context, AddExpenseActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 3, intent, 0);
        views.setOnClickPendingIntent(R.id.im_wd_expense, pendingIntent);

        Intent openIntent = new Intent(context, BaseActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 8, openIntent, 0);
        views.setOnClickPendingIntent(R.id.widget_container, pIntent);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public Bitmap getFontBitmap(Context context, String text, int color, float fontSizeSP,
                                       String fontName) {
        int fontSizePX = convertDiptoPix(context, fontSizeSP);
        int pad = (fontSizePX / 9);
        Paint paint = new Paint();
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
        paint.setAntiAlias(true);
        paint.setTypeface(typeface);
        paint.setColor(color);
        paint.setTextSize(fontSizePX);

        int textWidth = (int) (paint.measureText(text) + pad * 2);
        int height = (int) (fontSizePX / 0.75);
        Bitmap bitmap = Bitmap.createBitmap(textWidth, height, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        float xOriginal = pad;
        canvas.drawText(text, xOriginal, fontSizePX, paint);
        return bitmap;
    }

    public int convertDiptoPix(Context context, float dip) {
        int value = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
        return value;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName widget = new ComponentName(context.getPackageName(),
                    FynxWidget.class.getName());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(widget);
            for (int i = 0; i < appWidgetIds.length; i++) {
                updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
            }
        }
    }
}


