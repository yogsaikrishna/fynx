package com.fynxapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Summary;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.FileUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;

import java.util.List;

public class SyncService extends IntentService {
    private static final String TAG = "SyncService";

    private GoogleApiClient mGoogleApiClient;
    private DBHelper db;

    private DropboxAPI<AndroidAuthSession> mApi;
    private AppKeyPair mAppKeys;
    private AndroidAuthSession mSession;

    public SyncService() {
        super("SyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            db = new DBHelper(this);
            List<Summary> mList = db.getAccountSummary();
            FileUtils.exportToExcel(mList);
            db.close();

            final String action = intent.getStringExtra(Constants.SYNC_TYPE);
            if (Constants.ACTION_DRIVE_SYNC.equals(action)) {
                try {
                    if (mGoogleApiClient == null) {
                        mGoogleApiClient = new GoogleApiClient.Builder(this)
                                .addApi(Drive.API)
                                .addScope(Drive.SCOPE_FILE)
                                .build();
                    }

                    mGoogleApiClient.connect();
                    FileUtils.saveFileToDrive(this, mGoogleApiClient);
                    setNextSyncDate("drive");
                } catch (Exception e) {
                    Log.e(TAG, "Exception occurred: " + e.getMessage());
                }
            } else if (Constants.ACTION_DROPBOX_SYNC.equals(action)) {
                try {
                    mAppKeys = new AppKeyPair(Constants.APP_KEY, Constants.APP_SECRET);
                    mSession = new AndroidAuthSession(mAppKeys);
                    mApi = new DropboxAPI<AndroidAuthSession>(mSession);
                    String token = CommonsUtils.getPrefString(this, Constants.ACCESS_SECRET_NAME);
                    if (token != null) {
                        mApi.getSession().setOAuth2AccessToken(token);
                    }
                    FileUtils.saveFileToDropbox(this, mApi);
                    setNextSyncDate("box");
                } catch (Exception e) {
                    Log.e(TAG, "Exception occurred: " + e.getMessage());
                }
            }
        }
    }

    private void setNextSyncDate(String type) {
        SharedPreferences prefs = getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;

        String frequency = prefs.getString(Constants.PREF_STORED_FREQUENCY, null);

        String[] today = DateUtils.getTodayDate(Constants.DD_MM_YYYY).split("-");
        int date = Integer.parseInt(today[0]);
        int month = Integer.parseInt(today[1]);
        int year = Integer.parseInt(today[2]);

        if (frequency != null) {
            editor = prefs.edit();
            if (frequency.equalsIgnoreCase(getString(R.string.daily))) {
                date += 1;
                if (month == 2 && (date > 28)) {
                    if (date > 29) {
                        date = 1;
                        month += 1;
                    }
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date = 1;
                    month += 1;
                } else if (date > 31) {
                    date = 1;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase(getString(R.string.weekly))) {
                date += 7;
                if (month == 2 && (date > 28)) {
                    date -= 28;
                    month += 1;
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date -= 30;
                    month += 1;
                } else if (date > 31) {
                    date -= 31;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase(getString(R.string.bi_weekly))) {
                date += 14;
                if (month == 2 && (date > 28)) {
                    date -= 28;
                    month += 1;
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date -= 30;
                    month += 1;
                } else if (date > 31) {
                    date -= 31;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase(getString(R.string.monthly))) {
                month += 1;
                if (month == 2 && (date > 28)) {
                    date = 28;
                }
                if (month > 12) {
                    month = 1;
                    year += 1;
                }
            }
            if (type.equals("drive")) {
                editor.putInt(Constants.SYNC_DRIVE_DATE, date);
                editor.putInt(Constants.SYNC_DRIVE_MONTH, month);
                editor.putInt(Constants.SYNC_DRIVE_YEAR, year);
            } else if (type.equals("box")) {
                editor.putInt(Constants.SYNC_BOX_DATE, date);
                editor.putInt(Constants.SYNC_BOX_MONTH, month);
                editor.putInt(Constants.SYNC_BOX_YEAR, year);
            }
            editor.commit();
        }
    }
}
