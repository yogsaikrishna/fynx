package com.fynxapp.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fynxapp.AddExpenseActivity;
import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;

public class NotificationService extends IntentService {
    private static final String TAG = "NotificationService";
    private DBHelper db;
    private SharedPreferences preferences;
    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            if (intent.getAction().equals("Notify")) {
                db = new DBHelper(this);
                int count = db.getTodayExpenses();

                preferences = PreferenceManager.getDefaultSharedPreferences(this);
                boolean enabled = preferences.getBoolean("notifications_new_message", false);

                if (enabled
                        && count == 0) {
                    String uri = preferences.getString("notifications_new_message_ringtone", null);
                    boolean vibrate = preferences.getBoolean("notifications_new_message_vibrate", false);
                    CommonsUtils.showNotification(this, uri, vibrate);
                }
            } else if (intent.getAction().equals("Add")) {
                notificationManager.cancel(Constants.NOTIFICATION_ID);
                Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(it);
                Intent mIntent = new Intent(this, AddExpenseActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
            } else {
                notificationManager.cancel(Constants.NOTIFICATION_ID);
            }
        }
    }

    private void setNextSyncDate() {
        SharedPreferences prefs = getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;

        String[] today = DateUtils.getTodayDate(Constants.DD_MM_YYYY).split("-");
        int date = Integer.parseInt(today[0]);
        int month = Integer.parseInt(today[1]);
        int year = Integer.parseInt(today[2]);

        editor = prefs.edit();
        date += 1;
        if (month == 2 && (date > 28)) {
            if (date > 29) {
                date = 1;
                month += 1;
            }
        } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                && date > 30) {
            date = 1;
            month += 1;
        } else if (date > 31) {
            date = 1;
            if (month == 12) {
                month = 1;
                year += 1;
            } else {
                month += 1;
            }
        }
        editor.putInt(Constants.REMAINDER_DATE, date);
        editor.putInt(Constants.REMAINDER_MONTH, month);
        editor.putInt(Constants.REMAINDER_YEAR, year);
        editor.commit();
    }
}
