package com.fynxapp.services;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.widget.FynxWidget;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class RecurringService extends IntentService {
    private static final String TAG = "RecurringService";
    private DBHelper db;
    private Amount mRecurring;

    public RecurringService() {
        super("RecurringService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            long _id = bundle.getInt(Constants.RECURRENCE_ID);

            db = new DBHelper(this);
            mRecurring = db.getRecurringById((int) _id);
            if (mRecurring != null) {
                mRecurring.setRecurringId(mRecurring.getId());
                mRecurring.setId(-1);
                String tmpDate = mRecurring.getDate();
                mRecurring.setDate(mRecurring.getUpdatedDate());
                if (mRecurring.getType().equals("income")) {
                    db.saveIncome(mRecurring);
                } else {
                    db.saveExpense(mRecurring);
                }
                mRecurring.setId((int) _id);
                mRecurring.setDate(tmpDate);
                String dbDate = DateUtils.convertStringToDate(tmpDate);
                db.updateGoals(dbDate, mRecurring.getAmount(), mRecurring.getType());
                Intent wIntent = new Intent(this, FynxWidget.class);
                wIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                int[] ids = {R.xml.fynx_widget_info};
                wIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                sendBroadcast(wIntent);

                setNextSyncDate(mRecurring.getFrequency());
            }
        }
    }

    private void setNextSyncDate(String frequency) {
        String[] today = DateUtils.getTodayDate(Constants.DD_MM_YYYY).split("-");
        int date = Integer.parseInt(today[0]);
        int month = Integer.parseInt(today[1]);
        int year = Integer.parseInt(today[2]);

        if (frequency != null) {
            if (frequency.equalsIgnoreCase("daily")) {
                date += 1;
                if (month == 2 && (date > 28)) {
                    if (date > 29) {
                        date = 1;
                        month += 1;
                    }
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date = 1;
                    month += 1;
                } else if (date > 31) {
                    date = 1;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("weekly")) {
                date += 7;
                if (month == 2 && (date > 28)) {
                    date -= 28;
                    month += 1;
                } else if ((month == 4 || month == 6 || month == 9 || month == 11)
                        && date > 30) {
                    date -= 30;
                    month += 1;
                } else if (date > 31) {
                    date -= 31;
                    if (month == 12) {
                        month = 1;
                        year += 1;
                    } else {
                        month += 1;
                    }
                }
            } else if (frequency.equalsIgnoreCase("monthly")) {
                month += 1;
                if (month == 2 && (date > 28)) {
                    date = 28;
                }
                if (month > 12) {
                    month = 1;
                    year += 1;
                }
            } else if (frequency.equalsIgnoreCase("yearly")) {
                year += 1;
            }
        }
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        calendar.set(Calendar.DATE, date);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        String mDate = df.format(calendar.getTime());
        mRecurring.setUpdatedDate(mDate);
        db.saveRecurring(mRecurring);
    }
}
