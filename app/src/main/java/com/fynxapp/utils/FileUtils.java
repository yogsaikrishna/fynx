package com.fynxapp.utils;

import android.content.Context;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.objects.Summary;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.util.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.bytecode.opencsv.CSVWriter;

public class FileUtils {
    private static final String TAG = "FileUtils";

    public static <E> Uri exportToExcel(List<E> mList) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Summary");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        HSSFRow header = sheet.createRow(0);
        header.createCell(0).setCellValue("Title");
        header.createCell(1).setCellValue("Category");
        header.createCell(2).setCellValue("Type");
        header.createCell(3).setCellValue("Amount");
        header.createCell(4).setCellValue("Date");
        Object[] entries;
        for (int i = 0; i < mList.size(); i++) {
            entries = new Object[5];
            Summary summary = (Summary) mList.get(i);

            entries[0] = summary.getTitle();
            entries[1] = summary.getCategory();
            entries[2] = summary.getType();
            entries[3] = summary.getAmount();
            entries[4] = summary.getDate();
            data.put((i + 1), entries);
        }

        Set<Integer> keySet = data.keySet();
        int rowNum = 1;
        for (Integer key : keySet) {
            HSSFRow row = sheet.createRow(rowNum++);
            Object[] objArr = data.get(key);
            int cellNum = 0;
            for (Object obj : objArr) {
                HSSFCell cell = row.createCell(cellNum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            if (CommonsUtils.isExternalStorageWritable()) {
                File file = new File(CommonsUtils.getStorageDir(Constants.FILE_DIR_NAME),
                        "Export_" + DateUtils.getTodayDate(Constants.DD_MMM_YYYY).replaceAll("-", "_") + ".xls");
                FileOutputStream out = new FileOutputStream(file);
                workbook.write(out);
                out.close();
                return Uri.fromFile(file);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred: " + e.getMessage());
        }
        return null;
    }

    public static <E> Uri exportToCSV(List<E> mList) {
        if (CommonsUtils.isExternalStorageWritable()) {
            try {
                File file = new File(CommonsUtils.getStorageDir(Constants.FILE_DIR_NAME),
                        "Export_" + DateUtils.getTodayDate(Constants.DD_MMM_YYYY).replaceAll("-", "_") + ".csv");
                CSVWriter writer = new CSVWriter(new FileWriter(file), ',');
                String header[] = {"Title", "Category", "Type", "Amount", "Date"};
                writer.writeNext(header);
                String entries[];
                List<String[]> values = new ArrayList<String[]>();
                for (int i = 0; i < mList.size(); i++) {
                    entries = new String[5];
                    Summary summary = (Summary) mList.get(i);
                    entries[0] = summary.getTitle();
                    entries[1] = summary.getCategory();
                    entries[2] = summary.getType();
                    entries[3] = "" + summary.getAmount();
                    entries[4] = summary.getDate();
                    values.add(entries);
                }
                writer.writeAll(values);
                writer.close();
                return Uri.fromFile(file);
            } catch (Exception e) {
                Log.e(TAG, "Exception occurred: " + e.getMessage());
            }
        }
        return null;
    }

    public static void  saveFileToDrive(final Context context, final GoogleApiClient mGoogleApiClient) {
        File fileDir = CommonsUtils.getStorageDir(Constants.FILE_DIR_NAME);
        final String fileName = "Export_" +
                DateUtils.getTodayDate(Constants.DD_MMM_YYYY).replaceAll("-", "_") + ".xls";
        final File file = new File(fileDir, fileName);
        Drive.DriveApi.newContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.ContentsResult>() {

            @Override
            public void onResult(DriveApi.ContentsResult result) {
                if (!result.getStatus().isSuccess()) {
                    Log.i(TAG, "Failed to create new contents.");
                    return;
                }

                OutputStream outputStream = result.getContents().getOutputStream();
                try {
                    InputStream is = new FileInputStream(file);
                    outputStream.write(IOUtils.toByteArray(is));
                } catch (Exception e) {
                    Log.i(TAG, "Unable to write file contents.");
                }
                String folderId = CommonsUtils.getPrefString(context, "drive_folder");

                MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                        .setTitle(fileName)
                        .setMimeType("application/vnd.ms-excel")
                        .build();

                Drive.DriveApi.getFolder(mGoogleApiClient, getDriveId(folderId))
                        .createFile(mGoogleApiClient, metadataChangeSet, result.getContents());
                Map<String, String> prefs = new HashMap<String, String>();
                prefs.put(Constants.PREF_LAST_DRIVE_SYNC, DateUtils.getCurrentDateAndTime());
                CommonsUtils.putPrefStrings(context, prefs);
            }
        });
    }

    public static DriveId getDriveId(String folderId) {
        return DriveId.decodeFromString(folderId);
    }

    public static void saveFileToDropbox(final Context context, final DropboxAPI<AndroidAuthSession> mApi) {
        File fileDir = CommonsUtils.getStorageDir(Constants.FILE_DIR_NAME);
        String fileName = "Export_" +
                DateUtils.getTodayDate(Constants.DD_MMM_YYYY).replaceAll("-", "_") + ".xls";
        File file = new File(fileDir, fileName);
        try {
            Log.d(TAG, mApi.getSession().getOAuth2AccessToken());
            FileInputStream is = new FileInputStream(file);
            DropboxAPI.Entry response = mApi.putFile(fileName, is,
                    file.length(), null, null);
            Map<String, String> prefs = new HashMap<String, String>();
            prefs.put(Constants.PREF_LAST_BOX_SYNC, DateUtils.getCurrentDateAndTime());
            CommonsUtils.putPrefStrings(context, prefs);
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred: " + e.toString());
        }
    }

    public static Uri exportDB() {
        try {
            File sd = CommonsUtils.getStorageDir(Constants.FILE_DIR_NAME);
            File data = Environment.getDataDirectory();
            if (sd.canWrite()) {
                String backUpFrom = "//data//" + "com.fynxapp"
                        + "//databases//" + "fynxdb";
                String backUpTo = "db_export_ " + DateUtils.getTodayDate(Constants.DD_MM_YYYY)
                        .replaceAll("-", "_")+ ".db"; // From SD directory.
                File backupDB = new File(data, backUpFrom);
                File currentDB = new File(sd, backUpTo);

                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                FileChannel src = new FileInputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                return Uri.fromFile(currentDB);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred: " + e.toString());
        }
        return null;
    }

    public static boolean importDB(String path) {
        try {
            File data = Environment.getDataDirectory();

            String restoreTo = "//data//" + "com.fynxapp"
                    + "//databases//" + "fynxdb";
            File currentDB = new File(data, restoreTo);
            File backupDB = new File(path);

            FileChannel src = new FileInputStream(backupDB).getChannel();
            FileChannel dst = new FileOutputStream(currentDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred: " + e.toString());
            return false;
        }
        return true;
    }
}
