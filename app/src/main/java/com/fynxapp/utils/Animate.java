package com.fynxapp.utils;

import android.support.v7.app.ActionBarActivity;
import android.widget.ProgressBar;

public class Animate {
    private ActionBarActivity context;
    private ProgressBar progressBar;
    private int mProgress, progress;

    public Animate(ActionBarActivity context, ProgressBar progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        animate();
    }

    private void animate() {
        mProgress = progressBar.getProgress();
        progress = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progress <= mProgress) {
                    try {
                        progress += 1;
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                    }
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progress);
                        }
                    });
                }
            }
        }).start();
    }
}
