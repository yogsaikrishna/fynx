package com.fynxapp.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.services.NotificationService;
import com.google.android.gms.common.internal.m;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CommonsUtils {
    private static final String TAG = "CommonsUtils";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    public static String getPrefString(Context context, String key) {
        SharedPreferences prefs = getPreferences(context);
        String value = prefs.getString(key, null);
        return value;
    }

    public static Boolean getPrefBoolean(Context context, String key) {
        SharedPreferences prefs = getPreferences(context);
        Boolean value = prefs.getBoolean(key, false);
        return value;
    }

    public static int getPrefInt(Context context, String key) {
        SharedPreferences prefs = getPreferences(context);
        int value = prefs.getInt(key, 0);
        return value;
    }

    public static void putPrefStrings(Context context, Map<String, String> map) {
        SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> pair = (Map.Entry<String, String>) iterator.next();
            editor.putString((String) pair.getKey(), (String) pair.getValue());
        }
        editor.commit();
    }

    public static void putPrefBooleans(Context context, Map<String, Boolean> map) {
        SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Iterator<Map.Entry<String, Boolean>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Boolean> pair = (Map.Entry<String, Boolean>) iterator.next();
            editor.putBoolean((String) pair.getKey(), (Boolean) pair.getValue());
        }
        editor.commit();
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)
                || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getStorageDir(String dirName) {
        File file = new File(Environment.getExternalStorageDirectory(), dirName);
        if (!file.mkdirs()) {
        }
        return file;
    }

    public static void showNotification(Context context, String uri, boolean vibrate) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent mIntent = new Intent(context, NotificationService.class);
        mIntent.setAction("Add");
        PendingIntent pIntentAdd = PendingIntent.getService(context, 0, mIntent, 0);
        mIntent.setAction("Clear");
        PendingIntent pIntentClear = PendingIntent.getService(context, 0, mIntent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(context.getString(R.string.daily_remainder))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(context.getString(R.string.notification_message)))
                .setSound(Uri.parse(uri))
                .setAutoCancel(true)
                .addAction(R.drawable.ic_action_cancel, context.getString(R.string.cancel), pIntentClear)
                .addAction(R.drawable.ic_action_new, context.getString(R.string.add_expense), pIntentAdd);
        if (vibrate)
            builder.setVibrate(new long[]{300, 200, 100, 500, 200, 100});
        manager.notify(Constants.NOTIFICATION_ID, builder.build());
    }

    public static final SpannableString getSpannableString(Context context, CharSequence str) {
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new TypefaceSpan(context, Constants.FONT_ROBOTO), 0, str.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static String joinList(String pSeperator, List<String> pList) {
        StringBuilder mBuilder = new StringBuilder();
        if (pList != null
                && pList.size() > 0) {
            for (int i = 0; i < pList.size() - 1; i++) {
                mBuilder.append("'" + pList.get(i) + "'").append(pSeperator);
            }
            mBuilder.append("'" + pList.get(pList.size() - 1) + "'");
        }
        return mBuilder.toString();
    }
}
