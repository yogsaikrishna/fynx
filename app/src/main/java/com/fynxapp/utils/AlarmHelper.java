package com.fynxapp.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.fynxapp.constants.Constants;
import com.fynxapp.receivers.NotificationReceiver;
import com.fynxapp.receivers.RecurringReceiver;
import com.fynxapp.receivers.SyncAlarmReceiver;

import java.util.Calendar;

public class AlarmHelper {
    private static final String TAG = "AlarmHelper";

    public static void setDropboxAlarm(Context context, long interval) {

        int day = CommonsUtils.getPrefInt(context, Constants.SYNC_BOX_DATE);
        int month = CommonsUtils.getPrefInt(context, Constants.SYNC_BOX_MONTH);
        int year = CommonsUtils.getPrefInt(context, Constants.SYNC_BOX_YEAR);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        if (day > 0 && month > 0 && year > 0) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, (month - 1));
            calendar.set(Calendar.YEAR, year);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);

        Intent intent = new Intent(context, SyncAlarmReceiver.class);
        intent.putExtra(Constants.SYNC_TYPE, Constants.ACTION_DROPBOX_SYNC);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.BOX_REQUEST_CODE,
                intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), interval, pendingIntent);

    }

    public static void setDriveAlarm(Context context, long interval) {
        int day = CommonsUtils.getPrefInt(context, Constants.SYNC_DRIVE_DATE);
        int month = CommonsUtils.getPrefInt(context, Constants.SYNC_DRIVE_MONTH);
        int year = CommonsUtils.getPrefInt(context, Constants.SYNC_DRIVE_YEAR);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        if (day > 0 && month > 0 && year > 0) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, (month - 1));
            calendar.set(Calendar.YEAR, year);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);

        Intent intent = new Intent(context, SyncAlarmReceiver.class);
        intent.putExtra(Constants.SYNC_TYPE, Constants.ACTION_DRIVE_SYNC);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.DRIVE_REQUEST_CODE,
                intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), interval, pendingIntent);
    }

    public static void setRecurringAlarm(Context context, long interval, int id, String mDate) {
        String tmpDate[] = mDate.split("-");

        int day = Integer.parseInt(tmpDate[0]);
        int month = DateUtils.getMonth(tmpDate[1]);
        int year = Integer.parseInt(tmpDate[2]);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        if (day > 0 && month > 0 && year > 0) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, (month - 1));
            calendar.set(Calendar.YEAR, year);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);

        Intent intent = new Intent(context, RecurringReceiver.class);
        intent.putExtra(Constants.RECURRENCE_ID, id);

        boolean mAlarm = PendingIntent.getBroadcast(context, id, intent,
                PendingIntent.FLAG_NO_CREATE) != null;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (mAlarm) {
            alarmManager.cancel(pendingIntent);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), interval, pendingIntent);

    }

    public static void cancelRecurringAlarm(Context context, int id) {
        Intent intent = new Intent(context, RecurringReceiver.class);
        intent.putExtra(Constants.RECURRENCE_ID, id);
        boolean mAlarm = PendingIntent.getBroadcast(context, id, intent,
                PendingIntent.FLAG_NO_CREATE) != null;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (mAlarm) {
            alarmManager.cancel(pendingIntent);
        }
    }

    public static void setNotificationAlarm(Context context) {
        int day = CommonsUtils.getPrefInt(context, Constants.REMAINDER_DATE);
        int month = CommonsUtils.getPrefInt(context, Constants.REMAINDER_MONTH);
        int year = CommonsUtils.getPrefInt(context, Constants.REMAINDER_YEAR);

        SharedPreferences mPreferences = context.getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences defPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        String tmpTime = mPreferences.getString(Constants.PREF_NOTIFY_TIME, null);
        if (tmpTime == null) {
            tmpTime = defPrefs.getString("alarm_time", null);
        }
        String alarmTime[];
        int hours = -1, minutes = -1;

        if (tmpTime != null) {
            alarmTime = tmpTime.split(":");
            hours = Integer.parseInt(alarmTime[0]);
            minutes = Integer.parseInt(alarmTime[1]);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        if (day > 0 && month > 0 && year > 0) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, (month - 1));
            calendar.set(Calendar.YEAR, year);
        }
        if (hours >= 0 && minutes >= 0) {
            calendar.set(Calendar.HOUR_OF_DAY, hours);
            calendar.set(Calendar.MINUTE, minutes);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, 18);
            calendar.set(Calendar.MINUTE, 00);
        }

        Intent intent = new Intent(context, NotificationReceiver.class);

        boolean mAlarm = PendingIntent.getBroadcast(context, Constants.NOTIFICATION_ID, intent,
                PendingIntent.FLAG_NO_CREATE) != null;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                Constants.NOTIFICATION_ID, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (mAlarm) {
            alarmManager.cancel(pendingIntent);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }
}
