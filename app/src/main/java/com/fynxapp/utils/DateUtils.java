package com.fynxapp.utils;

import android.util.Log;

import com.fynxapp.constants.Constants;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    private static final String TAG = "DateUtils";

    public static final String getMonth(int month) {
        DateFormatSymbols dfs = new DateFormatSymbols();
        return dfs.getShortMonths()[month];
    }

    public static final int getMonth(String month) {
        DateFormatSymbols dfs = new DateFormatSymbols();
        String localeMonths[] = dfs.getShortMonths();
        for (int i = 0; i < localeMonths.length; i++) {
            if (localeMonths[i].equalsIgnoreCase(month)) {
                return (i + 1);
            }
        }
        return 0;
    }

    public static final int getDaysInMonth(int month, int year) {
        switch (month) {
            case 1:
                return 31;
            case 2:
                if (isLeapYear(year)) {
                    return 29;
                }
                return 28;
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
        }
        return 0;
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0
                && year % 100 == 0
                && year % 400 == 0) {
            return true;
        }
        return false;
    }

    public static String getTodayDate(String format) {
        SimpleDateFormat sf = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        return sf.format(cal.getTime());
    }

    public static String getCurrentDateAndTime() {
        SimpleDateFormat sf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        Calendar cal = Calendar.getInstance();
        return sf.format(cal.getTime());
    }

    public static String convertStringToDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String convertedDate = null;
        try {
            Date dt = df.parse(date);
            SimpleDateFormat isoDate = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            convertedDate = isoDate.format(dt);
        } catch (Exception e) {
            Log.d(TAG, "Exception occurred: " + e.getMessage());
        }
        return convertedDate;
    }

    public static Date convertISOStringToDate(String isoDate) {
        SimpleDateFormat date = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            return date.parse(isoDate);
        } catch (Exception e) {
            Log.d(TAG, "Exception occurred: " + e.getMessage());
        }
        return null;
    }

    public static int getDaysBetweenDates(String startDate, String endDate) {
        String tmpDate = convertStringToDate(startDate);
        DateTime stDate = new DateTime(convertISOStringToDate(tmpDate));
        tmpDate = convertStringToDate(endDate);
        DateTime edDate = new DateTime(convertISOStringToDate(tmpDate));
        return Days.daysBetween(stDate, edDate).getDays();
    }

}
