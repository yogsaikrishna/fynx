package com.fynxapp.settings;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.receivers.SyncAlarmReceiver;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;
import com.fynxapp.widget.FynxWidget;

import java.util.Calendar;

public class PreferenceListener implements Preference.OnPreferenceChangeListener {
    private static final String TAG = "FynxPreferenceListener";
    private Context context;
    private String folder, token;
    private SharedPreferences prefs;

    public PreferenceListener(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        String stringValue = value.toString();
        SharedPreferences.Editor editor;
        folder = CommonsUtils.getPrefString(context, Constants.PREF_DRIVE_FOLDER);
        token = CommonsUtils.getPrefString(context, Constants.ACCESS_SECRET_NAME);

        if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(stringValue);

            String key = listPreference.getKey();
            if (key.equals("sync_frequency")) {
                String listValue = listPreference.getEntries()[index].toString();
                String frequency = prefs.getString(Constants.PREF_STORED_FREQUENCY, null);
                if (frequency != null) {
                    if (!listValue.equals(frequency)) {
                        editor = prefs.edit();
                        editor.putString(Constants.PREF_STORED_FREQUENCY, listValue);
                        editor.commit();
                        changeSyncFrequency(context);
                    }
                } else {
                    if (folder != null
                            || token != null) {
                        editor = prefs.edit();
                        editor.putString(Constants.PREF_STORED_FREQUENCY, listValue);
                        editor.commit();
                        changeSyncFrequency(context);
                    } else {
                        editor = prefs.edit();
                        editor.putString(Constants.PREF_STORED_FREQUENCY, listValue);
                        editor.commit();
                    }
                }
            }

            if (key.equals("data_currency")
                    || key.equals("grouping_list")) {
                Intent intent = new Intent(context, FynxWidget.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                int[] ids = {R.xml.fynx_widget_info};
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                context.sendBroadcast(intent);
            }

            preference.setSummary(
                    index >= 0
                            ? listPreference.getEntries()[index]
                            : null);

        } else if (preference instanceof RingtonePreference) {
            if (TextUtils.isEmpty(stringValue)) {
                preference.setSummary(R.string.pref_ringtone_silent);

            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(
                        preference.getContext(), Uri.parse(stringValue));

                if (ringtone == null) {
                    preference.setSummary(null);
                } else {
                    String name = ringtone.getTitle(preference.getContext());
                    preference.setSummary(name);
                }
            }

        } else {
            preference.setSummary(stringValue);
        }
        return true;
    }

    private void changeSyncFrequency(Context context) {
        String frequency = prefs.getString(Constants.PREF_STORED_FREQUENCY, null);

        if (frequency != null) {
            if (frequency.equalsIgnoreCase(context.getString(R.string.daily))) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES);
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.monthly))) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 2);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 2);
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.bi_weekly))) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 3);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 3);
            } else if (frequency.equalsIgnoreCase(context.getString(R.string.monthly))) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 4);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_FIFTEEN_MINUTES * 4);
            }
        }
    }
}
