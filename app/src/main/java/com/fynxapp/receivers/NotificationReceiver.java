package com.fynxapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.fynxapp.services.NotificationService;

public class NotificationReceiver extends WakefulBroadcastReceiver {
    public NotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent mIntent = new Intent(context, NotificationService.class);
        mIntent.setAction("Notify");
        startWakefulService(context, mIntent);
    }
}
