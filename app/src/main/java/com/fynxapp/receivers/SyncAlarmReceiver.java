package com.fynxapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.fynxapp.constants.Constants;
import com.fynxapp.services.SyncService;

public class SyncAlarmReceiver extends WakefulBroadcastReceiver {
    public SyncAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, SyncService.class);
        service.putExtra(Constants.SYNC_TYPE, intent.getExtras().getString(Constants.SYNC_TYPE));
        startWakefulService(context, service);
    }
}
