package com.fynxapp.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.fynxapp.constants.Constants;
import com.fynxapp.services.RecurringService;

public class RecurringReceiver extends WakefulBroadcastReceiver {
    public RecurringReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        Intent service = new Intent(context, RecurringService.class);
        service.putExtra(Constants.RECURRENCE_ID, bundle.getInt(Constants.RECURRENCE_ID));
        startWakefulService(context, service);
    }
}
