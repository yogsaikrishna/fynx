package com.fynxapp.receivers;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.fynxapp.R;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Amount;
import com.fynxapp.utils.AlarmHelper;
import com.fynxapp.utils.CommonsUtils;

import java.util.List;

public class BootReceiver extends BroadcastReceiver {
    private String daily, weekly, bi_weekly, monthly, yearly;

    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        daily = context.getString(R.string.daily);
        weekly = context.getString(R.string.weekly);
        bi_weekly = context.getString(R.string.bi_weekly);
        monthly = context.getString(R.string.monthly);
        yearly = context.getString(R.string.yearly);

        addSyncAlarm(context);
        addRecurringAlarm(context);
        AlarmHelper.setNotificationAlarm(context);
    }

    private void addSyncAlarm(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);

        String frequency = prefs.getString(Constants.PREF_STORED_FREQUENCY, null);

        String folder = CommonsUtils.getPrefString(context, Constants.PREF_DRIVE_FOLDER);
        String token = CommonsUtils.getPrefString(context, Constants.ACCESS_SECRET_NAME);


        if (frequency != null) {
            if (frequency.equalsIgnoreCase(daily)) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY);
            } else if (frequency.equalsIgnoreCase(weekly)) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 7);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 7);
            } else if (frequency.equalsIgnoreCase(bi_weekly)) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 14);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 14);
            } else if (frequency.equalsIgnoreCase(monthly)) {
                if (folder != null)
                    AlarmHelper.setDriveAlarm(context, AlarmManager.INTERVAL_DAY * 30);
                if (token != null)
                    AlarmHelper.setDropboxAlarm(context, AlarmManager.INTERVAL_DAY * 30);
            }
        }
    }

    private void addRecurringAlarm(Context context) {
        DBHelper db = new DBHelper(context);
        List<Amount> recurring = db.getRecurringList();
        db.close();

        if (recurring != null
                && recurring.size() > 0) {
            for (int i = 0; i < recurring.size(); i++) {
                Amount amount = recurring.get(i);
                String frequency = amount.getFrequency();
                long interval = AlarmManager.INTERVAL_DAY * 30;

                if (frequency.equalsIgnoreCase(daily)) {
                    interval = AlarmManager.INTERVAL_DAY;
                } else if (frequency.equalsIgnoreCase(weekly)) {
                    interval = AlarmManager.INTERVAL_DAY * 7;
                } else if (frequency.equalsIgnoreCase(monthly)) {
                    interval = AlarmManager.INTERVAL_DAY * 30;
                } else if (frequency.equalsIgnoreCase(yearly)) {
                    interval = AlarmManager.INTERVAL_DAY * 365;
                }

                AlarmHelper.setRecurringAlarm(context, interval, amount.getId(), amount.getUpdatedDate());
            }
        }
    }

}
