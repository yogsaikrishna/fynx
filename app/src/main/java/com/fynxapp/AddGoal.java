package com.fynxapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fynxapp.constants.Constants;
import com.fynxapp.db.DBHelper;
import com.fynxapp.db.objects.Goal;
import com.fynxapp.drawer.ListAdapter;
import com.fynxapp.drawer.RowData;
import com.fynxapp.utils.DateUtils;
import com.fynxapp.utils.TypefaceSpan;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;


public class AddGoal extends ActionBarActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandler {
    private static final String TAG = "AddGoal";

    private String[] mListText;
    private Integer[] mImgResources = {R.drawable.ic_add, R.drawable.ic_add,
            R.drawable.ic_accounts,
            R.drawable.ic_categories, R.drawable.ic_summary, R.drawable.ic_reports,
            R.drawable.ic_action_recurring, R.drawable.ic_action_settings};

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mNavAdapter;
    private Vector<RowData> mData;
    private RowData mRowData;
    private ListAdapter mListAdapter;
    private EditText mTitle, mDescription, mStartDate, mEndDate;
    private TextView mAmount;
    private TextView mHdrDetails, mHdrStartDate, mHdrAmount, mHdrEndDate;
    private DBHelper db;
    private Typeface mRoboto, mRobotoLight;
    private String currency;
    private NumberPickerBuilder npb;
    private Bundle mBundle;
    private boolean mEdit;
    private Goal mEditGoal, mGoal;
    private RadioButton mIncome, mExpense;
    private RadioGroup mRadioGroup;
    private String mType;
    private NumberFormat nf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_goal);

        db = new DBHelper(this);

        nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mEdit = mBundle.getBoolean("edit");
            int id = mBundle.getInt("id");
            if (mEdit) {
                mEditGoal = db.getGoalById(id);
                getSupportActionBar().setTitle(getString(R.string.edit_goal));
                mType = mEditGoal.getType();
            }
        } else {
            mType = "income";
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        currency = sharedPref.getString("data_currency", "\u0024");

        mRobotoLight = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO_LIGHT);
        mRoboto = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_ROBOTO);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mTitle = (EditText) this.findViewById(R.id.et_title);
        mDescription = (EditText) this.findViewById(R.id.et_description);
        mAmount = (TextView) this.findViewById(R.id.tv_amount);
        mStartDate = (EditText) this.findViewById(R.id.et_start_date);
        mEndDate = (EditText) this.findViewById(R.id.et_end_date);

        mHdrDetails = (TextView) this.findViewById(R.id.hdr_details);
        mHdrStartDate = (TextView) this.findViewById(R.id.hdr_start_date);
        mHdrAmount = (TextView) this.findViewById(R.id.hdr_goal_amount);
        mHdrEndDate = (TextView) this.findViewById(R.id.hdr_end_date);

        mIncome = (RadioButton) this.findViewById(R.id.rb_income);
        mExpense = (RadioButton) this.findViewById(R.id.rb_expense);
        mRadioGroup = (RadioGroup) this.findViewById(R.id.radiogroup);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mStartDate.setText(getString(R.string.today));
        mStartDate.setFocusable(false);
        mEndDate.setFocusable(false);

        mStartDate.setOnClickListener(this);
        mEndDate.setOnClickListener(this);

        mAmount.setTypeface(mRobotoLight);
        mStartDate.setTypeface(mRoboto);
        mEndDate.setTypeface(mRoboto);

        mTitle.setTypeface(mRoboto);
        mDescription.setTypeface(mRoboto);

        mHdrDetails.setTypeface(mRobotoLight);
        mHdrStartDate.setTypeface(mRobotoLight);
        mHdrAmount.setTypeface(mRobotoLight);
        mHdrEndDate.setTypeface(mRobotoLight);

        mIncome.setTypeface(mRoboto);
        mExpense.setTypeface(mRoboto);

        mAmount.setText("0.00 " + currency);
        mAmount.setOnClickListener(this);

        mRadioGroup.check(R.id.rb_income);

        npb = new NumberPickerBuilder()
                .setFragmentManager(getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setDecimalVisibility(View.VISIBLE)
                .setLabelText(currency);

        if (mEdit) {
            mAmount.setText(nf.format(mEditGoal.getAmount()) + " " + currency);
            mTitle.setText(mEditGoal.getTitle());
            mDescription.setText(mEditGoal.getDescription());
            mStartDate.setText(mEditGoal.getStartDate());
            mEndDate.setText(mEditGoal.getEndDate());
            if (mType.equals("income")) {
                mRadioGroup.check(R.id.rb_income);
            } else {
                mRadioGroup.check(R.id.rb_expense);
            }
            mIncome.setClickable(false);
            mExpense.setClickable(false);
        } else {
            npb.show();
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_income:
                        mType = "income";
                        break;
                    case R.id.rb_expense:
                        mType = "expense";
                        break;
                }
            }
        });

        SpannableString s = new SpannableString(this.getTitle());
        s.setSpan(new TypefaceSpan(this, Constants.FONT_ROBOTO), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_goal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.save:
                String amount = mAmount.getText().toString().replace(currency, "");
                String title = mTitle.getText().toString();
                String description = mDescription.getText().toString();
                String startDate = mStartDate.getText().toString();
                String endDate = mEndDate.getText().toString();

                if (amount.startsWith("0.00")) {
                    Toast.makeText(this, getString(R.string.err_amt), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (title.equals("")) {
                    Toast.makeText(this, getString(R.string.err_title), Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (endDate.equals("")) {
                    Toast.makeText(this, getString(R.string.err_goal_end_dt),
                            Toast.LENGTH_SHORT).show();
                    return true;
                } else if (DateUtils.getDaysBetweenDates(startDate, endDate) <= 0) {
                    Toast.makeText(this, getString(R.string.err_end_start_dt),
                            Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    if (startDate.equals(getString(R.string.today))) {
                        Calendar today = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        startDate = df.format(today.getTime());
                    }
                    mGoal = new Goal();
                    mGoal.setTitle(title);
                    mGoal.setDescription(description);
                    mGoal.setStartDate(startDate);
                    mGoal.setEndDate(endDate);
                    try {
                        mGoal.setAmount(nf.parse(amount).doubleValue());
                    } catch (Exception e) {
                        Log.d(TAG, "Exception occurred: " + e.toString());
                    }
                    mGoal.setType(mType);
                    if (mEdit) {
                        mGoal.setId(mEditGoal.getId());
                        mGoal.setGoalPercent(mEditGoal.getGoalPercent());
                        mGoal.setGoalAmount(mEditGoal.getGoalAmount());
                        mGoal.setCreatedDate(mEditGoal.getCreatedDate());
                    }
                    db.saveGoal(mGoal);
                    this.finish();
                }
                break;
            case R.id.cancel:
                this.finish();
                break;
            case R.id.discard:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setMessage(getString(R.string.info_delete))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.deleteGoalById(mEditGoal.getId());
                                AddGoal.this.finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem delete = menu.findItem(R.id.discard);
        MenuItem cancel = menu.findItem(R.id.cancel);

        if (mEdit) {
            delete.setVisible(true);
            cancel.setVisible(false);
        } else {
            delete.setVisible(false);
            cancel.setVisible(true);
        }
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeNavList();
        mNavAdapter = (ListView) findViewById(R.id.left_drawer);
        mData = new Vector<RowData>();
        for (int i = 0; i < mListText.length; i++) {
            mRowData = new RowData(mListText[i], mImgResources[i], false);
            mData.add(mRowData);
        }
        mListAdapter = new ListAdapter(this, R.layout.drawer_list_item, R.id.tv_primary, mData);
        mNavAdapter.setAdapter(mListAdapter);
        mNavAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_start_date:
                DatePickerBuilder sdpb = new DatePickerBuilder()
                        .setFragmentManager(getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                sdpb.setReference(R.id.et_start_date);
                sdpb.show();
                break;
            case R.id.et_end_date:
                DatePickerBuilder edpb = new DatePickerBuilder()
                        .setFragmentManager(getSupportFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                edpb.setReference(R.id.et_end_date);
                edpb.show();
                break;
            case R.id.tv_amount:
                npb.show();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = null;
        switch (i) {
            case 0:
                intent = new Intent(AddGoal.this, AddIncomeActivity.class);
                break;
            case 1:
                intent = new Intent(AddGoal.this, AddExpenseActivity.class);
                break;
            case 2:
                intent = new Intent(AddGoal.this, AccountsActivity.class);
                break;
            case 3:
                intent = new Intent(AddGoal.this, CategoriesActivity.class);
                break;
            case 4:
                intent = new Intent(AddGoal.this, AccSummaryActivity.class);
                break;
            case 5:
                intent = new Intent(AddGoal.this, ReportsActivity.class);
                break;
            case 6:
                intent = new Intent(AddGoal.this, RecurringActivity.class);
                break;
            case 7:
                intent = new Intent(AddGoal.this, SettingsActivity.class);
                break;
        }
        startActivity(intent);
        mDrawerLayout.closeDrawers();
        finish();
    }

    @Override
    public void onDialogDateSet(int i, int year, int month, int date) {
        switch (i) {
            case R.id.et_start_date:
                mStartDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
                break;
            case R.id.et_end_date:
                mEndDate.setText(date + "-" + DateUtils.getMonth(month) + "-" + year);
                break;
        }
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        mAmount.setText(nf.format(v2) + " " + currency);
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    private void initializeNavList() {
        mListText = new String[8];
        mListText[0] = getString(R.string.add_income);
        mListText[1] = getString(R.string.add_expense);
        mListText[2] = getString(R.string.accounts);
        mListText[3] = getString(R.string.categories);
        mListText[4] = getString(R.string.title_activity_acc_summary);
        mListText[5] = getString(R.string.reports);
        mListText[6] = getString(R.string.recurring);
        mListText[7] = getString(R.string.action_settings);
    }
}